﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;

public class TestLtiGradePushDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
	IGradePushDbContext<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser,
		TestExternalGroupAssignment, TestGroupAssignment, TestScore, TestGroupAssignmentJobLog>
{
	public TestLtiGradePushDbContext()
	{
	}

	public TestLtiGradePushDbContext(DbContextOptions options) : base(options)
	{
	}

	public TestLtiGradePushDbContext(DbConnection existingConnection) : base(existingConnection)
	{
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		builder
			.Entity<TestExternalGroup>()
			.HasOne(l => (TestUser)l.User);

		builder
			.Entity<TestExternalGroupUser>()
			.HasOne(l => (TestUser)l.User);
	}

	public DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

	public DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

	public DbSet<TestGroup> Groups { get; set; }

	public DbSet<TestExternalTerm> ExternalTerms { get; set; }

	public DbSet<TestExternalGroup> ExternalGroups { get; set; }

	public DbSet<TestExternalGroupUser> ExternalGroupUsers { get; set; }

	public DbSet<TestExternalGroupAssignment> ExternalGroupAssignments { get; set; }

	public DbSet<TestGroupAssignment> GroupAssignments { get; set; }

	public DbSet<TestAssignment> Assignments { get; set; }

	public DbSet<TestScore> GradableScores { get; set; }

	public DbSet<TestGroupAssignmentJobLog> GroupAssignmentJobLogs { get; set; }

	public async Task<List<TestScore>> GetScoresAsync(int gradableId, CancellationToken cancellationToken = default)
	{
		return await GradableScores.Where(s => s.GroupAssignmentId.Equals(gradableId))
			.ToListAsync(cancellationToken);
	}

	public async Task<IGradable> GetGradableAsync(int gradableId, CancellationToken cancellationToken)
	{
		var gradable = await GroupAssignments.SingleOrDefaultAsync(ga => ga.Id == gradableId && !ga.IsDeleted, cancellationToken);
		if (gradable == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(TestGroupAssignment), gradableId));

		var assignment =
			await Assignments.SingleOrDefaultAsync(a => a.Id.Equals(gradable.AssignmentId) && !a.IsDeleted,
				cancellationToken);
		if (assignment == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(TestAssignment), gradable.AssignmentId));

		var group =
			await Groups.SingleOrDefaultAsync(g => g.Id.Equals(gradable.GroupId) && !g.IsDeleted,
				cancellationToken);
		if (group == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(TestGroup), gradable.GroupId));

		return gradable;
	}

	public DbSet<TestGroupAssignment> GetGradables()
	{
		return GroupAssignments;
	}

	public DbSet<TestGroupAssignmentJobLog> GetGradableJobLogs()
	{
		return GroupAssignmentJobLogs;
	}

	public DbSet<TestExternalGroupAssignment> GetExternalGradables()
	{
		return ExternalGroupAssignments;
	}

	public async Task<List<TestExternalGroupAssignment>> GetExternalGradablesAsync(int gradableId,
		CancellationToken cancellationToken = default)
	{
		return await ExternalGroupAssignments.Where(ega => ega.GroupAssignmentId.Equals(gradableId))
			.ToListAsync(cancellationToken);
	}
}