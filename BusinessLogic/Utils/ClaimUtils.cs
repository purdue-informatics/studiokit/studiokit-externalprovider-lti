﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;

public static class ClaimUtils
{
	private static readonly JsonSerializerSettings JsonClaimSerializerSettings = new JsonSerializerSettings
	{
		ContractResolver = new CamelCasePropertyNamesContractResolver(),
		NullValueHandling = NullValueHandling.Ignore
	};

	/// <summary>
	/// Given a collection of external Claims, generates new Claims where the Issuer is a combination of
	/// Issuer, ResourceLink Id (if it exists), and Context Id.
	/// </summary>
	/// <param name="externalClaims">The claims to update</param>
	/// <returns>The new collection of claims</returns>
	public static List<Claim> UpdateExternalClaimsIssuer(ICollection<Claim> externalClaims)
	{
		var suffix = "";
		var messageType = externalClaims.Single(c => c.Type.Equals(LtiClaim.MessageType)).Value;
		if (messageType == LtiMessageType.LtiResourceLinkRequest)
		{
			var resourceLinkClaimValue = externalClaims.Single(c => c.Type.Equals(LtiClaim.ResourceLink)).Value;
			var resourceLink = JsonConvert.DeserializeObject<ResourceLink>(resourceLinkClaimValue);
			suffix = resourceLink.Id;
		}

		var contextClaimValue = externalClaims.Single(c => c.Type.Equals(LtiClaim.Context)).Value;
		var context = JsonConvert.DeserializeObject<Context>(contextClaimValue);
		suffix = $"{(!string.IsNullOrWhiteSpace(suffix) ? $"{suffix}-" : "")}{context.Id}";

		return externalClaims
			.Select(ec => new Claim(ec.Type, ec.Value, ec.ValueType, $"{ec.Issuer}-{suffix}",
				ec.OriginalIssuer, ec.Subject))
			.ToList();
	}

	public static List<Claim> GetCommonClaims(IEnumerable<Claim> externalClaims)
	{
		return externalClaims
			.Where(c => !string.IsNullOrWhiteSpace(c.Value))
			.Join(LtiClaim.CommonClaimMap,
				c => c.Type,
				kvp => kvp.Key,
				(c, kvp) => new Claim(kvp.Value, c.Value, c.ValueType, c.Issuer, c.OriginalIssuer, c.Subject))
			.ToList();
	}

	public static List<Claim> GetBrightspaceClaims(IEnumerable<Claim> externalClaims)
	{
		var claims = new List<Claim>();

		var brightspaceClaim =
			externalClaims.SingleOrDefault(c => c.Type.Equals(BrightspaceClaim.BrightspaceClaimType));
		if (brightspaceClaim == null)
			return claims;

		var brightspaceClaimValue = JsonConvert.DeserializeObject<BrightspaceClaim>(brightspaceClaim.Value);

		claims.Add(new Claim(
			BrightspaceClaim.BrightspaceOrgDefinedId,
			brightspaceClaimValue.OrgDefinedId,
			ClaimValueTypes.String,
			brightspaceClaim.Issuer,
			brightspaceClaim.OriginalIssuer,
			brightspaceClaim.Subject));

		claims.Add(new Claim(
			BrightspaceClaim.BrightspaceUsername,
			brightspaceClaimValue.Username,
			ClaimValueTypes.String,
			brightspaceClaim.Issuer,
			brightspaceClaim.OriginalIssuer,
			brightspaceClaim.Subject));

		if (!string.IsNullOrWhiteSpace(brightspaceClaimValue.ImpersonationOrgDefinedId))
		{
			claims.Add(new Claim(
				BrightspaceClaim.BrightspaceImpersonationOrgDefinedId,
				brightspaceClaimValue.ImpersonationOrgDefinedId,
				ClaimValueTypes.String,
				brightspaceClaim.Issuer,
				brightspaceClaim.OriginalIssuer,
				brightspaceClaim.Subject));
		}

		return claims;
	}

	public static List<string> GetExternalRoleNames(IEnumerable<Claim> externalClaims)
	{
		return externalClaims
			.Where(c => c.Type.Equals(LtiClaim.Roles))
			.Select(c => c.Value)
			.Distinct()
			.ToList();
	}

	public static List<string> GetGroupRoleNames(IEnumerable<Claim> externalClaims)
	{
		return GetGroupRoleNames(GetExternalRoleNames(externalClaims));
	}

	public static List<string> GetGroupRoleNames(List<string> externalRoleNames)
	{
		return externalRoleNames
			// if a sub-role is included, exclude the principal role
			// e.g. with Instructor and Instructor#Grader, ignore Instructor
			.Where(name => !LtiRole.ContextSubRoles.ContainsKey(name) || !LtiRole.ContextSubRoles[name].Any(externalRoleNames.Contains))
			.Join(LtiRole.ContextRoleMap,
				name => name,
				kvp => kvp.Key,
				(name, kvp) => kvp.Value)
			.Distinct()
			.ToList();
	}

	public static List<string> GetApplicationRoleNames(IEnumerable<Claim> externalClaims)
	{
		return GetApplicationRoleNames(GetExternalRoleNames(externalClaims));
	}

	public static List<string> GetApplicationRoleNames(List<string> externalRoleNames)
	{
		return externalRoleNames
			.Join(LtiRole.ApplicationRoleMap,
				name => name,
				kvp => kvp.Key,
				(name, kvp) => kvp.Value)
			.Distinct()
			.ToList();
	}

	public static List<Claim> GetApplicationRoleClaims(IEnumerable<Claim> externalClaims)
	{
		return GetApplicationRoleNames(externalClaims)
			.Select(r => new Claim(ClaimTypes.Role, r))
			.ToList();
	}

	public static Claim CreateJsonClaim(string type, object value)
	{
		var stringValue = JsonConvert.SerializeObject(value, JsonClaimSerializerSettings);
		return new Claim(type, stringValue, JsonClaimValueTypes.Json);
	}

	public static Claim CreateJsonArrayClaim(string type, object value)
	{
		var stringValue = JsonConvert.SerializeObject(value, JsonClaimSerializerSettings);
		return new Claim(type, stringValue, JsonClaimValueTypes.JsonArray);
	}
}