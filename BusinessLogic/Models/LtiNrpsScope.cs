﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class LtiNrpsScope
{
	public static string ContextMembershipReadOnly = "https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly";
}