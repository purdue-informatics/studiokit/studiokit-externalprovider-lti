﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Lti.Models;

public class LtiLaunch : ModelBase, IOwnable
{
	[Required]
	public int ExternalProviderId { get; set; }

	[ForeignKey(nameof(ExternalProviderId))]
	public LtiExternalProvider ExternalProvider { get; set; }

	[Required]
	public string CreatedById { get; set; }

	[ForeignKey(nameof(CreatedById))]
	public IUser User { get; set; }

	[Required]
	public string ExternalId { get; set; }

	[Required]
	public string Name { get; set; }

	public string Description { get; set; }

	/// <summary>
	/// When not null, indicates that a group exists for the launch and does not need to be created by the front-end.
	/// </summary>
	public int? GroupId { get; set; }

	public DateTime? CourseSectionStartDate { get; set; }

	public DateTime? CourseSectionEndDate { get; set; }

	public string Roles { get; set; }

	public string RosterUrl { get; set; }

	public string LtiVersion { get; set; }

	/// <summary>
	/// The platform URL to which the browser should be redirected when the LTI Launch is cancelled.
	/// </summary>
	public string ReturnUrl { get; set; }

	public string GradesUrl { get; set; }

	/// <summary>
	/// The platform URL to which the browser should be redirected when the deep linking selection process is complete.
	/// </summary>
	public string DeepLinkingReturnUrl { get; set; }

	public string DeepLinkingData { get; set; }
}