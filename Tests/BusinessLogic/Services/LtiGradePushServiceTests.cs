﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using Newtonsoft.Json;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Tests;
using StudioKit.Utilities;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services;

[TestClass]
public class LtiGradePushServiceTests : BaseTest
{
	private static DbConnection _dbConnection;
	private TestLtiGradePushDbContext _dbContext;
	private IDbContextTransaction _transaction;

	private IErrorHandler _errorHandler;

	private ILogger<LtiGradePushService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup,
		TestExternalGroupUser, TestExternalGroupAssignment, TestGroupAssignment, TestScore,
		TestGroupAssignmentJobLog>> _logger;

	private ILtiApiService _ltiApiService;
	private IDateTimeProvider _dateTimeProvider;
	private LtiExternalProvider _ltiExternalProvider;
	private TestGroupAssignment _groupAssignment;
	private int _lineItemCreatedCount;
	private int _lineItemLoadedCount;
	private int _lineItemUpdatedCount;
	private int _resultsCreatedOrUpdatedCount;
	private TestExternalGroup _externalGroup1;
	private List<TestExternalGroupUser> _externalGroup1Students;
	private List<TestScore> _scores;
	private TestExternalGroup _externalGroup2;
	private List<TestExternalGroupUser> _externalGroup2Students;
	private TestExternalGroupAssignment _externalGroupAssignment;
	private LineItem _existingLineItem;
	private List<Result> _existingResults;
	private List<Score> _postedScores;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		_dbConnection = DbConnectionFactory.CreateSqliteInMemoryConnection();
		await _dbConnection.OpenAsync();

		var dateTimeProvider = new DynamicDateTimeProvider
		{
			UtcNow = new DateTime(2020, 1, 1, 5, 0, 0)
		};
		var groupCreatedDate = dateTimeProvider.UtcNow.AddDays(-10);

		await using var dbContext = DbContextFactory<TestLtiGradePushDbContext>.CreateTransient(_dbConnection);

		var instructor = new TestUser { Id = "1", UserName = "1", NormalizedUserName = "1" };
		dbContext.Users.Add(instructor);
		var students = new List<TestUser>
		{
			new() { Id = "2", UserName = "2", NormalizedUserName = "2" },
			new() { Id = "3", UserName = "3", NormalizedUserName = "3" },
			new() { Id = "4", UserName = "4", NormalizedUserName = "4" },
		};
		dbContext.Users.AddRange(students);

		var ltiExternalProvider = TestUtils.GetLtiExternalProvider();
		ltiExternalProvider.GradePushEnabled = true;
		dbContext.LtiExternalProviders.Add(ltiExternalProvider);

		var externalGroup1 = new TestExternalGroup
		{
			Name = "Test Course",
			ExternalProvider = ltiExternalProvider,
			ExternalId = "1",
			GradesUrl = "https://lms.edu/courses/1/lineitems",
			DateStored = groupCreatedDate,
			DateLastUpdated = groupCreatedDate,
			UserId = "1",
			ExternalGroupUsers = new List<TestExternalGroupUser>
			{
				new()
				{
					UserId = "1",
					ExternalUserId = "a",
					DateStored = groupCreatedDate,
					DateLastUpdated = groupCreatedDate,
					Roles =
						$"{LtiRole.MembershipInstructor},{LtiRole.MembershipAdministrator},{LtiRole.InstitutionInstructor},{LtiRole.InstitutionAdministrator}"
				}
			}
		};

		var groupAssignment = new TestGroupAssignment
		{
			DateStored = groupCreatedDate,
			DateLastUpdated = groupCreatedDate,
			Assignment = new TestAssignment
			{
				Name = "Test Assignment",
				Points = 100m,
				DateStored = groupCreatedDate,
				DateLastUpdated = groupCreatedDate
			},
			Group = new TestGroup
			{
				DateStored = groupCreatedDate,
				DateLastUpdated = groupCreatedDate,
				ExternalGroups = new List<TestExternalGroup>
				{
					externalGroup1
				}
			}
		};
		dbContext.GroupAssignments.Add(groupAssignment);

		await dbContext.SaveChangesAsync();
	}

	[TestInitialize]
	public void BeforeEach()
	{
		_errorHandler = new Mock<IErrorHandler>().Object;
		_logger = new Mock<ILogger<LtiGradePushService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup,
			TestExternalGroupUser, TestExternalGroupAssignment, TestGroupAssignment, TestScore,
			TestGroupAssignmentJobLog>>>().Object;
		_dateTimeProvider = new DynamicDateTimeProvider
		{
			UtcNow = new DateTime(2020, 1, 1, 5, 0, 0)
		};
		_ltiApiService = GetLtiApiService();

		(_dbContext, _transaction) = DbContextFactory<TestLtiGradePushDbContext>.CreateTransientAndTransaction(_dbConnection);

		_postedScores = new List<Score>();

		_ltiExternalProvider = _dbContext.LtiExternalProviders.First();
		_externalGroup1 = _dbContext.ExternalGroups.Single(eg => eg.ExternalId == "1");
		_groupAssignment = _dbContext.GroupAssignments
			.Include(ga => ga.Group)
			.Include(ga => ga.Assignment)
			.First();

		var rosterSyncDate = _dateTimeProvider.UtcNow.AddDays(-5);
		var scoreCreatedDate = _dateTimeProvider.UtcNow.AddDays(-4);
		var previousGradePushDate = _dateTimeProvider.UtcNow.AddDays(-3);

		_externalGroup2 = new TestExternalGroup
		{
			Name = "Test Course 2",
			Group = _groupAssignment.Group,
			ExternalProvider = _ltiExternalProvider,
			ExternalId = "2",
			GradesUrl = "https://lms.edu/courses/2/lineitems",
			DateStored = _groupAssignment.Group.DateStored,
			DateLastUpdated = _groupAssignment.Group.DateLastUpdated,
			UserId = "1",
			ExternalGroupUsers = new List<TestExternalGroupUser>
			{
				new()
				{
					UserId = "1",
					ExternalUserId = "a",
					DateStored = _groupAssignment.Group.DateStored,
					DateLastUpdated = _groupAssignment.Group.DateLastUpdated,
					Roles = LtiRole.MembershipInstructor
				}
			}
		};

		_externalGroup1Students = new List<TestExternalGroupUser>
		{
			new()
			{
				ExternalGroup = _externalGroup1,
				UserId = "2",
				ExternalUserId = "b",
				DateStored = rosterSyncDate,
				DateLastUpdated = rosterSyncDate,
				Roles = LtiRole.MembershipLearner
			},
			new()
			{
				ExternalGroup = _externalGroup1,
				UserId = "3",
				ExternalUserId = "c",
				DateStored = rosterSyncDate,
				DateLastUpdated = rosterSyncDate,
				Roles = LtiRole.MembershipLearner
			},
			new()
			{
				ExternalGroup = _externalGroup1,
				UserId = "4",
				ExternalUserId = "d",
				DateStored = rosterSyncDate,
				DateLastUpdated = rosterSyncDate,
				Roles = LtiRole.MembershipLearner
			}
		};

		_externalGroup2Students = new List<TestExternalGroupUser>
		{
			new()
			{
				ExternalGroup = _externalGroup2,
				UserId = "2",
				ExternalUserId = "x",
				DateStored = rosterSyncDate,
				DateLastUpdated = rosterSyncDate,
				Roles = LtiRole.MembershipLearner
			},
			new()
			{
				ExternalGroup = _externalGroup2,
				UserId = "3",
				ExternalUserId = "y",
				DateStored = rosterSyncDate,
				DateLastUpdated = rosterSyncDate,
				Roles = LtiRole.MembershipLearner
			},
			new()
			{
				ExternalGroup = _externalGroup2,
				UserId = "4",
				ExternalUserId = "z",
				DateStored = rosterSyncDate,
				DateLastUpdated = rosterSyncDate,
				Roles = LtiRole.MembershipLearner
			}
		};

		_scores = new List<TestScore>
		{
			// score for external instructor, should not be pushed
			new()
			{
				GroupAssignment = _groupAssignment,
				UserId = "1",
				TotalPoints = 100m,
				DateStored = scoreCreatedDate,
				DateLastUpdated = scoreCreatedDate
			},
			new()
			{
				GroupAssignment = _groupAssignment,
				UserId = "2",
				TotalPoints = 90m,
				DateStored = scoreCreatedDate,
				DateLastUpdated = scoreCreatedDate
			},
			new()
			{
				GroupAssignment = _groupAssignment,
				UserId = "3",
				TotalPoints = 80m,
				DateStored = scoreCreatedDate,
				DateLastUpdated = scoreCreatedDate
			},
			new()
			{
				GroupAssignment = _groupAssignment,
				UserId = "4",
				TotalPoints = 0m,
				DateStored = scoreCreatedDate,
				DateLastUpdated = scoreCreatedDate
			}
		};

		_externalGroupAssignment = new TestExternalGroupAssignment
		{
			GroupAssignment = _groupAssignment,
			ExternalGroup = _externalGroup1,
			ExternalId = $"{_externalGroup1.GradesUrl}/999",
			DateStored = previousGradePushDate,
			DateLastUpdated = previousGradePushDate
		};

		_existingLineItem = new LineItem
		{
			Id = $"{_externalGroup1.GradesUrl}/999",
			// ReSharper disable once PossibleInvalidOperationException
			ScoreMaximum = _groupAssignment.Points.Value,
			Label = _groupAssignment.Name,
			ResourceId = _groupAssignment.Id.ToString(),
			ResourceLinkId = Guid.NewGuid().ToString(),
			Tag = "grade"
		};
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		_dbConnection.Close();
		_dbConnection.Dispose();
	}

	#region PushGradesAsync

	[TestMethod]
	public void PushGradesAsync_ShouldThrowIfNullPrincipal()
	{
		var service = GetService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(1, null, cancellationToken: CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
	}

	[TestMethod]
	public void PushGradesAsync_ShouldThrowIfPrincipalNotSystem()
	{
		var service = GetService(_dbContext);
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(1, new GenericPrincipal(new ClaimsIdentity(), null),
					cancellationToken: CancellationToken.None);
			}), string.Format(Security.Properties.Strings.RequiresSystemPrincipal, "PushGradesAsync"));
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfGradableIsNotFound()
	{
		_dbContext.GroupAssignments.Remove(_groupAssignment);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(1, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), string.Format(Strings.EntityNotFoundById, nameof(TestGroupAssignment), 1));
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfGradableIsDeleted()
	{
		_groupAssignment.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), string.Format(Strings.EntityNotFoundById, nameof(TestGroupAssignment), 1));
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfAssignmentIsDeleted()
	{
		_groupAssignment.Assignment.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), string.Format(Strings.EntityNotFoundById, nameof(TestAssignment), 1));
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIGroupIsDeleted()
	{
		_groupAssignment.Group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), string.Format(Strings.EntityNotFoundById, nameof(TestGroup), 1));
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfAssignmentHasNullPoints()
	{
		_groupAssignment.Assignment.Points = null;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), Strings.GradablePointsRequired);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfAssignmentHasZeroPoints()
	{
		_groupAssignment.Assignment.Points = 0;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), Strings.GradablePointsRequired);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfNoExternalGroups()
	{
		_groupAssignment.Group.ExternalGroups = null;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), Strings.GradePushExternalGroupsRequired);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfNoExternalGroupsWithDisabledExternalProvider()
	{
		_ltiExternalProvider.GradePushEnabled = false;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), Strings.GradePushExternalGroupsRequired);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowIfNoExternalGroupsWithNoGradesUrl()
	{
		_groupAssignment.Group.ExternalGroups.First().GradesUrl = null;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), Strings.GradePushExternalGroupsRequired);
	}

	[TestMethod]
	public void PushGradesAsync_ShouldThrowIfNoScores()
	{
		var service = GetService(_dbContext);
		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}), Strings.GradePushScoresRequired);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldPushGradesToASingleExternalGroup()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 1, 0, 0, 3);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldPushGradesToASingleExternalGroup_WhileNotSaving()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), false, CancellationToken.None);

		await AssertAsync(_dbContext, 1, 1, 0, 0, 3);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldNotPushGradesIfScoreHasNoPoints()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_scores.Skip(1).First().TotalPoints = null;
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 1, 0, 0, 2);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldPushGradesToMultipleExternalGroups()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.ExternalGroups.Add(_externalGroup2);
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
		_dbContext.GradableScores.AddRange(_scores);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 2, 2, 0, 0, 6);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldPushGradesIfSomeExternalGradablesExist()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.ExternalGroups.Add(_externalGroup2);
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 2, 1, 1, 0, 6);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldNotPushGradeIfResultExistsWithSamePoints()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		// mock that 1 student already had their grade pushed
		_existingResults = new List<Result>
		{
			new()
			{
				UserId = _externalGroup1Students.Single(egu => egu.UserId.Equals(_scores.Skip(1).First().UserId)).ExternalUserId,
				ResultScore = _scores.Skip(1).First().TotalPoints ?? 0m // making ReSharper happy
			}
		};

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 0, 1, 0, 2);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldCaptureExceptionForConflict()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var wasErrorHandlerCalled = false;
		var errorHandlerMock = new Mock<IErrorHandler>();
		errorHandlerMock.Setup(x => x.CaptureException(It.IsAny<LtiException>())).Callback(() =>
		{
			wasErrorHandlerCalled = true;
		});
		var errorHandler = errorHandlerMock.Object;

		var service = GetService(_dbContext,
			GetLtiApiService(scoreException: new LtiException(Strings.LtiApiRequestError,
				new HttpRequestException(Strings.HttpRequestExceptionConflict))), errorHandler);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 0, 1, 0, 0);
		Assert.IsTrue(wasErrorHandlerCalled);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldCaptureExceptionForNotFoundUserDoesNotExist()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var wasErrorHandlerCalled = false;
		var errorHandlerMock = new Mock<IErrorHandler>();
		errorHandlerMock.Setup(x => x.CaptureException(It.IsAny<LtiException>())).Callback(() =>
		{
			wasErrorHandlerCalled = true;
		});
		var errorHandler = errorHandlerMock.Object;

		var service = GetService(_dbContext,
			GetLtiApiService(scoreException: new LtiException(Strings.LtiApiRequestError,
				new HttpRequestException($"{Strings.HttpRequestExceptionNotFound}. ({Strings.GradePushUserDoesNotExist})"))), errorHandler);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 0, 1, 0, 0);
		Assert.IsTrue(wasErrorHandlerCalled);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldCaptureExceptionForForbiddenUserNotEnrolled()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var wasErrorHandlerCalled = false;
		var errorHandlerMock = new Mock<IErrorHandler>();
		errorHandlerMock.Setup(x => x.CaptureException(It.IsAny<LtiException>())).Callback(() =>
		{
			wasErrorHandlerCalled = true;
		});
		var errorHandler = errorHandlerMock.Object;

		var service = GetService(_dbContext,
			GetLtiApiService(scoreException: new LtiException(Strings.LtiApiRequestError,
				new HttpRequestException($"{Strings.HttpRequestExceptionForbidden}. ({Strings.GradePushUserNotEnrolled})"))), errorHandler);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 0, 1, 0, 0);
		Assert.IsTrue(wasErrorHandlerCalled);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldCaptureExceptionForUnproccessableEntity()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var wasErrorHandlerCalled = false;
		var errorHandlerMock = new Mock<IErrorHandler>();
		errorHandlerMock.Setup(x => x.CaptureException(It.IsAny<LtiException>())).Callback(() =>
		{
			wasErrorHandlerCalled = true;
		});
		var errorHandler = errorHandlerMock.Object;

		var service = GetService(_dbContext,
			GetLtiApiService(scoreException: new LtiException(Strings.LtiApiRequestError,
				new HttpRequestException($"{Strings.HttpRequestExceptionUnprocessableEntity}. ({Strings.GradePushScoreCannotBePublished})"))), errorHandler);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 0, 1, 0, 0);
		Assert.IsTrue(wasErrorHandlerCalled);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldThrowOnNonConflictExceptions()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext,
			GetLtiApiService(scoreException: new LtiException(Strings.LtiApiRequestError, new JsonException())));
		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);
			}),
			string.Format(Strings.GradePushFailed, _externalGroupAssignment.Id, _externalGroup1.Id,
				_externalGroup1Students.First().ExternalUserId, _externalGroup1Students.First().UserId));
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldUpdateLineItemWithNameChange()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.ExternalGroups.Add(_externalGroup2);
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);

		_groupAssignment.Assignment.Name = "Updated Assignment";

		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 2, 1, 1, 1, 6);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldUpdateLineItemAndScoresWithPointsChange()
	{
		// mock that students already had their grades pushed
		_existingResults = _scores.Skip(1).Select(score =>
			new Result
			{
				UserId = _externalGroup1Students.Single(egu => egu.UserId.Equals(score.UserId)).ExternalUserId,
				ResultScore = score.TotalPoints ?? 0m // making ReSharper happy
			}
		).ToList();

		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);

		// update points maximum
		_groupAssignment.Assignment.Points = 120m;
		// update scores, since changing points the scores changed too (unless they were zero)
		_scores.ForEach(score =>
		{
			score.TotalPoints = (score.TotalPoints / 100m) * 120m;
		});

		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 1, 0, 1, 1, 2); // only 2 of 3 updated since the "0" score stays the same

		_postedScores.ForEach(postedScore =>
		{
			Assert.AreEqual(_dateTimeProvider.UtcNow, postedScore.Timestamp, "All scores sent with overridden date");
		});
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldCreateNewLineItemIfExistingNotFound()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.ExternalGroups.Add(_externalGroup2);
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
		_dbContext.GradableScores.AddRange(_scores);
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext,
			GetLtiApiService(lineItemGetException: new LtiException(Strings.LtiApiRequestError,
				new HttpRequestException(Strings.HttpRequestExceptionNotFound))));
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 2, 2, 0, 0, 6);

		var deletedExternalGroupAssignment =
			await _dbContext.ExternalGroupAssignments.SingleOrDefaultAsync(ega => ega.Id.Equals(_externalGroupAssignment.Id));
		Assert.IsNull(deletedExternalGroupAssignment);
	}

	[TestMethod]
	public async Task PushGradesAsync_ShouldCreateNewLineItemIfExistingWasNotSetUp()
	{
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
		_dbContext.ExternalGroups.Add(_externalGroup2);
		_dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
		_dbContext.GradableScores.AddRange(_scores);
		_externalGroupAssignment.ExternalId = null;
		_dbContext.ExternalGroupAssignments.Add(_externalGroupAssignment);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext, GetLtiApiService());
		await service.PushGradesAsync(_groupAssignment.Id, new SystemPrincipal(), cancellationToken: CancellationToken.None);

		await AssertAsync(_dbContext, 2, 2, 0, 0, 6);

		var deletedExternalGroupAssignment =
			await _dbContext.ExternalGroupAssignments.SingleOrDefaultAsync(ega => ega.Id.Equals(_externalGroupAssignment.Id));
		Assert.IsNull(deletedExternalGroupAssignment);
	}

	#endregion PushGradesAsync

	#region Helpers

	private ILtiApiService GetLtiApiService(Exception lineItemGetException = null, Exception scoreException = null)
	{
		var mockLtiApiService = new Mock<ILtiApiService>();

		// Mock creating a LineItem
		mockLtiApiService.Setup(x => x.PostAsync<LineItem>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			.Returns(async (LtiExternalProvider _, string url, Func<Uri, HttpContent> getHttpContent,
				CancellationToken cancellationToken, MediaTypeWithQualityHeaderValue _) =>
			{
				var stringContent = (StringContent)getHttpContent(new Uri(url));
				var stringContentAsString = await stringContent.ReadAsStringAsync(cancellationToken);
				var lineItem = JsonConvert.DeserializeObject<LineItem>(stringContentAsString);

				if (lineItem.ResourceId != _groupAssignment.Id.ToString())
				{
					throw new Exception();
				}

				_lineItemCreatedCount++;
				lineItem.Id = $"{url}/{_lineItemCreatedCount}";
				return (lineItem, new HttpResponseMessage());
			});

		// Mock loading existing LineItem
		mockLtiApiService.Setup(x => x.GetResultAsync<LineItem>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			.Returns((LtiExternalProvider _, string _,
				CancellationToken _, MediaTypeWithQualityHeaderValue _) =>
			{
				if (lineItemGetException != null)
				{
					throw lineItemGetException;
				}

				_lineItemLoadedCount++;
				return Task.FromResult((_existingLineItem, new HttpResponseMessage()));
			});

		// Mock updating a LineItem
		mockLtiApiService.Setup(x => x.PutAsync<LineItem>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			.Returns(async (LtiExternalProvider _, string url, Func<Uri, HttpContent> getHttpContent,
				CancellationToken cancellationToken, MediaTypeWithQualityHeaderValue _) =>
			{
				var stringContent = (StringContent)getHttpContent(new Uri(url));
				var stringContentAsString = await stringContent.ReadAsStringAsync(cancellationToken);
				var lineItem = JsonConvert.DeserializeObject<LineItem>(stringContentAsString);

				// mock errors for changing properties that are not allowed to change
				if (lineItem.ResourceId != _existingLineItem.ResourceId)
				{
					throw new LtiException(Strings.LtiApiRequestError,
						new HttpRequestException(
							$"{Strings.HttpRequestExceptionBadRequest} ({Strings.CannotChangeResourceId})."));
				}

				if (lineItem.ResourceLinkId != _existingLineItem.ResourceLinkId)
				{
					throw new LtiException(Strings.LtiApiRequestError,
						new HttpRequestException(
							$"{Strings.HttpRequestExceptionBadRequest} ({Strings.CannotChangeResourceLinkId})."));
				}

				if (lineItem.Tag != _existingLineItem.Tag)
				{
					throw new LtiException(Strings.LtiApiRequestError,
						new HttpRequestException(
							$"{Strings.HttpRequestExceptionBadRequest} ({Strings.CannotChangeTags})."));
				}

				_lineItemUpdatedCount++;
				return (lineItem, new HttpResponseMessage());
			});

		// Mock pushing a Score
		mockLtiApiService.Setup(x => x.PostAsync<Result>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			.Returns(async (LtiExternalProvider _, string url, Func<Uri, HttpContent> getHttpContent,
				CancellationToken cancellationToken, MediaTypeWithQualityHeaderValue _) =>
			{
				var stringContent = (StringContent)getHttpContent(new Uri(url));
				var stringContentAsString = await stringContent.ReadAsStringAsync(cancellationToken);
				var score = JsonConvert.DeserializeObject<Score>(stringContentAsString);
				_postedScores.Add(score);
				var lineItemUrl = url.Replace("/scores", "");
				if (scoreException != null)
				{
					throw scoreException;
				}

				var result = new Result
				{
					Id = $"{lineItemUrl}/results/{_resultsCreatedOrUpdatedCount}",
					ScoreOf = lineItemUrl,
					UserId = score.UserId,
					ResultScore = score.ScoreGiven,
					ResultMaximum = score.ScoreMaximum,
					Comment = score.Comment
				};
				_resultsCreatedOrUpdatedCount++;
				return (result, new HttpResponseMessage());
			});

		// Mock loading existing Results
		_existingResults = new List<Result>();
		mockLtiApiService.Setup(x => x.GetResultsAsync<List<Result>>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			.Returns((LtiExternalProvider _, string _,
					CancellationToken _, MediaTypeWithQualityHeaderValue _) =>
				Task.FromResult(new List<(List<Result>, HttpResponseMessage)>
					{ (_existingResults, new HttpResponseMessage()) }));

		return mockLtiApiService.Object;
	}

	private LtiGradePushService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup,
		TestExternalGroupUser, TestExternalGroupAssignment, TestGroupAssignment, TestScore,
		TestGroupAssignmentJobLog> GetService(
		TestLtiGradePushDbContext dbContext,
		ILtiApiService ltiApiService = null,
		IErrorHandler errorHandler = null)
	{
		dbContext.ChangeTracker.Clear();
		return new LtiGradePushService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser,
			TestExternalGroupAssignment, TestGroupAssignment, TestScore, TestGroupAssignmentJobLog>(dbContext,
			ltiApiService ?? _ltiApiService, errorHandler ?? _errorHandler, _logger, _dateTimeProvider);
	}

	private async Task AssertAsync(
		TestLtiGradePushDbContext dbContext,
		int externalGradablesCount,
		int lineItemsCreated,
		int lineItemsLoaded,
		int lineItemsUpdated,
		int resultsCreatedOrUpdatedCount,
		CancellationToken cancellationToken = default)
	{
		// assert - externalGradable created for externalGroups without one
		var externalGradables = await dbContext.GetExternalGradablesAsync(_groupAssignment.Id, cancellationToken);
		Assert.AreEqual(externalGradablesCount, externalGradables.Count, "Number of ExternalGradables after push");
		Assert.AreEqual(lineItemsCreated, _lineItemCreatedCount, "Number of LineItems created");
		Assert.AreEqual(lineItemsLoaded, _lineItemLoadedCount, "Number of LineItems loaded");
		Assert.AreEqual(lineItemsUpdated, _lineItemUpdatedCount, "Number of LineItems updated");

		// assert - POST called correct number of times (externalGradables * externalGroupUser w/ score)
		Assert.AreEqual(resultsCreatedOrUpdatedCount, _resultsCreatedOrUpdatedCount, "Number of Results created");
	}

	#endregion Helpers
}