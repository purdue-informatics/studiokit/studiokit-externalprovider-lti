﻿using System.ComponentModel.DataAnnotations;

namespace StudioKit.ExternalProvider.Lti.Models;

public class LtiExternalProvider : ExternalProvider.Models.ExternalProvider
{
	[Required]
	public string Issuer { get; set; }

	[Required]
	public string DeploymentId { get; set; }

	[Required]
	public string OauthClientId { get; set; }

	[Required]
	public string OauthTokenUrl { get; set; }

	[Required]
	public string OpenIdConnectAuthenticationEndpoint { get; set; }

	[Required]
	public string OauthAudience { get; set; }

	[Required]
	public string KeySetUrl { get; set; }

	public bool ShouldDisplayCreateNonLtiGroupAlert { get; set; }
}