﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class LtiDeepLinkingContentType
{
	public static string Link = "link";

	public static string File = "file";

	public static string Html = "html";

	public static string LtiResourceLink = "ltiResourceLink";

	public static string Image = "image";
}