﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Security.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;

public static class TestUtils
{
	public const string TestRsaKeyPair = @"-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAo0YiQdd3b84+HiEwUlN1jZoqn5iU0ePpCqvhvi6mhejXEsvl
lrEVLgtYfWpIeP2HF7C1m66s1ml0JKi7AFdANubVHUB31cc1RLnS0gEafC+P+phk
llPfC/Sb9oI0/P2qHERdbk2Atl6voz9m3BSyK0Dhkno6USCxLgOtA8QIBt2UTvwi
T4Am74UgbRidIRVSPXTAT47bMOdez4yrNhPI0olr9x8+fjUH7tHASrg7o18qg7TF
6boOtEucUi2TVOGm3ZEgfQkNqu7agYkI/oxaHDb3dqAxUZsHtgNwyViOUg0wmXDG
oSGBdvon+NemxDTQpjVkTYBnEIsxn5QzvPyIzQIDAQABAoIBADJITA6I49B77Kds
wyvnZgF44/2IiPRmwwM1Ue03ArOktG+meYtB9+rZNaRSEgWo1mzWT+elELdrfIg5
qnV9aVo/FozOqNkeY2pJ7AIesuBh6W5cdkXRiJRu/YUaLmVnXXcdGsT4e1YurNqS
kQPgH8qwTYfFuqX3cnlgSm0pdutd3fqOuBiknHrN0W0/wS14/oR1Y/bDvOpo7jDw
Pa7n3ZYcOFOUc7BQPwkhiHDG/aEJwXum32xMEpR/UcIkvtAB/vE4VkTmoZsbk8p3
MfJp7o6feedQZTcgIOD4fxrPg/LCYyb+YSbqb8QHPK6hdTqkin+sYqysYHZbG8Ig
v90B8iUCgYEAzt0wOfVuqhaWPlZg6F/vXhPNtOr4kvZrV3TBtigjs3vVHozRS/iY
MHOUiHhu0fEC9yslEwX2jkWeteCl/EeksbC6FZzo1Sm/VfmPX1WYM7HYr7Kzjwc3
hZTZm8OVEiKgk0StaQVYz7rbcKXdXh/DocZ0C/SEzuaLsf0kMtm6WNsCgYEAyg5b
tbnCelSpRVB6XHItBNLbgzqU5BY8WQP6eeD6nhJnWyeNPJ/ehwW6Vn/85e+BKusr
pZuCeQFHFTqJ0q6bDD6YD31LOb0AMfiIYJE/u+oBoVEOBihG+CpAxsMJyFYOKIuT
jttFthRctMn9RCnkje/9/sxXtBW1jLvpwbmLIXcCgYB2yvh94B6L2GqxI6OE7kqk
iNTRdzoGEzJzR55SP9y4nn01jQJIEVs8P7NlZ1ukfjYIwKMKuJrs+rf4lChdprrC
O82wyam/d7jj42tdAOdlkFTyGLoagbd1o5QPahJ6Fp6F06ONsr9ck16e4vErsywC
A9fyYZm+wxAnx0n5VaU4jwKBgF3CeeYC0+7GGGIUrSL2zFMfsULczThl5Qz9Xp5t
un3dVl6jJNPL74vCKax36ZedItgSlodbeRjDcgO0zT2ZTlNJPHB7mIurW0rU5BvD
asq9FJKRtStAR2Zi3PoeiQyQejNwSUGSGHAcVMSDsmWN9wOyKK+yjMQBCuTKUfdW
LOyTAoGAGxKqfuaJEWJxktGR2yB93PlK0vptBKIdlvxgEl3/HJmK+LSU2AIQtMTW
njggIW2o5HDGhqh8RSSk3hggKx7LVNbcQeNul8oE7eClQSERy0LjVwrMhb9TGCVD
4HphNIPTgDbnc/nBaZ6XUDBvsTR0CBIXvuVckBTk4+Eyb6VLG1M=
-----END RSA PRIVATE KEY-----
";

	public const string Issuer = "https://lti-ri.imsglobal.org/";

	public const string Audience = "12345";

	public const int DeploymentId = 1;

	public const string Email = "unit.tester@example.org";

	public static string Sub = Guid.NewGuid().ToString();

	public static string ContextId = "17";

	public static string ContextTitle = "Course 1";

	public static string ContextLabel = "Course 1 Label";

	public static string TargetLinkUri = "https://tool.org";

	public static string LaunchPresentationReturnUrl = "https://lti-ri.imsglobal.org/platforms/19/returns";

	public static string NamesRoleServiceMembershipsUrl = "https://lti-ri.imsglobal.org/platforms/19/contexts/17/memberships.json";

	public static string EndpointLineItems = "https://lti-ri.imsglobal.org/platforms/19/line_items";

	public static string EndpointLineItem = "https://lti-ri.imsglobal.org/platforms/19/line_items/23";

	public static string DeepLinkingReturnUrl = "https://lti-ri.imsglobal.org/platforms/19/deep_linking";

	public static string DeepLinkingData = "csrftoken:c7fbba78-7b75-46e3-9201-11e6d5f36f53";

	public static string BrightspaceOrgDefinedId = "111111111";

	public static string BrightspaceUsername = "george";

	public static LaunchPresentation LaunchPresentation => new()
	{
		DocumentTarget = LtiDeepLinkingDocumentTarget.Iframe,
		Height = 320,
		Width = 240,
		ReturnUrl = LaunchPresentationReturnUrl
	};

	public static Context Context => new()
	{
		Id = ContextId,
		Label = ContextLabel,
		Title = ContextTitle,
		Type = new[] { LisVocab.CourseOffering }
	};

	public static Endpoint Endpoint => new()
	{
		Scope = new[]
		{
			LtiAgsScope.LineItem,
			LtiAgsScope.ResultReadOnly,
			LtiAgsScope.Score
		},
		LineItems = EndpointLineItems,
		LineItem = EndpointLineItem
	};

	public static NamesRoleService NamesRoleService => new()
	{
		ContextMembershipsUrl = NamesRoleServiceMembershipsUrl,
		ServiceVersion = "2.0"
	};

	public static ResourceLink ResourceLink => new()
	{
		Id = "35",
		Title = "Test Link 1",
		Description = "First Test Link"
	};

	public static DeepLinkingSettings DeepLinkingSettings => new()
	{
		DeepLinkReturnUrl = DeepLinkingReturnUrl,
		AcceptTypes = new[]
		{
			LtiDeepLinkingContentType.Link,
			LtiDeepLinkingContentType.File,
			LtiDeepLinkingContentType.LtiResourceLink,
			LtiDeepLinkingContentType.Image
		},
		AcceptPresentationDocumentTargets = new[]
		{
			LtiDeepLinkingDocumentTarget.Iframe,
			LtiDeepLinkingDocumentTarget.Window
		},
		AcceptMediaTypes = "*/*",
		AcceptMultiple = true,
		AutoCreate = false,
		Data = DeepLinkingData
	};

	public static BrightspaceClaim BrightspaceClaim => new()
	{
		TenantId = "351ba9a3-ec59-4583-8b49-8dfa92ecd3db",
		OrgDefinedId = BrightspaceOrgDefinedId,
		UserId = 22840,
		Username = BrightspaceUsername,
		ResourceLinkIdHistory = "",
		ContextIdHistory = ""
	};

	private static long ToUnixTime(DateTime dateTime)
	{
		return (int)dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
	}

	private static readonly Dictionary<string, object> ClaimsDictionary = new()
	{
		{ JsonWebTokenClaim.Subject, Sub },
		{ OidcTokenClaim.Nonce, "dummy" },
		{ LtiClaim.DeploymentId, DeploymentId },
		{
			LtiClaim.Platform,
			new Dictionary<string, object>
			{
				{ "name", "Test Platform" },
				{ "contact_email", "" },
				{ "description", "" },
				{ "url", "" },
				{ "product_family_code", "" },
				{ "version", "1.0" }
			}
		},
		{ LtiClaim.Version, LtiVersion.Lti1P3 },
		{ OidcTokenClaim.Locale, "en-US" },
		{ LtiClaim.TargetLinkUri, TargetLinkUri },
		{
			LtiClaim.LaunchPresentation,
			LaunchPresentation
		},
		{ OidcTokenClaim.GivenName, "Absolute" },
		{ OidcTokenClaim.FamilyName, "Unit" },
		{ OidcTokenClaim.MiddleName, "Testing" },
		{ OidcTokenClaim.Picture, "http://example.org/Carson.jpg" },
		{ OidcTokenClaim.Email, "unit.tester@example.org" },
		{ OidcTokenClaim.Name, "Absolute Testing Unit" },
		{
			LtiClaim.Roles,
			new[] { LtiRole.MembershipInstructor }
		},
		{
			LtiClaim.Context,
			Context
		},
		{
			LtiAgsClaim.Endpoint,
			Endpoint
		},
		{
			LtiNrpsClaim.NamesRoleService,
			NamesRoleService
		},
		{
			LtiClaim.Custom,
			new Dictionary<string, object>
			{
				{ "myCustomValue", 123 }
			}
		},
		{
			"https://www.example.com/extension",
			new Dictionary<string, object>
			{
				{ "color", "violet" }
			}
		},
		{
			BrightspaceClaim.BrightspaceClaimType,
			BrightspaceClaim
		}
	};

	public static Dictionary<string, object> GetResourceLinkClaimsDictionary(DateTime? issued = null, DateTime? expires = null) =>
		new(ClaimsDictionary)
		{
			{ LtiClaim.MessageType, LtiMessageType.LtiResourceLinkRequest },
			{ JsonWebTokenClaim.IssuedAt, ToUnixTime(issued ?? DateTime.UtcNow) },
			{ JsonWebTokenClaim.Expiration, ToUnixTime(expires ?? DateTime.UtcNow.AddHours(1)) },
			{
				LtiClaim.ResourceLink,
				ResourceLink
			}
		};

	public static Dictionary<string, object> GetDeepLinkingClaimsDictionary(DateTime? issued = null, DateTime? expires = null) =>
		new(ClaimsDictionary)
		{
			{ LtiClaim.MessageType, LtiMessageType.LtiDeepLinkingRequest },
			{ JsonWebTokenClaim.IssuedAt, ToUnixTime(issued ?? DateTime.UtcNow) },
			{ JsonWebTokenClaim.Expiration, ToUnixTime(expires ?? DateTime.UtcNow.AddHours(1)) },
			{
				LtiDeepLinkingClaim.DeepLinkingSettings,
				DeepLinkingSettings
			}
		};

	public static IEnumerable<Claim> GetClaims(Dictionary<string, object> claimsDictionary)
	{
		return claimsDictionary
			.SelectMany(kvp =>
			{
				var rawValue = kvp.Value;
				switch (rawValue)
				{
					case long l:
						return new List<Claim>
						{
							new(kvp.Key, l.ToString(CultureInfo.InvariantCulture), ClaimValueTypes.String, Issuer)
						};

					case int i:
						return new List<Claim>
						{
							new(kvp.Key, i.ToString(), ClaimValueTypes.String, Issuer)
						};

					case string s:
						return new List<Claim>
						{
							new(kvp.Key, s, ClaimValueTypes.String, Issuer)
						};

					case string[] strings:
						return strings.Select(v => new Claim(kvp.Key, v, ClaimValueTypes.String, Issuer));

					case List<object> _:
						return new List<Claim>
						{
							ClaimUtils.CreateJsonArrayClaim(kvp.Key, kvp.Value)
						};

					default:
						return new List<Claim>
						{
							ClaimUtils.CreateJsonClaim(kvp.Key, kvp.Value)
						};
				}
			})
			.ToList();
	}

	public static (SigningCredentials, JsonWebKeySet) GetCredentials()
	{
		var rsa = new RSACryptoServiceProvider(2048); // Generate a new 2048 bit RSA key
		var signingCredentials = new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256Signature,
			SecurityAlgorithms.Sha256Digest);
		var rsaParameters = rsa.ExportParameters(false);
		var jsonWebKeySet = GetWebKeySet(rsaParameters);
		return (signingCredentials, jsonWebKeySet);
	}

	/// <summary>
	/// https://gist.github.com/nanasess/ff8629c476edfddc1b304f9759b9eaa3
	/// </summary>
	/// <param name="keyParameters"></param>
	/// <returns></returns>
	public static JsonWebKeySet GetWebKeySet(RSAParameters keyParameters)
	{
		var e = Base64UrlEncoder.Encode(keyParameters.Exponent);
		var n = Base64UrlEncoder.Encode(keyParameters.Modulus);
		var dict = new Dictionary<string, string>
		{
			{ "e", e },
			{ "kty", "RSA" },
			{ "n", n }
		};
		var hash = SHA256.Create();
		var hashBytes = hash.ComputeHash(System.Text.Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(dict)));
		var jsonWebKey = new JsonWebKey
		{
			Kid = Base64UrlEncoder.Encode(hashBytes),
			Kty = "RSA",
			E = e,
			N = n
		};
		var jsonWebKeySet = new JsonWebKeySet();
		jsonWebKeySet.Keys.Add(jsonWebKey);
		return jsonWebKeySet;
	}

	public static JwtSecurityToken GetToken(SigningCredentials signingCredentials, string issuer = Issuer, string audience = Audience,
		IEnumerable<Claim> claims = null, DateTime? issued = null, DateTime? expires = null)
	{
		var issuedValue = issued ?? DateTime.UtcNow;
		var expiresValue = expires ?? DateTime.UtcNow.AddHours(1);
		var claimsValue = claims ?? GetClaims(GetResourceLinkClaimsDictionary(issuedValue, expiresValue));
		return new JwtSecurityToken(issuer, audience, claimsValue, issuedValue, expiresValue, signingCredentials);
	}

	public static string GetTokenString(JwtSecurityToken token)
	{
		var handler = new JwtSecurityTokenHandler();
		return handler.WriteToken(token);
	}

	public static LtiExternalProvider GetLtiExternalProvider()
	{
		return new LtiExternalProvider
		{
			Name = "Lti Unit Test Provider",
			Issuer = Issuer,
			DeploymentId = DeploymentId.ToString(),
			OauthClientId = Audience,
			OauthTokenUrl = "https://lti-ri.imsglobal.org/platforms/19/access_tokens",
			OauthAudience = "dummy",
			OpenIdConnectAuthenticationEndpoint = "dummy",
			KeySetUrl = "dummy",
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow
		};
	}
}