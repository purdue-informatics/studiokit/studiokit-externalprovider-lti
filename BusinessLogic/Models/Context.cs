﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class Context
{
	public string Id { get; set; }

	public string Title { get; set; }

	public string Label { get; set; }

	public string[] Type { get; set; }
}