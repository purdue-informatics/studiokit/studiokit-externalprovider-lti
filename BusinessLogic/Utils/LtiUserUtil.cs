namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;

public static class LtiUserUtil
{
	public const string DemoStudentUserName = "nobody";
	public const string DemoStudentDomain = "@purdue.edu";

	/// <summary>
	/// Check if the user's email matches the Brightspace demo student email. If so, add the given external id onto
	/// the email address to separate demo students of different courses. If the email does not match the Brightspace
	/// demo student email, then return it as-is.
	/// </summary>
	/// <param name="email">The user's email address</param>
	/// <param name="contextId">The external ID of the context the user is coming from</param>
	/// <returns>The student email, with the context ID appended if a demo student</returns>
	public static string AppendContextIdIfNeeded(string email, string contextId)
	{
		return email == $@"{DemoStudentUserName}{DemoStudentDomain}"
			? $@"{DemoStudentUserName}-{contextId}{DemoStudentDomain}"
			: email;
	}
}