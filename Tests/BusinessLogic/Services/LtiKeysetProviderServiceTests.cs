﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Tests;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services;

[TestClass]
public class LtiKeySetProviderServiceTests : BaseTest
{
	#region GetKeySetAsync

	[TestMethod]
	public void GetKeySetAsync_ShouldThrowWithNullExternalProvider()
	{
		var service = GetService();
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetKeySetAsync(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("ltiExternalProvider"));
	}

	[TestMethod]
	public void GetKeySetAsync_ShouldThrowWithNullKeysetUrl()
	{
		var service = GetService();
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetKeySetAsync(new LtiExternalProvider());
			}), ExceptionUtils.ArgumentNullExceptionMessage(nameof(LtiExternalProvider.KeySetUrl)));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetKeySetAsync(new LtiExternalProvider { KeySetUrl = "" });
			}), ExceptionUtils.ArgumentNullExceptionMessage(nameof(LtiExternalProvider.KeySetUrl)));
	}

	#endregion GetKeySetAsync

	#region Helpers

	private static LtiKeySetProviderService GetService()
	{
		return new LtiKeySetProviderService(() => new Mock<IHttpClient>().Object);
	}

	#endregion Helpers
}