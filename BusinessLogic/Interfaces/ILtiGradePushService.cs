﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;

public interface ILtiGradePushService
{
	Task PushGradesAsync(int gradableId, IPrincipal principal, bool shouldSave = true, CancellationToken cancellationToken = default);
}