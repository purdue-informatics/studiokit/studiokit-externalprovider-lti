﻿using Newtonsoft.Json;
using System;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

internal class OauthToken
{
	[JsonProperty("access_token")]
	public string AccessToken { get; set; }

	[JsonProperty("token_type")]
	public string TokenType { get; set; }

	[JsonProperty("expires_in")]
	public int ValidDuration { get; set; }

	public string Scope { get; set; }

	public DateTime ExpirationTime { get; set; }
}