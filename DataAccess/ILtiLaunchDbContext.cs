﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.DataAccess;

public interface ILtiLaunchDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser>
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>, new()
	where TGroupUserRole : class, IGroupUserRole, new()
	where TExternalTerm : class, IExternalTerm, new()
	where TExternalGroup : class, IExternalGroup, new()
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
{
	Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

	#region DbSets

	DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

	DbSet<Role> Roles { get; set; }

	DbSet<TExternalGroup> ExternalGroups { get; set; }

	DbSet<TExternalGroupUser> ExternalGroupUsers { get; set; }

	DbSet<TGroup> Groups { get; set; }

	DbSet<TGroupUserRole> GroupUserRoles { get; set; }

	DbSet<LtiLaunch> LtiLaunches { get; set; }

	#endregion DbSets

	void AddExternalGradable(IExternalGradable externalGradable);

	/// <summary>
	/// Return the ExternalGradable for the Gradable, if any. It is assumed there is either one or none for LTI.
	/// </summary>
	/// <returns></returns>
	Task<IExternalGradable> GetExternalGradableOrDefaultAsync(int gradableId, int? externalGroupId = null, string externalId = null,
		CancellationToken cancellationToken = default);

	/// <summary>
	/// Returns gradable after checking for null/deleted gradable, assignment/assessment, and group
	/// </summary>
	Task<IGradable> GetGradableAsync(int gradableId, CancellationToken cancellationToken = default);
}