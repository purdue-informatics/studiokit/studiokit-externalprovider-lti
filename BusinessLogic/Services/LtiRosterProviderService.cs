﻿using FluentValidation;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services;

public class LtiRosterProviderService<TUser, TExternalGroup, TExternalGroupUser> : IRosterProviderService<TExternalGroup,
	TExternalGroupUser>
	where TUser : class, IUser, new()
	where TExternalGroup : class, IExternalGroup, new()
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
{
	private readonly ILtiApiService _apiService;
	private readonly IUserFactory<TUser> _userFactory;
	private readonly IErrorHandler _errorHandler;

	public LtiRosterProviderService(ILtiApiService apiService, IUserFactory<TUser> userFactory, IErrorHandler errorHandler)
	{
		_apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
		_userFactory = userFactory ?? throw new ArgumentNullException(nameof(userFactory));
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
	}

	public async Task<IEnumerable<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>> GetRosterAsync(
		ExternalProvider.Models.ExternalProvider externalProvider,
		TExternalGroup externalGroup,
		CancellationToken cancellationToken = default)
	{
		if (externalProvider == null) throw new ArgumentNullException(nameof(externalProvider));
		if (externalGroup == null) throw new ArgumentNullException(nameof(externalGroup));
		if (!(externalProvider is LtiExternalProvider ltiExternalProvider))
			throw new ArgumentException(Strings.LtiExternalProviderRequired);

		var roster = new List<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>();
		var members = await GetMembersAsync(ltiExternalProvider, externalGroup, cancellationToken);

		// validate incoming membership data for required props
		var missingEmailMembers = members.Where(m => string.IsNullOrWhiteSpace(m.Email)).ToList();
		if (missingEmailMembers.Any())
		{
			_errorHandler.CaptureException(new ValidationException(string.Format(Strings.MembersMissingEmail,
				string.Join(", ", missingEmailMembers.Select(m => $"{m.GivenName} {m.FamilyName}")))));
		}

		foreach (var member in members.Except(missingEmailMembers))
		{
			// Skip adding member if they are not active, or already added
			if (!member.IsActive ||
				roster.Any(m => m.ExternalGroupUser.ExternalUserId.Equals(member.UserId) &&
								m.ExternalGroupUser.ExternalGroupId.Equals(externalGroup.Id))) continue;

			var username = LtiUserUtil.AppendContextIdIfNeeded(member.Email, externalGroup.ExternalId);
			roster.Add(new ExternalRosterEntry<TExternalGroup, TExternalGroupUser>
			{
				ExternalGroupUser = new TExternalGroupUser
				{
					ExternalGroupId = externalGroup.Id,
					ExternalUserId = member.UserId,
					Roles = string.Join(",", member.Roles),
					User = _userFactory.CreateUser(username, member.Email, member.GivenName, member.FamilyName)
				},
				GroupUserRoles = ClaimUtils.GetGroupRoleNames(member.Roles)
			});
		}

		// filter roster by supported roles
		roster = roster.Where(r => r.GroupUserRoles.Any()).ToList();

		return roster;
	}

	private async Task<List<Member>> GetMembersAsync(
		LtiExternalProvider ltiExternalProvider,
		TExternalGroup externalGroup,
		CancellationToken cancellationToken = default)
	{
		var responses = await _apiService.GetResultsAsync<MembershipContainer>(
			ltiExternalProvider,
			externalGroup.RosterUrl,
			cancellationToken,
			new MediaTypeWithQualityHeaderValue(MembershipContainer.ContentType));

		return responses.SelectMany(r => r.ResponseObject.Members).ToList();
	}
}