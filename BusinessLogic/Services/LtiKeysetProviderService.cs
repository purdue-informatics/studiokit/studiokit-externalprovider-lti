﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services;

public class LtiKeySetProviderService : ILtiKeySetProviderService
{
	private readonly Func<IHttpClient> _getHttpClient;

	public LtiKeySetProviderService(Func<IHttpClient> getHttpClient)
	{
		_getHttpClient = getHttpClient;
	}

	public async Task<JsonWebKeySet> GetKeySetAsync(LtiExternalProvider ltiExternalProvider, CancellationToken cancellationToken = default)
	{
		if (ltiExternalProvider == null) throw new ArgumentNullException(nameof(ltiExternalProvider));
		if (string.IsNullOrWhiteSpace(ltiExternalProvider.KeySetUrl))
			throw new ArgumentNullException(nameof(ltiExternalProvider.KeySetUrl));

		try
		{
			var httpClient = _getHttpClient();
			var jsonWebKeySetString = await httpClient.GetStringAsync(new Uri(ltiExternalProvider.KeySetUrl), cancellationToken);
			return JsonConvert.DeserializeObject<JsonWebKeySet>(jsonWebKeySetString);
		}
		catch (Exception e)
		{
			throw new UnauthorizedException(Strings.PlatformPublicKeysNotFound, e);
		}
	}
}