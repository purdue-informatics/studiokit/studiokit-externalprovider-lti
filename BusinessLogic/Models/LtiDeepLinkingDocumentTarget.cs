﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class LtiDeepLinkingDocumentTarget
{
	public static string Iframe = "iframe";

	public static string Window = "window";

	public static string Embed = "embed";
}