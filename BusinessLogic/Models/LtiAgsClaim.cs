﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// LTI Assignment and Grade Services Claim, "lti-ags"
/// </summary>
public static class LtiAgsClaim
{
	public static string Endpoint = "https://purl.imsglobal.org/spec/lti-ags/claim/endpoint";
}