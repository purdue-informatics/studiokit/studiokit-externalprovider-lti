﻿using System.ComponentModel.DataAnnotations;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// See https://www.imsglobal.org/spec/lti-ags/v2p0/#creating-a-new-line-item
/// and https://www.imsglobal.org/spec/lti-ags/v2p0/#updating-a-line-item
/// </summary>
public class LineItem
{
	public const string ContentType = "application/vnd.ims.lis.v2.lineitem+json";

	/// <summary>
	/// Platform-defined identifier.
	/// </summary>
	public string Id { get; set; }

	/// <summary>
	/// The maximum score for this line item.
	/// Maximum score MUST be a numeric non-null value, strictly greater than 0.
	/// </summary>
	[Required]
	public decimal ScoreMaximum { get; set; }

	/// <summary>
	/// The label is a short string with a human readable text for the line item.
	/// It MUST be specified and not blank when posted by the tool.
	/// </summary>
	[Required]
	public string Label { get; set; }

	/// <summary>
	/// <para>Tool-defined value pointing to a resource, e.g. a gradable.</para>
	/// <para>CANNOT be changed by the tool after the line item exists.</para>
	/// </summary>
	public string ResourceId { get; set; }

	/// <summary>
	/// <para>Platform-defined resource link identifier, e.g. an LTI Deep Link.</para>
	/// <para>CANNOT be changed by the tool after the line item exists.</para>
	/// </summary>
	public string ResourceLinkId { get; set; }

	/// <summary>
	/// <para>Tool-defined value set at time of creation.</para>
	/// <para>CANNOT be changed by the tool after the line item exists.</para>
	/// </summary>
	public string Tag { get; set; }
}