﻿using Newtonsoft.Json;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class BrightspaceClaim
{
	public const string BrightspaceClaimType = "http://www.brightspace.com";

	public const string BrightspaceUsername = "BrightspaceUsername";

	public const string BrightspaceOrgDefinedId = "BrightspaceOrgDefinedId";

	public const string BrightspaceImpersonationOrgDefinedId = "BrightspaceImpersonationOrgDefinedId";

	[JsonProperty("tenant_id")]
	public string TenantId { get; set; }

	[JsonProperty("org_defined_id")]
	public string OrgDefinedId { get; set; }

	[JsonProperty("user_id")]
	public long UserId { get; set; }

	[JsonProperty("username")]
	public string Username { get; set; }

	[JsonProperty("impersonation_role")]
	public string[] ImpersonationRole { get; set; }

	[JsonProperty("impersonation_org_defined_id")]
	public string ImpersonationOrgDefinedId { get; set; }

	[JsonProperty("ResourceLink.id.history")]
	public string ResourceLinkIdHistory { get; set; }

	[JsonProperty("Context.id.history")]
	public string ContextIdHistory { get; set; }
}