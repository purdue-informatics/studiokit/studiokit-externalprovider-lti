﻿using StudioKit.Data.Entity.Identity;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// Only "core" roles are accounted for at this time.
/// See https://www.imsglobal.org/spec/lti/v1p3/#role-vocabularies
/// </summary>
public static class LtiRole
{
	public static string SystemRolePrefix = "http://purl.imsglobal.org/vocab/lis/v2/system/person#";

	public static string InstitutionRolePrefix = "http://purl.imsglobal.org/vocab/lis/v2/institution/person#";

	public static string ContextRolePrefix = "http://purl.imsglobal.org/vocab/lis/v2/membership#";

	public static string ContextSubRolePrefix = "http://purl.imsglobal.org/vocab/lis/v2/membership/";

	#region System Roles

	public static string SystemAdministrator = "http://purl.imsglobal.org/vocab/lis/v2/system/person#Administrator ";

	#endregion System Roles

	#region Institution Roles

	public static string InstitutionAdministrator = "http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator";

	public static string InstitutionStaff = "http://purl.imsglobal.org/vocab/lis/v2/institution/person#Staff";

	public static string InstitutionStudent = "http://purl.imsglobal.org/vocab/lis/v2/institution/person#Student";

	public static string InstitutionInstructor = "http://purl.imsglobal.org/vocab/lis/v2/institution/person#Instructor";

	public static string InstitutionLearner = "http://purl.imsglobal.org/vocab/lis/v2/institution/person#Learner";

	#endregion Institution Roles

	#region Core Principal Context Roles

	public static string MembershipAdministrator = "http://purl.imsglobal.org/vocab/lis/v2/membership#Administrator";

	public static string MembershipContentDeveloper = "http://purl.imsglobal.org/vocab/lis/v2/membership#ContentDeveloper";

	public static string MembershipInstructor = "http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor";

	public static string MembershipLearner = "http://purl.imsglobal.org/vocab/lis/v2/membership#Learner";

	public static string MembershipMentor = "http://purl.imsglobal.org/vocab/lis/v2/membership#Mentor";

	#endregion Core Principal Context Roles

	#region Context Sub-Roles

	public static string MembershipInstructorGrader = "http://purl.imsglobal.org/vocab/lis/v2/membership/Instructor#Grader";

	#endregion Context Sub-Roles

	#region Simple Context Roles (Deprecated)

	public static string Administrator = "Administrator";

	public static string ContentDeveloper = "ContentDeveloper";

	public static string Instructor = "Instructor";

	public static string Learner = "Learner";

	public static string Mentor = "Mentor";

	public static List<string> SimpleContextRoles = new List<string>
	{
		Administrator,
		ContentDeveloper,
		Instructor,
		Learner,
		Mentor
	};

	#endregion Simple Context Roles (Deprecated)

	public static Dictionary<string, string> ApplicationRoleMap => new Dictionary<string, string>
	{
		{ MembershipAdministrator, BaseRole.Creator },
		{ MembershipContentDeveloper, BaseRole.Creator },
		{ MembershipInstructor, BaseRole.Creator },

		{ Administrator, BaseRole.Creator },
		{ ContentDeveloper, BaseRole.Creator },
		{ Instructor, BaseRole.Creator }
	};

	public static Dictionary<string, string> ContextRoleMap => new Dictionary<string, string>
	{
		{ MembershipAdministrator, BaseRole.GroupOwner },
		{ MembershipContentDeveloper, BaseRole.GroupOwner },
		{ MembershipInstructor, BaseRole.GroupOwner },
		{ MembershipInstructorGrader, BaseRole.GroupGrader },
		{ MembershipLearner, BaseRole.GroupLearner },

		{ Administrator, BaseRole.GroupOwner },
		{ ContentDeveloper, BaseRole.GroupOwner },
		{ Instructor, BaseRole.GroupOwner },
		{ Learner, BaseRole.GroupLearner }
	};

	public static Dictionary<string, List<string>> ContextSubRoles =>
		new Dictionary<string, List<string>>
		{
			{
				MembershipInstructor,
				new List<string>
				{
					MembershipInstructorGrader
				}
			}
		};
}