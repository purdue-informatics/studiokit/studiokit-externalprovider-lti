﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// LTI Deep Linking Claim, "lti-dl"
/// </summary>
public static class LtiDeepLinkingClaim
{
	public static string DeepLinkingSettings = "https://purl.imsglobal.org/spec/lti-dl/claim/deep_linking_settings";

	public static string Data = "https://purl.imsglobal.org/spec/lti-dl/claim/data";

	public static string ContentItems = "https://purl.imsglobal.org/spec/lti-dl/claim/content_items";

	public static string Msg = "https://purl.imsglobal.org/spec/lti-dl/claim/msg";

	public static string Log = "https://purl.imsglobal.org/spec/lti-dl/claim/log";

	public static string ErrorMsg = "https://purl.imsglobal.org/spec/lti-dl/claim/errormsg";

	public static string ErrorLog = "https://purl.imsglobal.org/spec/lti-dl/claim/errorlog";
}