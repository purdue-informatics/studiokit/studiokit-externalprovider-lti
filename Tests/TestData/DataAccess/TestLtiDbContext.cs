﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;

public class TestLtiLaunchDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
	ILtiLaunchDbContext<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser>
{
	public TestLtiLaunchDbContext()
	{
	}

	public TestLtiLaunchDbContext(DbContextOptions options) : base(options)
	{
	}

	public TestLtiLaunchDbContext(DbConnection existingConnection) : base(existingConnection)
	{
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		builder
			.Entity<TestExternalGroup>()
			.HasOne(l => (TestUser)l.User);

		builder
			.Entity<TestExternalGroupUser>()
			.HasOne(l => (TestUser)l.User);

		builder
			.Entity<LtiLaunch>()
			.HasOne(l => (TestUser)l.User);
	}

	public void AddExternalGradable(IExternalGradable externalGradable)
	{
		ExternalGroupAssignments.Add((TestExternalGroupAssignment)externalGradable);
	}

	public async Task<IExternalGradable> GetExternalGradableOrDefaultAsync(int gradableId, int? externalGroupId = null,
		string externalId = null, CancellationToken cancellationToken = default)
	{
		return await ExternalGroupAssignments.FirstOrDefaultAsync(ega =>
				ega.GroupAssignmentId.Equals(gradableId) &&
				(!externalGroupId.HasValue || ega.ExternalGroupId.Equals(externalGroupId.Value)) &&
				(externalId == null || externalId == string.Empty || ega.ExternalId.Equals(externalId)),
			cancellationToken);
	}

	public async Task<IGradable> GetGradableAsync(int gradableId, CancellationToken cancellationToken)
	{
		var gradable = await GroupAssignments.SingleOrDefaultAsync(ga => ga.Id == gradableId && !ga.IsDeleted, cancellationToken);
		if (gradable == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(TestGroupAssignment), gradableId));

		var assignment =
			await Assignments.SingleOrDefaultAsync(a => a.Id.Equals(gradable.AssignmentId) && !a.IsDeleted,
				cancellationToken);
		if (assignment == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(TestAssignment), gradable.AssignmentId));

		var group =
			await Groups.SingleOrDefaultAsync(g => g.Id.Equals(gradable.GroupId) && !g.IsDeleted,
				cancellationToken);
		if (group == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(TestGroup), gradable.GroupId));

		return gradable;
	}

	public DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

	public DbSet<TestGroup> Groups { get; set; }

	public DbSet<TestExternalGroup> ExternalGroups { get; set; }

	public DbSet<TestExternalGroupUser> ExternalGroupUsers { get; set; }

	public DbSet<TestGroupUserRole> GroupUserRoles { get; set; }

	public DbSet<LtiLaunch> LtiLaunches { get; set; }

	public DbSet<TestGroupAssignment> GroupAssignments { get; set; }

	public DbSet<TestAssignment> Assignments { get; set; }

	public DbSet<TestExternalGroupAssignment> ExternalGroupAssignments { get; set; }
}