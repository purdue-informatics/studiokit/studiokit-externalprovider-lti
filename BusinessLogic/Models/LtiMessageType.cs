﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class LtiMessageType
{
	public static string LtiResourceLinkRequest = "LtiResourceLinkRequest";

	public static string LtiDeepLinkingRequest = "LtiDeepLinkingRequest";

	public static string LtiDeepLinkingResponse = "LtiDeepLinkingResponse";
}