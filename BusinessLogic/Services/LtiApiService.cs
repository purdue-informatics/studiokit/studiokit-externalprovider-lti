﻿using Newtonsoft.Json;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.Security.Common;
using StudioKit.TransientFaultHandling.Http.Constants;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using JsonException = Newtonsoft.Json.JsonException;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services;

public class LtiApiService : ILtiApiService
{
	private static readonly ConcurrentDictionary<int, OauthToken> OauthTokenCache;
	private readonly IJsonWebTokenService _tokenService;
	private readonly Func<IHttpClient> _getHttpClient;
	private readonly IErrorHandler _errorHandler;

	static LtiApiService()
	{
		OauthTokenCache = new ConcurrentDictionary<int, OauthToken>();
	}

	public static void ClearCache()
	{
		OauthTokenCache.Clear();
	}

	public LtiApiService(IJsonWebTokenService tokenService, Func<IHttpClient> getHttpClient, IErrorHandler errorHandler)
	{
		_tokenService = tokenService;
		_getHttpClient = getHttpClient;
		_errorHandler = errorHandler;
	}

	public async Task<(T ResponseObject, HttpResponseMessage ResponseMessage)> GetResultAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
	{
		if (ltiExternalProvider == null)
			throw new ArgumentNullException(nameof(ltiExternalProvider));

		if (url == null)
			throw new ArgumentNullException(nameof(url));

		var client = _getHttpClient();
		var token = await GetAuthTokenAsync(ltiExternalProvider, cancellationToken);
		var authorizationHeader = new AuthenticationHeaderValue(AuthorizationHeaderType.Bearer, token);

		try
		{
			var responseMessage = await client
				.GetAsync(new Uri(url), cancellationToken, authorizationHeader, acceptHeaderValue)
				.ConfigureAwait(false);
			var responseBody = await responseMessage.Content.ReadAsStringAsync();
			var responseObjects = JsonConvert.DeserializeObject<T>(responseBody);
			return (responseObjects, responseMessage);
		}
		catch (HttpRequestException e)
		{
			throw new LtiException(Strings.LtiApiRequestError, e);
		}
		catch (JsonException e)
		{
			throw new LtiException(Strings.LtiJsonError, e);
		}
	}

	public async Task<List<(T ResponseObject, HttpResponseMessage ResponseMessage)>> GetResultsAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
	{
		var results = new List<(T ResponseObject, HttpResponseMessage ResponseMessage)>();
		var batchUrl = url;

		while (batchUrl != null)
		{
			var (responseObject, responseMessage) = await GetResultAsync<T>(
				ltiExternalProvider,
				batchUrl,
				cancellationToken,
				acceptHeaderValue);

			results.Add((responseObject, responseMessage));

			responseMessage.Headers.TryGetValues("Link", out var linkHeaderValues);
			var linkHeaderList = linkHeaderValues != null ? linkHeaderValues.ToList() : new List<string>();
			var linkValue = linkHeaderList.FirstOrDefault();
			// when the "Link" value points to the next page, it is in the format:
			// "<url>;rel="next"" or "<url>; rel="next"" (with a space)
			const string relString = "rel=\"next\"";
			if (linkValue != null && linkValue.EndsWith(relString))
			{
				var semicolonIndex = linkValue.LastIndexOf(";", StringComparison.Ordinal);
				var newBatchUrl = linkValue
					.Substring(1, semicolonIndex - 2)
					// "Link" header is returning the URL without https
					.Replace("http://", "https://");
				// sometimes Brightspace still sends a "Link" header even when there are no more results
				batchUrl = newBatchUrl != batchUrl ? newBatchUrl : null;
			}
			else
			{
				batchUrl = null;
			}
		}

		return results;
	}

	public async Task<(T ResponseObject, HttpResponseMessage ResponseMessage)> PostAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
	{
		if (ltiExternalProvider == null)
			throw new ArgumentNullException(nameof(ltiExternalProvider));

		if (url == null)
			throw new ArgumentNullException(nameof(url));

		if (getHttpContent == null)
			throw new ArgumentNullException(nameof(getHttpContent));

		var client = _getHttpClient();
		var token = await GetAuthTokenAsync(ltiExternalProvider, cancellationToken);
		var authorizationHeader = new AuthenticationHeaderValue(AuthorizationHeaderType.Bearer, token);

		try
		{
			var responseMessage = await client
				.PostAsync(new Uri(url), getHttpContent, cancellationToken, authorizationHeader, acceptHeaderValue)
				.ConfigureAwait(false);
			var responseBody = await responseMessage.Content.ReadAsStringAsync();
			var responseObjects = JsonConvert.DeserializeObject<T>(responseBody);
			return (responseObjects, responseMessage);
		}
		catch (HttpRequestException e)
		{
			throw new LtiException(Strings.LtiApiRequestError, e);
		}
		catch (JsonException e)
		{
			throw new LtiException(Strings.LtiJsonError, e);
		}
	}

	public async Task<(T ResponseObject, HttpResponseMessage ResponseMessage)> PutAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
	{
		if (ltiExternalProvider == null)
			throw new ArgumentNullException(nameof(ltiExternalProvider));

		if (url == null)
			throw new ArgumentNullException(nameof(url));

		if (getHttpContent == null)
			throw new ArgumentNullException(nameof(getHttpContent));

		var client = _getHttpClient();
		var token = await GetAuthTokenAsync(ltiExternalProvider, cancellationToken);
		var authorizationHeader = new AuthenticationHeaderValue(AuthorizationHeaderType.Bearer, token);

		try
		{
			var responseMessage = await client
				.PutAsync(new Uri(url), getHttpContent, cancellationToken, authorizationHeader, acceptHeaderValue)
				.ConfigureAwait(false);
			var responseBody = await responseMessage.Content.ReadAsStringAsync();
			var responseObjects = JsonConvert.DeserializeObject<T>(responseBody);
			return (responseObjects, responseMessage);
		}
		catch (HttpRequestException e)
		{
			throw new LtiException(Strings.LtiApiRequestError, e);
		}
		catch (JsonException e)
		{
			throw new LtiException(Strings.LtiJsonError, e);
		}
	}

	public async Task<string> GetAuthTokenAsync(LtiExternalProvider provider, CancellationToken cancellationToken = default)
	{
		if (provider == null)
			throw new ArgumentNullException(nameof(provider));

		OauthTokenCache.TryGetValue(provider.Id, out var providerToken);

		//If the token has less than 5 minutes left, go ahead and get a new one.
		if (providerToken != null && DateTime.UtcNow.AddMinutes(5) < providerToken.ExpirationTime)
			return providerToken.AccessToken;

		var client = _getHttpClient();

		var claims = new List<Claim>
		{
			new Claim(JsonWebTokenClaim.Subject, provider.OauthClientId),
			new Claim(JsonWebTokenClaim.Audience, provider.OauthAudience),
			new Claim(JsonWebTokenClaim.JwtId, Guid.NewGuid().ToString())
		};
		var authRequestToken = await _tokenService.CreateJwtAsync(provider, claims, cancellationToken);
		var handler = new JwtSecurityTokenHandler();
		var tokenString = handler.WriteToken(authRequestToken);
		var requestStartTime = DateTime.UtcNow;
		OauthToken responseToken;

		try
		{
			var json = await client.PostForStringAsync(
					new Uri(provider.OauthTokenUrl),
					uri => new FormUrlEncodedContent(new[]
					{
						new KeyValuePair<string, string>(OidcAuthorizationRequestKey.ClientAssertion, tokenString),
						new KeyValuePair<string, string>(OidcAuthorizationRequestKey.ClientAssertionType,
							OidcAuthorizationRequestKey.JwtBearerAssertionType),
						new KeyValuePair<string, string>(OAuthRequestKey.GrantType,
							OAuthGrantType.ClientCredentials),
						new KeyValuePair<string, string>(OAuthRequestKey.Scope,
							$"{LtiNrpsScope.ContextMembershipReadOnly} {LtiAgsScope.LineItem} {LtiAgsScope.ResultReadOnly} {LtiAgsScope.Score}")
					}),
					cancellationToken)
				.ConfigureAwait(false);

			responseToken = JsonConvert.DeserializeObject<OauthToken>(json);
		}
		catch (HttpRequestException e)
		{
			//If the original token hasn't expired, just use that
			if (providerToken != null && DateTime.UtcNow < providerToken.ExpirationTime)
			{
				_errorHandler.CaptureException(e);
				return providerToken.AccessToken;
			}

			throw new LtiException(Strings.LtiOauthRequestError, e);
		}
		catch (JsonException e)
		{
			throw new LtiException(Strings.LtiJsonError, e);
		}

		responseToken.ExpirationTime = requestStartTime.AddSeconds(responseToken.ValidDuration);

		OauthTokenCache[provider.Id] = responseToken;

		return responseToken.AccessToken;
	}
}