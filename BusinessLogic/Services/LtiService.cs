﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StudioKit.Caching.Interfaces;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Security.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services;

public class LtiService<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser, TExternalGradable> : ILtiService
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>, new()
	where TGroupUserRole : class, IGroupUserRole, new()
	where TExternalTerm : class, IExternalTerm, new()
	where TExternalGroup : class, IExternalGroup, new()
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	where TExternalGradable : class, IExternalGradable, new()
{
	// Redis requires a value, but methods in this class are only concerned with the
	// existence of certain keys. So we use this for the value.

	private readonly ILtiLaunchDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> _dbContext;
	private readonly IAsyncCacheClient _cacheClient;
	private readonly IJsonWebTokenService _jsonWebTokenService;
	private readonly IGroupUserRoleService<TGroupUserRole> _groupUserRoleService;

	public LtiService(
		ILtiLaunchDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> dbContext,
		IJsonWebTokenService jsonWebTokenService,
		IAsyncCacheClient cacheClient,
		IGroupUserRoleService<TGroupUserRole> groupUserRoleService
	)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_jsonWebTokenService = jsonWebTokenService ?? throw new ArgumentNullException(nameof(jsonWebTokenService));
		_cacheClient = cacheClient ?? throw new ArgumentNullException(nameof(cacheClient));
		_groupUserRoleService = groupUserRoleService ?? throw new ArgumentNullException(nameof(groupUserRoleService));
	}

	public async Task<string> PrepareOidcRedirectAsync(IEnumerable<KeyValuePair<string, string>> requestData, string baseUri,
		CancellationToken cancellationToken = default)
	{
		var fixedQueryStringParams = new Dictionary<string, string>
		{
			{ OidcAuthorizationRequestKey.Scope, OidcScope.OpenId },
			{ OidcAuthorizationRequestKey.ResponseType, OidcResponseType.IdToken },
			{ OidcAuthorizationRequestKey.ResponseMode, OidcResponseMode.FormPost },
			{ OidcAuthorizationRequestKey.Prompt, OidcPrompt.None },
			// this redirect URI must have been registered on the platform, aka the LMS
			{ OidcAuthorizationRequestKey.RedirectUri, baseUri }
		};
		var guidQueryStringParams = new List<string>
		{
			OidcAuthorizationRequestKey.State,
			OidcAuthorizationRequestKey.Nonce
		};
		var providerBasedQueryStringParams = new Dictionary<string, Func<LtiExternalProvider, string>>
		{
			// this value will have been provided by the platform and will have been stored in the LtiExternalProvider record
			{ OidcAuthorizationRequestKey.ClientId, p => p.OauthClientId },
		};
		// These get reflected back as they were sent in
		var reflectQueryStringParams = new List<string>
		{
			OidcAuthorizationRequestKey.LoginHint,
			OidcAuthorizationRequestKey.LtiMessageHint,
			OidcAuthorizationRequestKey.LtiDeploymentId
		};

		var requestDataList = requestData.ToList();

		var issuer = requestDataList.Single(rd => rd.Key == JsonWebTokenClaim.Issuer).Value;
		var deploymentId = requestDataList.Single(rd => rd.Key == OidcAuthorizationRequestKey.LtiDeploymentId).Value;
		var provider = await _dbContext.LtiExternalProviders.SingleAsync(p => p.Issuer == issuer && p.DeploymentId == deploymentId,
			cancellationToken);

		var queryString = HttpUtility.ParseQueryString(string.Empty);
		fixedQueryStringParams.Select(kvp => new NameValueCollection { { kvp.Key, kvp.Value } })
			.Union(guidQueryStringParams.Select(p => new NameValueCollection { { p, Guid.NewGuid().ToString() } }))
			.Union(reflectQueryStringParams.Select(p => new NameValueCollection { { p, requestDataList.Single(rd => rd.Key == p).Value } }))
			.Union(providerBasedQueryStringParams.Select(kvp => new NameValueCollection { { kvp.Key, kvp.Value(provider) } }))
			.ToList()
			.ForEach(p => queryString.Add(p));

		var ltiStateKey = $"{CacheConstants.LtiStateCachePrefix}{queryString.Get("state")}";
		await _cacheClient.PutAsync(ltiStateKey, CacheConstants.ExistentialCacheValue, TimeSpan.FromSeconds(60));

		var platformOpenIdAuthEndpoint = provider.OpenIdConnectAuthenticationEndpoint;
		var redirectUri = $"{platformOpenIdAuthEndpoint}?{queryString}";
		return redirectUri;
	}

	public string GetLaunchPresentationReturnUrl(string tokenString)
	{
		if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));
		var token = new JwtSecurityToken(tokenString);
		var claims = token.Claims.ToList();
		var launchPresentationClaim = claims.SingleOrDefault(c => c.Type == LtiClaim.LaunchPresentation);
		if (launchPresentationClaim == null)
			return null;
		var launchPresentation = JsonConvert.DeserializeObject<LaunchPresentation>(launchPresentationClaim.Value);
		return launchPresentation.ReturnUrl;
	}

	public async Task<ExternalLoginInfo> GetLoginInfoAsync(string tokenString, CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));

		await _jsonWebTokenService.AssertIsTokenValidAsync(tokenString, cancellationToken);

		var token = new JwtSecurityToken(tokenString);
		var claims = token.Claims.ToList();
		AssertLaunchClaimsAreValid(claims);
		var externalProvider = await _jsonWebTokenService.GetTokenProviderAsync(token, cancellationToken);

		var updatedClaims = ClaimUtils.UpdateExternalClaimsIssuer(claims);
		var externalUserId = updatedClaims.Single(x => x.Type == JsonWebTokenClaim.Subject).Value;
		var email = updatedClaims.Single(x => x.Type == OidcTokenClaim.Email).Value;

		var commonClaims = ClaimUtils.GetCommonClaims(updatedClaims);
		var brightspaceClaims = ClaimUtils.GetBrightspaceClaims(updatedClaims);
		var applicationRoleClaims = ClaimUtils.GetApplicationRoleClaims(updatedClaims);
		var combinedClaims = updatedClaims
			.Concat(commonClaims)
			.Concat(brightspaceClaims)
			.Concat(applicationRoleClaims)
			.ToList();

		var contextClaimValue = updatedClaims.Single(c => c.Type.Equals(LtiClaim.Context)).Value;
		var context = JsonConvert.DeserializeObject<Context>(contextClaimValue);

		var username = LtiUserUtil.AppendContextIdIfNeeded(email, context.Id);
		combinedClaims.Add(new Claim(ClaimTypes.Name, username, ClaimValueTypes.String, ClaimsIdentity.DefaultIssuer));

		return new ExternalLoginInfo(
			new ClaimsPrincipal(new ClaimsIdentity(combinedClaims)),
			string.Format(Strings.LoginProviderFormat, externalProvider.Id),
			externalUserId,
			externalProvider.Name);
	}

	public async Task<LaunchResponse> GetLaunchResponseAsync(string tokenString, string userId, List<Claim> claims,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));
		if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
		await _jsonWebTokenService.AssertIsTokenValidAsync(tokenString, cancellationToken);
		AssertLaunchClaimsAreValid(claims);

		var token = new JwtSecurityToken(tokenString);
		var externalProvider = await _jsonWebTokenService.GetTokenProviderAsync(token, cancellationToken);
		var externalUserId = claims.Single(x => x.Type == JsonWebTokenClaim.Subject).Value;

		// Get list of Context roles from claims
		var externalRoleNames = ClaimUtils.GetExternalRoleNames(claims);
		var externalRolesString = string.Join(",", externalRoleNames);
		var groupRoleNames = ClaimUtils.GetGroupRoleNames(claims);
		var isGroupOwner = groupRoleNames.Any(r => r.Equals(BaseRole.GroupOwner));

		// Get LTI Context
		var contextClaimValue = claims.Single(c => c.Type.Equals(LtiClaim.Context)).Value;
		var context = JsonConvert.DeserializeObject<Context>(contextClaimValue);

		// Get MessageType
		var messageTypeClaimValue = claims.Single(c => c.Type.Equals(LtiClaim.MessageType)).Value;

		// Get Names and Role Provisioning Service
		var namesRoleServiceClaimValue = claims.SingleOrDefault(c => c.Type.Equals(LtiNrpsClaim.NamesRoleService));
		var namesRoleService = namesRoleServiceClaimValue != null
			? JsonConvert.DeserializeObject<NamesRoleService>(namesRoleServiceClaimValue.Value)
			: null;

		// Get Endpoint
		var endpointClaimValue = claims.SingleOrDefault(c => c.Type.Equals(LtiAgsClaim.Endpoint));
		var endpoint = endpointClaimValue != null
			? JsonConvert.DeserializeObject<Endpoint>(endpointClaimValue.Value)
			: null;

		// Load ExternalGroup connected to the Context (if any)
		var externalGroup = await _dbContext.ExternalGroups
			// TODO: revert to SingleOrDefaultAsync after adding unique index
			.FirstOrDefaultAsync(g =>
				g.ExternalId.Equals(context.Id) &&
				g.ExternalProviderId.Equals(externalProvider.Id), cancellationToken);

		// If a student launches the tool but the instructor never completed the group creation, don't do anything
		if (!isGroupOwner && externalGroup == null)
			throw new ForbiddenException(Strings.CourseNotAvailable);

		// Get TargetLinkUri
		var targetLinkUri = claims.Single(c => c.Type.Equals(LtiClaim.TargetLinkUri)).Value;

		// Launching to view a created Resource Link
		if (messageTypeClaimValue.Equals(LtiMessageType.LtiResourceLinkRequest) && externalGroup != null)
		{
			var group = await _dbContext.Groups
				.SingleAsync(g => g.Id.Equals(externalGroup.GroupId), cancellationToken);

			if (group.IsDeleted)
			{
				// If a student accesses a deleted group, do not proceed
				if (!isGroupOwner)
					throw new ForbiddenException(Strings.CourseNotAvailable);

				// Restore the group
				group.IsDeleted = false;
			}

			// Update ExternalGroup details
			externalGroup.Name = context.Title ?? context.Id;
			externalGroup.Description = context.Label;
			externalGroup.RosterUrl = namesRoleService?.ContextMembershipsUrl;
			externalGroup.GradesUrl = endpoint?.LineItems;

			// Add or update ExternalGroupUser
			var externalGroupUser = await _dbContext.ExternalGroupUsers
				.SingleOrDefaultAsync(egu =>
					egu.ExternalGroupId.Equals(externalGroup.Id) &&
					egu.ExternalUserId.Equals(externalUserId) &&
					egu.UserId.Equals(userId), cancellationToken);
			if (externalGroupUser == null)
			{
				externalGroupUser = new TExternalGroupUser
				{
					UserId = userId,
					Roles = externalRolesString,
					ExternalUserId = externalUserId,
					ExternalGroupId = externalGroup.Id
				};
				_dbContext.ExternalGroupUsers.Add(externalGroupUser);
			}
			else
			{
				externalGroupUser.Roles = externalRolesString;
			}

			// Validate that the TargetLinkUri points to the correct course
			var targetLinkUriParts = targetLinkUri.Split('/').ToList();
			var indexOfCourse = targetLinkUriParts.IndexOf("courses");
			if (indexOfCourse > -1 && targetLinkUriParts.Count > indexOfCourse + 1)
			{
				var didParseLinkCourseId = int.TryParse(targetLinkUriParts.ElementAt(indexOfCourse + 1), out var linkCourseId);
				if (didParseLinkCourseId && linkCourseId != externalGroup.GroupId)
				{
					var resourceLinkClaim = claims.Single(c => c.Type.Equals(LtiClaim.ResourceLink));
					var resourceLink = JsonConvert.DeserializeObject<ResourceLink>(resourceLinkClaim.Value);
					var resourceLinkName = resourceLink.Title;
					throw new ForbiddenException(string.Format(
						isGroupOwner
							? Strings.LtiDeepLinkInvalidCourseInstructorError
							: Strings.LtiDeepLinkInvalidCourseError,
						resourceLinkName));
				}
			}

			// Create/Update ExternalGradable for ExternalGroup + LineItem (if needed)
			if (endpoint != null && !string.IsNullOrWhiteSpace(endpoint.LineItem))
			{
				// Try to get GradableId from TargetLinkUri
				// ASSUMPTION: if the TargetLinkUri ends with an integer, it is for a Gradable
				var didParse = int.TryParse(targetLinkUriParts.Last(), out var gradableId);
				if (didParse)
				{
					// Check for ExternalGradable, should only have one per Gradable + ExternalGroup
					var externalGradable =
						await _dbContext.GetExternalGradableOrDefaultAsync(gradableId, externalGroup.Id,
							cancellationToken: cancellationToken);

					if (externalGradable == null)
					{
						// Create ExternalGradable
						_dbContext.AddExternalGradable(new TExternalGradable
						{
							ExternalGroupId = externalGroup.Id,
							GradableId = gradableId,
							ExternalId = endpoint.LineItem
						});
					}
					else
					{
						// Update ExternalId
						externalGradable.ExternalId = endpoint.LineItem;
					}
				}
			}

			// Add GroupUserRoles (if needed)
			var roles = await _dbContext.Roles.ToListAsync(cancellationToken);
			var existingGroupUserRoles = (await _dbContext.GroupUserRoles
					.Where(gur =>
						gur.GroupId.Equals(externalGroup.GroupId) &&
						gur.UserId.Equals(userId))
					.ToListAsync(cancellationToken))
				.Select(gur => roles.Single(r => r.Id.Equals(gur.RoleId)).Name)
				.ToList();
			var groupUserRolesToAdd = groupRoleNames
				.Where(r => !existingGroupUserRoles.Contains(r))
				.Select(n => new TGroupUserRole
				{
					GroupId = externalGroup.GroupId,
					UserId = userId,
					RoleId = roles.Single(r => r.Name.Equals(n)).Id,
					IsExternal = true
				})
				.ToList();

			// Save and return
			await _groupUserRoleService.AddRangeAsync(groupUserRolesToAdd, new SystemPrincipal(), cancellationToken);

			return new LaunchResponse
			{
				TargetLinkUri = targetLinkUri,
				GroupId = externalGroup.GroupId
			};
		}

		//
		// Launching to create a basic Resource Link or a Deep Link
		//

		// Get custom date claims
		var customClaimValue = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.Custom));
		var customClaims = customClaimValue != null ? JsonConvert.DeserializeObject<LtiCustomClaim>(customClaimValue.Value) : null;
		var courseStartDate =
			string.IsNullOrEmpty(customClaims?.CourseSectionStartDate) ||
			customClaims.CourseSectionStartDate == LtiCustomClaim.CourseSectionStartDateLisVariable
				? (DateTime?)null
				: DateTime.Parse(customClaims.CourseSectionStartDate, null, DateTimeStyles.AdjustToUniversal);
		var courseEndDate =
			string.IsNullOrEmpty(customClaims?.CourseSectionEndDate) ||
			customClaims.CourseSectionEndDate == LtiCustomClaim.CourseSectionEndDateLisVariable
				? (DateTime?)null
				: DateTime.Parse(customClaims.CourseSectionEndDate, null, DateTimeStyles.AdjustToUniversal);

		// If an end date claim exists, ensure it ends at 59 seconds on the minute for consistency with other app dates
		if (courseEndDate.HasValue && courseEndDate.Value.Second != 59)
		{
			var ltiEndDate = courseEndDate.Value;
			courseEndDate = ltiEndDate.AddSeconds(-1 * ltiEndDate.Second - 1);
		}

		// Get Version claim
		var versionClaim = claims.Single(c => c.Type.Equals(LtiClaim.Version)).Value;

		// Get LaunchPresentation claim
		var presentationClaimValue = claims.Single(c => c.Type.Equals(LtiClaim.LaunchPresentation)).Value;
		var presentation = JsonConvert.DeserializeObject<LaunchPresentation>(presentationClaimValue);

		// Create LtiLaunch
		var ltiLaunch = new LtiLaunch
		{
			CreatedById = userId,
			ExternalProviderId = externalProvider.Id,
			ExternalId = context.Id,
			Name = context.Title ?? context.Id,
			Description = context.Label,
			CourseSectionStartDate = courseStartDate,
			CourseSectionEndDate = courseEndDate,
			Roles = externalRolesString,
			LtiVersion = versionClaim,
			ReturnUrl = presentation.ReturnUrl,
			RosterUrl = namesRoleService?.ContextMembershipsUrl,
			GradesUrl = endpoint?.LineItems
		};

		// Get Deep Linking Settings to store on the LtiLaunch
		if (messageTypeClaimValue.Equals(LtiMessageType.LtiDeepLinkingRequest))
		{
			var deepLinkingSettingsClaim = claims.Single(c => c.Type.Equals(LtiDeepLinkingClaim.DeepLinkingSettings));
			var deepLinkingSettings = JsonConvert.DeserializeObject<DeepLinkingSettings>(deepLinkingSettingsClaim.Value);

			ltiLaunch.DeepLinkingReturnUrl = deepLinkingSettings.DeepLinkReturnUrl;
			ltiLaunch.DeepLinkingData = deepLinkingSettings.Data;

			// Track if the LTI Context is already associated with an existing Group
			if (externalGroup != null)
			{
				ltiLaunch.GroupId = externalGroup.GroupId;
			}
		}

		// Save and return
		_dbContext.LtiLaunches.Add(ltiLaunch);
		await _dbContext.SaveChangesAsync(cancellationToken);
		return new LaunchResponse
		{
			TargetLinkUri = targetLinkUri,
			LtiLaunchId = ltiLaunch.Id
		};
	}

	/// <summary>
	/// Check for required claims and values for launch.
	/// Other claims are also required, but are checked by <see cref="IJsonWebTokenService.AssertIsTokenValidAsync"/>.
	/// </summary>
	/// <param name="claims"></param>
	public void AssertLaunchClaimsAreValid(List<Claim> claims)
	{
		var messageTypeClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.MessageType));
		if (messageTypeClaim == null || !messageTypeClaim.Value.Equals(LtiMessageType.LtiResourceLinkRequest) &&
			!messageTypeClaim.Value.Equals(LtiMessageType.LtiDeepLinkingRequest))
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.MessageType,
				$"{LtiMessageType.LtiResourceLinkRequest} or {LtiMessageType.LtiDeepLinkingRequest}"));

		// In case the spec document doesn't get updated, '1.3.0' is the new correct value
		if (!claims.Any(c => c.Type.Equals(LtiClaim.Version) && c.Value.Equals(LtiVersion.Lti1P3)))
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.Version, LtiVersion.Lti1P3));

		var contextClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.Context));
		if (contextClaim == null)
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Context));

		var context = JsonConvert.DeserializeObject<Context>(contextClaim.Value);
		if (string.IsNullOrWhiteSpace(context.Id))
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.Context}.id"));

		var emailClaim = claims.FirstOrDefault(x => x.Type == OidcTokenClaim.Email);
		if (emailClaim?.Value == null)
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, OidcTokenClaim.Email));

		var targetLinkUriClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.TargetLinkUri));
		if (targetLinkUriClaim == null)
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.TargetLinkUri));

		if (messageTypeClaim.Value.Equals(LtiMessageType.LtiResourceLinkRequest))
		{
			var resourceLinkClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.ResourceLink));
			if (resourceLinkClaim == null)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.ResourceLink));

			var resourceLink = JsonConvert.DeserializeObject<ResourceLink>(resourceLinkClaim.Value);
			if (string.IsNullOrWhiteSpace(resourceLink.Id))
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.ResourceLink}.id"));
		}
		else if (messageTypeClaim.Value.Equals(LtiMessageType.LtiDeepLinkingRequest))
		{
			var deepLinkingSettingsClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiDeepLinkingClaim.DeepLinkingSettings));
			if (deepLinkingSettingsClaim == null)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiDeepLinkingClaim.DeepLinkingSettings));

			var deepLinkingSettings = JsonConvert.DeserializeObject<DeepLinkingSettings>(deepLinkingSettingsClaim.Value);
			if (string.IsNullOrWhiteSpace(deepLinkingSettings.DeepLinkReturnUrl))
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim,
					$"{LtiDeepLinkingClaim.DeepLinkingSettings}.deep_link_return_url"));

			if (deepLinkingSettings.AcceptTypes == null || deepLinkingSettings.AcceptTypes.Length == 0)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim,
					$"{LtiDeepLinkingClaim.DeepLinkingSettings}.accept_types"));

			if (deepLinkingSettings.AcceptPresentationDocumentTargets == null ||
				deepLinkingSettings.AcceptPresentationDocumentTargets.Length == 0)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim,
					$"{LtiDeepLinkingClaim.DeepLinkingSettings}.accept_presentation_document_targets"));
		}
	}

	/// <summary>
	/// According to https://openid.net/specs/openid-connect-core-1_0.html, validate that
	/// <list type="number">
	/// <item>we have never seen this request (via the nonce)</item>
	/// <item>the request was generated by our original OIDC auth step</item>
	/// </list>
	/// Coordinate this process via Redis cache. If either of these validations fail, return false, otherwise return true.
	/// </summary>
	/// <param name="state">state from LTI launch parameters</param>
	/// <param name="tokenString">JWT token containing claims embedded in the launch's id_token</param>
	/// <returns></returns>
	public async Task AssertLtiLaunchRequestIsValidAsync(string state, string tokenString)
	{
		if (string.IsNullOrWhiteSpace(state)) throw new ArgumentNullException(nameof(state));
		if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));

		var token = new JwtSecurityToken(tokenString);
		var claims = token.Claims;
		var nonce = claims.SingleOrDefault(n => n.Type == OidcTokenClaim.Nonce)?.Value;
		if (nonce == null)
			throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, OidcTokenClaim.Nonce));

		// If the nonce exists in redis, this is a replay attack. Return 404.
		var cachedNonceKey = $"{CacheConstants.LtiNonceCachePrefix}{nonce}";
		var cachedNonce = await _cacheClient.GetAsync<string>(cachedNonceKey);
		if (!string.IsNullOrEmpty(cachedNonce))
			throw new UnauthorizedException(Strings.LtiLaunchNonceCached);

		// If the state does not exist in redis, this may by a csrf attempt. Return 404.
		var ltiStateKey = $"{CacheConstants.LtiStateCachePrefix}{state}";
		var cachedState = await _cacheClient.GetAsync<string>(ltiStateKey);
		if (string.IsNullOrEmpty(cachedState))
			throw new UnauthorizedException(Strings.LtiLaunchStateNotCached);

		// Remove the state to prevent subsequent csrf attempts
		await _cacheClient.RemoveAsync(ltiStateKey);
		// Store the nonce to prevent replay attacks for 24 hours
		await _cacheClient.PutAsync(cachedNonceKey, CacheConstants.ExistentialCacheValue, TimeSpan.FromHours(24));
	}

	public async Task<JwtSecurityToken> GetDeepLinkingResponseAsync(LtiLaunch ltiLaunch,
		DeepLinkingResponseRequest deepLinkingResponseRequest, CancellationToken cancellationToken = default)
	{
		var gradable = await _dbContext.GetGradableAsync(deepLinkingResponseRequest.GradableId, cancellationToken);
		var externalGradable = await _dbContext.GetExternalGradableOrDefaultAsync(
			deepLinkingResponseRequest.GradableId,
			cancellationToken: cancellationToken);

		var contentItem = new LtiResourceLinkContentItem
		{
			Url = deepLinkingResponseRequest.Url,
			Title = gradable.Name
		};

		// Create a LineItem if the Gradable has none (no ExternalGradable) and has some points
		if (externalGradable == null && gradable.Points.HasValue && gradable.Points.Value > 0)
		{
			contentItem.LineItem = new LineItem
			{
				ScoreMaximum = gradable.Points.Value,
				Label = gradable.Name,
				ResourceId = gradable.Id.ToString()
			};

			// Load ExternalGroup connected to the Context
			var externalGroup = await _dbContext.ExternalGroups
				// TODO: revert to SingleAsync after adding unique index
				.FirstAsync(g =>
					g.ExternalId.Equals(ltiLaunch.ExternalId) &&
					g.ExternalProviderId.Equals(ltiLaunch.ExternalProviderId), cancellationToken);

			// this could be included in the ExternalGroups query above here, but the explicit error message seemed nicer and more consistent
			if (gradable.GroupId != externalGroup.GroupId)
				throw new ForbiddenException(string.Format(Strings.LtiDeepLinkInvalidCourseInstructorError, gradable.Name));

			// Insert an ExternalGradable w/o an ExternalId (lineItem isn't created yet) to avoid duplicate lineItems
			// This ExternalGradable will have ExternalId set when the deep link is launched
			_dbContext.AddExternalGradable(new TExternalGradable
			{
				ExternalGroupId = externalGroup.Id,
				GradableId = gradable.Id
			});
			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		var claims = new List<Claim>
		{
			new Claim(JsonWebTokenClaim.Subject, ltiLaunch.ExternalProvider.OauthClientId),
			new Claim(JsonWebTokenClaim.Audience, ltiLaunch.ExternalProvider.Issuer),
			new Claim(JsonWebTokenClaim.JwtId, Guid.NewGuid().ToString()),
			new Claim(OidcTokenClaim.Nonce, Guid.NewGuid().ToString()),
			new Claim(LtiClaim.MessageType, LtiMessageType.LtiDeepLinkingResponse),
			new Claim(LtiClaim.Version, LtiVersion.Lti1P3),
			new Claim(LtiClaim.DeploymentId, ltiLaunch.ExternalProvider.DeploymentId),
			ClaimUtils.CreateJsonArrayClaim(LtiDeepLinkingClaim.ContentItems, new[] { contentItem })
		};

		// Add optional DeepLinkingData
		if (!string.IsNullOrWhiteSpace(ltiLaunch.DeepLinkingData))
		{
			claims.Add(new Claim(LtiDeepLinkingClaim.Data, ltiLaunch.DeepLinkingData));
		}

		var token = await _jsonWebTokenService.CreateJwtAsync(ltiLaunch.ExternalProvider, claims, cancellationToken);

		return token;
	}
}