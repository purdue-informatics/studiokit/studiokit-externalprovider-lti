﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class LtiAgsScoreActivityProgress
{
	public static string Initialized = "Initialized";

	public static string Started = "Started";

	public static string InProgress = "InProgress";

	public static string Submitted = "Submitted";

	public static string Completed = "Completed";
}