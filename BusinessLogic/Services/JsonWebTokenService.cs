﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.Common;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services;

public class JsonWebTokenService<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> : IJsonWebTokenService
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>, new()
	where TGroupUserRole : class, IGroupUserRole, new()
	where TExternalTerm : class, IExternalTerm, new()
	where TExternalGroup : class, IExternalGroup, new()
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
{
	private readonly ILtiLaunchDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> _dbContext;
	private readonly ILtiKeySetProviderService _keySetProviderService;
	private readonly ICertificateService _certificateService;

	public JsonWebTokenService(ILtiLaunchDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> dbContext,
		ILtiKeySetProviderService keySetProviderService, ICertificateService certificateService)
	{
		_dbContext = dbContext;
		_keySetProviderService = keySetProviderService;
		_certificateService = certificateService;
	}

	public async Task<LtiExternalProvider> GetTokenProviderAsync(JwtSecurityToken token, CancellationToken cancellationToken = default)
	{
		if (token == null) throw new ArgumentNullException(nameof(token));

		// Get the oauth2 client_id from the token
		var audiences = token.Audiences.ToList();

		var deploymentId = token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.DeploymentId))?.Value;
		if (deploymentId == null)
			return null;

		// Check that the audiences claim contains our client ID
		// If we do start trusting multiple clients
		return await _dbContext.LtiExternalProviders
			.SingleOrDefaultAsync(p => audiences.Contains(p.OauthClientId) && p.DeploymentId == deploymentId, cancellationToken);
	}

	public async Task AssertIsTokenValidAsync(string tokenString, CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));

		// Decode string (without validating) to be able to get the provider.
		var token = new JwtSecurityToken(tokenString);

		if (token.Issuer == null)
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Issuer));

		if (!token.Audiences.Any())
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Audience));

		if (token.Subject == null)
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Subject));

		if (token.ValidTo == DateTime.MinValue)
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Expiration));

		if (!token.Claims.Any(c => c.Type.Equals(JsonWebTokenClaim.IssuedAt) && c.Value != null))
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.IssuedAt));

		if (!token.Claims.Any(c => c.Type.Equals(OidcTokenClaim.Nonce) && c.Value != null))
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, OidcTokenClaim.Nonce));

		// Get the oauth2 client_id from the token
		var audiences = token.Audiences.ToList();

		// If the audiences claim has more than one item, reject it if we don't trust all audiences
		// (and we don't trust anyone else right now)
		// If we do trust people in the future, the 'azp' claim must be present if more than one audience exists
		if (audiences.Count > 1)
			throw new UnauthorizedException(Strings.TokenUntrustedAudiences);

		var deploymentId = token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.DeploymentId))?.Value;
		if (deploymentId == null)
			throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.DeploymentId));

		// Check that the audiences claim contains our client ID
		// If we do start trusting multiple clients
		var externalProvider = await GetTokenProviderAsync(token, cancellationToken);
		if (externalProvider == null)
			throw new UnauthorizedException(Strings.AudienceDeploymentIdNotRegistered);

		// If the 'authorized party' claim exists check that it equals the registered client_id value.
		if (token.Claims.Any(c => c.Type.Equals(OidcTokenClaim.AuthorizedParty) && !c.Value.Equals(externalProvider.OauthClientId)))
			throw new UnauthorizedException(Strings.AzpClaimDoesNotMatchClientId);

		// Get public keys from the platform
		var jsonWebKeySet = await _keySetProviderService.GetKeySetAsync(externalProvider, cancellationToken);

		var tokenHandler = new JwtSecurityTokenHandler();
		// Set required values for some claims based on the provider.
		var validationParams = new TokenValidationParameters
		{
			ValidateAudience = true,
			ValidateIssuer = true,
			ValidateLifetime = true,
			ValidateIssuerSigningKey = true,
			ValidAudience = externalProvider.OauthClientId,
			ValidIssuer = externalProvider.Issuer,
			IssuerSigningKeys = jsonWebKeySet.GetSigningKeys()
		};

		// Verify token signature
		try
		{
			tokenHandler.ValidateToken(tokenString, validationParams, out var _);
		}
		catch (SecurityTokenException e)
		{
			throw new UnauthorizedException(Strings.ValidateTokenFailed, e);
		}
	}

	/// <summary>
	/// Create a Json Web Token with the required LTI claims with values from the given LtiExternalProvider
	/// and including the given claims
	/// </summary>
	/// <param name="provider"><see cref="LtiExternalProvider"/> where this token will be sent</param>
	/// <param name="claimList">Claims that should be included in the created JWT</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <returns>A <see cref="JwtSecurityToken"/> with the required LTI claims set</returns>
	public async Task<JwtSecurityToken> CreateJwtAsync(LtiExternalProvider provider, List<Claim> claimList = null,
		CancellationToken cancellationToken = default)
	{
		if (provider == null)
			throw new ArgumentNullException(nameof(provider));

		if (claimList == null)
			claimList = new List<Claim>();

		var currentTime = DateTime.UtcNow;
		string audience = null;
		DateTime? expires = null;

		// Issuer is always going to be our tool ID for the target platform
		if (claimList.Any(c => c.Type.Equals(JsonWebTokenClaim.Issuer) && !c.Value.Equals(provider.OauthClientId)))
		{
			var invalidClaim = claimList.First(c => c.Type.Equals(JsonWebTokenClaim.Issuer));
			throw new LtiInvalidJwtException(string.Format(Strings.LtiInvalidJwtClaim, invalidClaim.Type, invalidClaim.Value));
		}

		// Check if the required claims already exist in the passed in claim List.
		// Set default values to use in the constructor if they do not exist.
		if (!claimList.Any(c => c.Type.Equals(JsonWebTokenClaim.Audience)))
		{
			audience = provider.Issuer;
		}

		if (!claimList.Any(c => c.Type.Equals(JsonWebTokenClaim.Expiration)))
		{
			expires = currentTime.AddMinutes(3);
		}

		if (!claimList.Any(c => c.Type.Equals(JsonWebTokenClaim.IssuedAt)))
		{
			var epochTimeSpan = currentTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
			claimList.Add(new Claim(JsonWebTokenClaim.IssuedAt, Convert.ToInt64(epochTimeSpan.TotalSeconds).ToString(),
				ClaimValueTypes.Integer64));
		}

		var signingCredentials = await _certificateService.GetSigningCredentialsAsync(cancellationToken);

		// The constructor here adds an 'nbf' (not before) claim instead of 'iat' (issued at). We don't need it.
		return new JwtSecurityToken(provider.OauthClientId, audience, claimList, null, expires, signingCredentials);
	}
}