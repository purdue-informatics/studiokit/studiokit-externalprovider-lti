﻿using Newtonsoft.Json;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// https://www.imsglobal.org/spec/lti-dl/v2p0/#deep-linking-request-message
/// </summary>
public class DeepLinkingSettings
{
	[JsonProperty("deep_link_return_url")]
	public string DeepLinkReturnUrl { get; set; }

	[JsonProperty("accept_types")]
	public string[] AcceptTypes { get; set; }

	[JsonProperty("accept_presentation_document_targets")]
	public string[] AcceptPresentationDocumentTargets { get; set; }

	[JsonProperty("accept_media_types")]
	public string AcceptMediaTypes { get; set; }

	[JsonProperty("accept_multiple")]
	public bool AcceptMultiple { get; set; }

	[JsonProperty("auto_create")]
	public bool AutoCreate { get; set; }

	public string Title { get; set; }

	public string Text { get; set; }

	public string Data { get; set; }
}