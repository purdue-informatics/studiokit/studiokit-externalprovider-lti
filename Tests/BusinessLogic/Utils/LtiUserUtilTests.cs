using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Utils;

[TestClass]
public class LtiUserUtilTests : BaseTest
{
	#region AppendContextIdIfNeeded

	[TestMethod]
	public void AppendContextIdIfNeeded_ShouldReturnNullForNullEmail()
	{
		const string testContextId = "1234";
		var updatedEmail = LtiUserUtil.AppendContextIdIfNeeded(null, testContextId);
		Assert.AreEqual(null, updatedEmail);
	}

	[TestMethod]
	public void AppendContextIdIfNeeded_ShouldReturnUserEmailsUnmodified()
	{
		const string testUserEmail = "someUser@purdue.edu";
		const string testContextId = "1234";
		var updatedEmail = LtiUserUtil.AppendContextIdIfNeeded(testUserEmail, testContextId);
		Assert.AreEqual(testUserEmail, updatedEmail);
	}

	[TestMethod]
	public void AppendContextIdIfNeeded_ShouldAppendContextIdForDemoUser()
	{
		const string demoUserEmail = $@"{LtiUserUtil.DemoStudentUserName}{LtiUserUtil.DemoStudentDomain}";
		const string testContextId = "1234";
		var updatedEmail = LtiUserUtil.AppendContextIdIfNeeded(demoUserEmail, testContextId);
		Assert.AreEqual($@"{LtiUserUtil.DemoStudentUserName}-{testContextId}{LtiUserUtil.DemoStudentDomain}", updatedEmail);
	}

	#endregion AppendContextIdIfNeeded
}