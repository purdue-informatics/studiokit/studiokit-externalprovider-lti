﻿using Newtonsoft.Json;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class LaunchPresentation
{
	[JsonProperty("document_target")]
	public string DocumentTarget { get; set; }

	public int Height { get; set; }

	public int Width { get; set; }

	[JsonProperty("return_url")]
	public string ReturnUrl { get; set; }
}