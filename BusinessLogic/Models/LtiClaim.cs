﻿using StudioKit.Security.Common;
using System.Collections.Generic;
using System.Security.Claims;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class LtiClaim
{
	public static string Context = "https://purl.imsglobal.org/spec/lti/claim/context";

	public static string MessageType = "https://purl.imsglobal.org/spec/lti/claim/message_type";

	public static string Version = "https://purl.imsglobal.org/spec/lti/claim/version";

	public static string DeploymentId = "https://purl.imsglobal.org/spec/lti/claim/deployment_id";

	public static string ResourceLink = "https://purl.imsglobal.org/spec/lti/claim/resource_link";

	public static string Platform = "https://purl.imsglobal.org/spec/lti/claim/tool_platform";

	public static string Roles = "https://purl.imsglobal.org/spec/lti/claim/roles";

	public static string RoleScopeMentor = "https://purlimsglobal.org/spec/lti/claim/role_scope_mentor";

	public static string LaunchPresentation = "https://purl.imsglobal.org/spec/lti/claim/launch_presentation";

	public static string Custom = "https://purl.imsglobal.org/spec/lti/claim/custom";

	public static string Lis = "https://purl.imsglobal.org/spec/lti/claim/lis";

	public static string TargetLinkUri = "https://purl.imsglobal.org/spec/lti/claim/target_link_uri";

	public static Dictionary<string, string> CommonClaimMap => new Dictionary<string, string>
	{
		{ JsonWebTokenClaim.Subject, ClaimTypes.NameIdentifier },
		{ OidcTokenClaim.Email, ClaimTypes.Email },
		{ OidcTokenClaim.GivenName, ClaimTypes.GivenName },
		{ OidcTokenClaim.FamilyName, ClaimTypes.Surname }
	};
}