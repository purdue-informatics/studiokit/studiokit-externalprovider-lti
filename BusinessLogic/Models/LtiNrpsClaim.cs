﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// LTI Names and Role Provisioning Services Claim, "lti-nrps"
/// </summary>
public static class LtiNrpsClaim
{
	public static string NamesRoleService = "https://purl.imsglobal.org/spec/lti-nrps/claim/namesroleservice";
}