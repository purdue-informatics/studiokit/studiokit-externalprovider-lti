﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

/// <summary>
/// https://www.imsglobal.org/spec/lti/v1p3/#context-type-vocabulary
/// </summary>
public static class LisVocab
{
	public static string CourseTemplate = "http://purl.imsglobal.org/vocab/lis/v2/course#CourseTemplate";

	public static string CourseOffering = "http://purl.imsglobal.org/vocab/lis/v2/course#CourseOffering";

	public static string CourseSection = "http://purl.imsglobal.org/vocab/lis/v2/course#CourseSection";

	public static string Group = "http://purl.imsglobal.org/vocab/lis/v2/course#Group";
}