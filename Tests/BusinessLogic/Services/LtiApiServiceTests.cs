﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using Newtonsoft.Json.Linq;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.Tests;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services;

[TestClass]
[DoNotParallelize]
public class LtiApiServiceTests : BaseTest
{
	private const string TestJwtString =
		"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzQ3ODk5ODUsImp0aSI6Ijk0NDBjODgwLWEyZmQtNDk3OC05ZTFiLTE4NmUzZTA4MjM0MSIsImV4cCI6MTYzNTg5MDE2NCwiaXNzIjoiMTIzNDUiLCJhdWQiOiJodHRwczovL2x0aS1yaS5pbXNnbG9iYWwub3JnL3BsYXRmb3Jtcy8xOS9hY2Nlc3NfdG9rZW5zIiwic3ViIjoiMTIzNDUifQ.jzQlRgPyOEngf86AdYfBIkDd67JHa5re0A1dVk9DIV6MaFdc-FSUJezpzw8mU9XKhe87383rW4Cbmk58dG7e9-2ie_M_uUMlow6B0qnDZu00o7JX6K9bzVE46g1A3pFel8UD05zYbxTF9AYEvTiJhBncBFKMKRucwLfwIAp4EKJGThpzz45XgnYhtzBnmvosubm8kmwh7aaMAxuZ_vuqqsfsvlASVxbYpqR1cpHBO1mYxf1k-dRdJjTBCpm7uC5YM2Rd108rxTyAoWE3LMLDLb1GR5aNnvd40uf1OI48hX21CEG7gzPP_zV2KpbtyRyAFzmNQJ35jDS5prSTUcS-cA";

	private const string TestToken =
		"{\"access_token\":\"token\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";

	/// <summary>
	/// JSON response that can't be de-serialized to any of our models
	/// </summary>
	private const string InvalidJsonResponse = "<{\"invalid\":\"object\"}";

	private readonly LtiExternalProvider _ltiExternalProvider = TestUtils.GetLtiExternalProvider();

	private IErrorHandler _errorHandler;

	private IJsonWebTokenService _jsonWebTokenService;

	[TestInitialize]
	public void Initialize()
	{
		var handler = new JwtSecurityTokenHandler();
		var token = handler.CreateJwtSecurityToken(TestJwtString);

		_errorHandler = new Mock<IErrorHandler>().Object;
		var tokenServiceMock = new Mock<IJsonWebTokenService>();
		tokenServiceMock.Setup(x =>
				x.CreateJwtAsync(It.IsAny<LtiExternalProvider>(), It.IsAny<List<Claim>>(), It.IsAny<CancellationToken>()))
			.ReturnsAsync(token);
		_jsonWebTokenService = tokenServiceMock.Object;
	}

	#region GetResultAsync

	[TestMethod]
	public void GetResultAsync_ShouldThrowIfExternalProviderNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.GetResultAsync<JObject>(null, "https://www.example.com", CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("ltiExternalProvider"));
	}

	[TestMethod]
	public void GetResultAsync_ShouldThrowIfUrlNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.GetResultAsync<JObject>(_ltiExternalProvider, null, CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("url"));
	}

	[TestMethod]
	public async Task GetResultAsync_ShouldSucceedIfGetsToken()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.GetAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("{\"test\":\"json\"}") });

		var apiService = GetService(() => httpClientMock.Object);

		var result = await apiService.GetResultAsync<JObject>(_ltiExternalProvider, "https://www.example.com", CancellationToken.None,
			new MediaTypeWithQualityHeaderValue("application/json"));

		Assert.AreEqual("json", result.Item1.Value<string>("test"));
	}

	[TestMethod]
	public void GetResultAsync_ShouldThrowOnNetworkException()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.GetAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json")),
				true))
			.Throws(new HttpRequestException());

		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.GetResultAsync<JObject>(_ltiExternalProvider, "https://www.example.com",
					CancellationToken.None,
					new MediaTypeWithQualityHeaderValue("application/json"));
			}), Strings.LtiApiRequestError);
	}

	[TestMethod]
	public void GetResultAsync_ShouldThrowOnInvalidJson()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.GetAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(InvalidJsonResponse) });

		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.GetResultAsync<JObject>(_ltiExternalProvider, "https://www.example.com",
					CancellationToken.None,
					new MediaTypeWithQualityHeaderValue("application/json"));
			}), Strings.LtiJsonError);
	}

	#endregion GetResultAsync

	#region GetResultsAsync

	[TestMethod]
	public async Task GetResultsAsync_ShouldSucceedWithoutLinkHeader()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.GetAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://example.com/results")),
				It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent("{\"test\":\"json\"}") });

		var apiService = GetService(() => httpClientMock.Object);

		var responses = await apiService.GetResultsAsync<JObject>(
			_ltiExternalProvider,
			"https://example.com/results",
			CancellationToken.None,
			new MediaTypeWithQualityHeaderValue("application/json"));

		Assert.AreEqual(1, responses.Count);
		Assert.AreEqual("json", responses.First().Item1.Value<string>("test"));
	}

	[TestMethod]
	public async Task GetResultsAsync_ShouldSucceedWithLinkHeader()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.GetAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://example.com/results")),
				It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new StringContent("{\"test\":\"json1\"}"),
				Headers =
				{
					{ "Link", "<https://example.com/results?limit=1&page=2>;rel=\"next\"" }
				}
			});
		httpClientMock.Setup(x => x.GetAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://example.com/results?limit=1&page=2")),
				It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new StringContent("{\"test\":\"json2\"}"),
				Headers =
				{
					// return the same "Link" on the last page
					{ "Link", "<https://example.com/results?limit=1&page=2>;rel=\"next\"" }
				}
			});

		var apiService = GetService(() => httpClientMock.Object);

		var responses = await apiService.GetResultsAsync<JObject>(
			_ltiExternalProvider,
			"https://example.com/results",
			CancellationToken.None,
			new MediaTypeWithQualityHeaderValue("application/json"));

		Assert.AreEqual(2, responses.Count);
		Assert.AreEqual("json1", responses.First().Item1.Value<string>("test"));
		Assert.AreEqual("json2", responses.ElementAt(1).Item1.Value<string>("test"));
	}

	#endregion GetResultsAsync

	#region PostAsync

	[TestMethod]
	public void PostAsync_ShouldThrowIfExternalProviderNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.PostAsync<JObject>(null, "https://www.example.com", _ => new StringContent(""),
					CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("ltiExternalProvider"));
	}

	[TestMethod]
	public void PostAsync_ShouldThrowIfUrlNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.PostAsync<JObject>(_ltiExternalProvider, null, _ => new StringContent(""),
					CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("url"));
	}

	[TestMethod]
	public void PostAsync_ShouldThrowIfContentNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.PostAsync<JObject>(_ltiExternalProvider, "https://www.example.com", null, CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("getHttpContent"));
	}

	[TestMethod]
	public async Task PostAsync_ShouldSucceed()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.PostAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsNotNull<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent("{\"test\":\"json\"}") });

		var apiService = GetService(() => httpClientMock.Object);
		var result = await apiService.PostAsync<JObject>(_ltiExternalProvider, "https://www.example.com", _ => new StringContent(""),
			CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));

		Assert.AreEqual("json", result.Item1.Value<string>("test"));
	}

	[TestMethod]
	public void PostAsync_ShouldThrowOnNetworkError()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.PostAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsNotNull<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json")),
				true))
			.Throws(new HttpRequestException());

		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.PostAsync<JObject>(_ltiExternalProvider, "https://www.example.com", _ => new StringContent(""),
					CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));
			}), Strings.LtiApiRequestError);
	}

	[TestMethod]
	public void PostAsync_ShouldThrowOnInvalidJson()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.PostAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsNotNull<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(InvalidJsonResponse) });

		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.PostAsync<JObject>(_ltiExternalProvider, "https://www.example.com", _ => new StringContent(""),
					CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));
			}), Strings.LtiJsonError);
	}

	#endregion PostAsync

	#region PutAsync

	[TestMethod]
	public void PutAsync_ShouldThrowIfExternalProviderNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.PutAsync<JObject>(null, "https://www.example.com", _ => new StringContent(""),
					CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("ltiExternalProvider"));
	}

	[TestMethod]
	public void PutAsync_ShouldThrowIfUrlNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.PutAsync<JObject>(_ltiExternalProvider, null, _ => new StringContent(""),
					CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("url"));
	}

	[TestMethod]
	public void PutAsync_ShouldThrowIfContentNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.PutAsync<JObject>(_ltiExternalProvider, "https://www.example.com", null, CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("getHttpContent"));
	}

	[TestMethod]
	public async Task PutAsync_ShouldSucceed()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.PutAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsNotNull<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent("{\"test\":\"json\"}") });

		var apiService = GetService(() => httpClientMock.Object);
		var result = await apiService.PutAsync<JObject>(_ltiExternalProvider, "https://www.example.com", _ => new StringContent(""),
			CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));

		Assert.AreEqual("json", result.Item1.Value<string>("test"));
	}

	[TestMethod]
	public void PutAsync_ShouldThrowOnNetworkError()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.PutAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsNotNull<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json")),
				true))
			.Throws(new HttpRequestException());

		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.PutAsync<JObject>(_ltiExternalProvider, "https://www.example.com", _ => new StringContent(""),
					CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));
			}), Strings.LtiApiRequestError);
	}

	[TestMethod]
	public void PutAsync_ShouldThrowOnInvalidJson()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<AuthenticationHeaderValue>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>(),
				true))
			.ReturnsAsync(TestToken);
		httpClientMock.Setup(x => x.PutAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals("https://www.example.com/")),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsNotNull<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json")),
				true))
			.ReturnsAsync(new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(InvalidJsonResponse) });

		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.PutAsync<JObject>(_ltiExternalProvider, "https://www.example.com", _ => new StringContent(""),
					CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));
			}), Strings.LtiJsonError);
	}

	#endregion PutAsync

	#region GetAuthTokenAsync

	[TestMethod]
	public void GetAuthTokenAsync_ShouldThrowIfProviderNull()
	{
		var apiService = GetService();

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await apiService.GetAuthTokenAsync(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("provider"));
	}

	[TestMethod]
	public async Task GetAuthTokenAsync_ShouldUseCache()
	{
		const string testToken2 =
			"{\"access_token\":\"token2\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";

		var tokenStack = new Stack<string>();
		tokenStack.Push(testToken2);
		tokenStack.Push(TestToken);

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true))
			.ReturnsAsync(tokenStack.Pop);

		LtiApiService.ClearCache();
		var apiService = GetService(() => httpClientMock.Object);
		var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider);
		var result2 = await apiService.GetAuthTokenAsync(_ltiExternalProvider);

		httpClientMock.Verify(c => c.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true),
			Times.Once);

		Assert.AreEqual("token", result);
		Assert.AreEqual("token", result2);
	}

	[TestMethod]
	public async Task GetAuthTokenAsync_ShouldRenewIfTokenExpiring()
	{
		//Token refreshes if it expires within the next 5 minutes, this one only lasts 4 minutes
		var expiringToken =
			"{\"access_token\":\"expiringToken\",\"token_type\":\"Bearer\",\"expires_in\": 240,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";
		var tokenStack = new Stack<string>();
		tokenStack.Push(TestToken);
		tokenStack.Push(expiringToken);

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true))
			.ReturnsAsync(tokenStack.Pop);

		LtiApiService.ClearCache();
		var apiService = GetService(() => httpClientMock.Object);
		var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider, CancellationToken.None);
		var result2 = await apiService.GetAuthTokenAsync(_ltiExternalProvider, CancellationToken.None);

		httpClientMock.Verify(c => c.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true),
			Times.Exactly(2));

		Assert.AreEqual("expiringToken", result);
		Assert.AreEqual("token", result2);
	}

	[TestMethod]
	public void GetAuthTokenAsync_ShouldThrowOnNetworkException()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true))
			.Throws(new HttpRequestException());

		LtiApiService.ClearCache();
		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.GetAuthTokenAsync(_ltiExternalProvider);
			}), Strings.LtiOauthRequestError);
	}

	[TestMethod]
	public async Task GetAuthTokenAsync_ShouldReturnCachedTokenOnNetworkExceptionIfNotExpired()
	{
		const string expiringToken =
			"{\"access_token\":\"expiringToken\",\"token_type\":\"Bearer\",\"expires_in\": 240,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";
		var tokenStack = new Stack<string>();
		tokenStack.Push(TestToken);
		tokenStack.Push(expiringToken);

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true))
			.ReturnsAsync(() => tokenStack.Count == 2 ? tokenStack.Pop() : throw new HttpRequestException());

		LtiApiService.ClearCache();
		var apiService = GetService(() => httpClientMock.Object);
		var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider);
		var result2 = await apiService.GetAuthTokenAsync(_ltiExternalProvider);

		Assert.AreEqual("expiringToken", result);
		Assert.AreEqual("expiringToken", result2);
	}

	[TestMethod]
	public async Task GetAuthTokenAsync_ShouldThrowOnNetworkErrorWithExpiredCacheToken()
	{
		// Token expired two minutes ago
		const string expiringToken =
			"{\"access_token\":\"expiringToken\",\"token_type\":\"Bearer\",\"expires_in\": -120,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";
		var tokenStack = new Stack<string>();
		tokenStack.Push(TestToken);
		tokenStack.Push(expiringToken);

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true))
			.ReturnsAsync(() => tokenStack.Count == 2 ? tokenStack.Pop() : throw new HttpRequestException());

		LtiApiService.ClearCache();
		var apiService = GetService(() => httpClientMock.Object);
		var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider);

		Assert.AreEqual("expiringToken", result);
		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.GetAuthTokenAsync(_ltiExternalProvider);
			}), Strings.LtiOauthRequestError);
	}

	[TestMethod]
	public void GetAuthTokenAsync_ShouldThrowOnInvalidJson()
	{
		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(x => x.PostForStringAsync(
				It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
				It.IsNotNull<Func<Uri, HttpContent>>(),
				It.IsAny<CancellationToken>(),
				It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
				It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null),
				true))
			.ReturnsAsync(InvalidJsonResponse);

		LtiApiService.ClearCache();
		var apiService = GetService(() => httpClientMock.Object);

		Assert.ThrowsAsync<LtiException>(
			Task.Run(async () =>
			{
				await apiService.GetAuthTokenAsync(_ltiExternalProvider);
			}), Strings.LtiJsonError);
	}

	#endregion GetAuthTokenAsync

	#region Helpers

	private LtiApiService GetService(Func<IHttpClient> httpClientProviderFunc = null)
	{
		return new LtiApiService(_jsonWebTokenService, httpClientProviderFunc ?? (() => new Mock<IHttpClient>().Object), _errorHandler);
	}

	#endregion Helpers
}