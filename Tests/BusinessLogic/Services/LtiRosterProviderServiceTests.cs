﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Services;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Tests.TestData.EqualityComparers;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services;

[TestClass]
public class LtiRosterProviderServiceTests : BaseTest
{
	private ILtiApiService _ltiApiService;
	private IErrorHandler _errorHandler;
	private IUserFactory<TestUser> _userFactory;
	private MembershipContainer _membershipContainer;
	private TestExternalGroup _externalGroup;
	private List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>> _expectedResult;

	[TestInitialize]
	public void BeforeEach()
	{
		_errorHandler = new Mock<IErrorHandler>().Object;

		_userFactory = new BaseUserFactory<TestUser>(new UpperInvariantLookupNormalizer());

		_membershipContainer = new MembershipContainer
		{
			Id = "https://lti-ri.imsglobal.org/platforms/19/contexts/17/memberships.json",
			Members = new List<Member>
			{
				new()
				{
					ContextId = "17",
					ContextLabel = "Test Course",
					ContextTitle = "A test course from the LTI test tool",
					Name = "Cleveland Lukas Blob",
					Picture = "https://example.org/Cleveland.jpg",
					GivenName = "Cleveland",
					FamilyName = "Blob",
					MiddleName = "Lukas",
					Email = "Cleveland.Blob@example.org",
					UserId = "e51bfcf77f6c6b30ce4c",
					Roles = new List<string>
					{
						LtiRole.MembershipInstructor,
						LtiRole.MembershipAdministrator,
						LtiRole.InstitutionInstructor,
						LtiRole.InstitutionAdministrator
					}
				},
				new()
				{
					ContextId = "17",
					ContextLabel = "Test Course",
					ContextTitle = "A test course from the LTI test tool",
					Name = "Taurean Marlon Roob",
					Picture = "https://example.org/Taurean.jpg",
					GivenName = "Taurean",
					FamilyName = "Roob",
					MiddleName = "Marlon",
					Email = "Taurean.Roob@example.org",
					UserId = "50024ef0fe0de638781b",
					Roles = new List<string>
					{
						LtiRole.MembershipLearner,
						LtiRole.InstitutionInstructor
					}
				},
				new()
				{
					ContextId = "17",
					ContextLabel = "Test Course",
					ContextTitle = "A test course from the LTI test tool",
					Name = "Margarette Hettie Cummings",
					Picture = "https://example.org/Margarette.jpg",
					GivenName = "Margarette",
					FamilyName = "Cummings",
					MiddleName = "Hettie",
					Email = "Margarette.Cummings@example.org",
					UserId = "3f49327c40b006bf001e",
					Roles = new List<string>
					{
						LtiRole.MembershipLearner,
						LtiRole.InstitutionStudent,
						LtiRole.InstitutionLearner
					}
				},
				new()
				{
					ContextId = "17",
					ContextLabel = "Test Course",
					ContextTitle = "A test course from the LTI test tool",
					Name = "Gianni Ken Rutherford",
					Picture = "https://example.org/Gianni.jpg",
					GivenName = "Gianni",
					FamilyName = "Rutherford",
					MiddleName = "Ken",
					Email = "Gianni.Rutherford@example.org",
					UserId = "d1830007bd4f0e2f1947",
					Roles = new List<string>
					{
						LtiRole.MembershipLearner,
						LtiRole.InstitutionStudent,
						LtiRole.InstitutionLearner
					}
				},
				new()
				{
					ContextId = "17",
					ContextLabel = "Test Course",
					ContextTitle = "A test course from the LTI test tool",
					Name = "Alexys Caterina Feest",
					Picture = "https://example.org/Alexys.jpg",
					GivenName = "Alexys",
					FamilyName = "Feest",
					MiddleName = "Caterina",
					Email = "Alexys.Feest@example.org",
					UserId = "57252576ca62a9722c25",
					Roles = new List<string>
					{
						LtiRole.MembershipMentor
					}
				}
			}
		};

		_externalGroup = new TestExternalGroup
		{
			Id = 1,
			RosterUrl = "https://example.com/some-roster"
		};

		_expectedResult = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
		{
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup.Id,
					ExternalUserId = "e51bfcf77f6c6b30ce4c",
					Roles =
						$"{LtiRole.MembershipInstructor},{LtiRole.MembershipAdministrator},{LtiRole.InstitutionInstructor},{LtiRole.InstitutionAdministrator}",
					User = new TestUser
					{
						UserName = "Cleveland.Blob@example.org",
						NormalizedUserName = "CLEVELAND.BLOB@EXAMPLE.ORG",
						Email = "Cleveland.Blob@example.org",
						NormalizedEmail = "CLEVELAND.BLOB@EXAMPLE.ORG",
						FirstName = "Cleveland",
						LastName = "Blob"
					}
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupOwner
				}
			},
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup.Id,
					ExternalUserId = "50024ef0fe0de638781b",
					Roles = $"{LtiRole.MembershipLearner},{LtiRole.InstitutionInstructor}",
					User = new TestUser
					{
						UserName = "Taurean.Roob@example.org",
						NormalizedUserName = "TAUREAN.ROOB@EXAMPLE.ORG",
						Email = "Taurean.Roob@example.org",
						NormalizedEmail = "TAUREAN.ROOB@EXAMPLE.ORG",
						FirstName = "Taurean",
						LastName = "Roob"
					}
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			},
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup.Id,
					ExternalUserId = "3f49327c40b006bf001e",
					Roles = $"{LtiRole.MembershipLearner},{LtiRole.InstitutionStudent},{LtiRole.InstitutionLearner}",
					User = new TestUser
					{
						UserName = "Margarette.Cummings@example.org",
						NormalizedUserName = "MARGARETTE.CUMMINGS@EXAMPLE.ORG",
						Email = "Margarette.Cummings@example.org",
						NormalizedEmail = "MARGARETTE.CUMMINGS@EXAMPLE.ORG",
						FirstName = "Margarette",
						LastName = "Cummings"
					}
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			},
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup.Id,
					ExternalUserId = "d1830007bd4f0e2f1947",
					Roles = $"{LtiRole.MembershipLearner},{LtiRole.InstitutionStudent},{LtiRole.InstitutionLearner}",
					User = new TestUser
					{
						UserName = "Gianni.Rutherford@example.org",
						NormalizedUserName = "GIANNI.RUTHERFORD@EXAMPLE.ORG",
						Email = "Gianni.Rutherford@example.org",
						NormalizedEmail = "GIANNI.RUTHERFORD@EXAMPLE.ORG",
						FirstName = "Gianni",
						LastName = "Rutherford"
					}
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			}
		};

		var mockLtiApiService = new Mock<ILtiApiService>();
		mockLtiApiService
			.Setup(s => s.GetResultsAsync<MembershipContainer>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			.ReturnsAsync(new List<(MembershipContainer, HttpResponseMessage)>
				{ (_membershipContainer, new HttpResponseMessage()) });
		_ltiApiService = mockLtiApiService.Object;
	}

	[TestMethod]
	public void GetRosterAsync_ShouldThrowIfNullArguments()
	{
		var service = GetService();
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetRosterAsync(null, null, CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("externalProvider"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetRosterAsync(new LtiExternalProvider(), null, CancellationToken.None);
			}), ExceptionUtils.ArgumentNullExceptionMessage("externalGroup"));
	}

	[TestMethod]
	public void GetRosterAsync_ShouldThrowIfNotLtiExternalProvider()
	{
		var service = GetService();
		Assert.ThrowsAsync<ArgumentException>(
			Task.Run(async () =>
			{
				await service.GetRosterAsync(new TestExternalProvider(), new TestExternalGroup(), CancellationToken.None);
			}), Strings.LtiExternalProviderRequired);
	}

	[TestMethod]
	public async Task GetRosterAsync_ShouldIgnoreIfAnyMembersAreMissingEmailButCaptureException()
	{
		var member = _membershipContainer.Members.First();
		member.Email = null;

		var externalGroup = new TestExternalGroup
		{
			Id = 1,
			RosterUrl = "https://example.com/some-roster"
		};

		Exception capturedException = null;
		var errorHandlerMock = new Mock<IErrorHandler>();
		errorHandlerMock.Setup(x => x.CaptureException(It.IsAny<Exception>())).Callback((Exception e) =>
		{
			capturedException = e as ValidationException;
		});
		_errorHandler = errorHandlerMock.Object;

		var service = GetService();
		var rosterEntries = await service.GetRosterAsync(new LtiExternalProvider(), externalGroup, CancellationToken.None);
		Assert.IsTrue(rosterEntries.SequenceEqual(_expectedResult.Skip(1), new ExternalRosterEntryEqualityComparer()));
		var capturedValidationException = capturedException as ValidationException;
		Assert.IsNotNull(capturedValidationException);
		Assert.AreEqual(string.Format(Strings.MembersMissingEmail, $"{member.GivenName} {member.FamilyName}"),
			capturedValidationException?.Message);
	}

	[TestMethod]
	public async Task GetRosterAsync_ShouldReturnInstructorsAndLearners()
	{
		var service = GetService();
		var rosterEntries = await service.GetRosterAsync(new LtiExternalProvider(), _externalGroup, CancellationToken.None);
		Assert.IsTrue(rosterEntries.SequenceEqual(_expectedResult, new ExternalRosterEntryEqualityComparer()));
	}

	[TestMethod]
	public async Task GetRosterAsync_ShouldKeepLoadingMembershipsIfResponseIncludesLinkHeaderValue()
	{
		var mockLtiApiService = new Mock<ILtiApiService>();
		mockLtiApiService
			.SetupSequence(s => s.GetResultsAsync<MembershipContainer>(
				It.IsAny<LtiExternalProvider>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>(),
				It.IsAny<MediaTypeWithQualityHeaderValue>()))
			// return the 5 memberships in multiple requests
			// 1. return first membership, with a link to the second membership
			// 2. return second membership, with a link to the last 3 memberships
			// 2. return last 3 memberships, with a link to the SAME 3 memberships
			.ReturnsAsync(
				new List<(MembershipContainer, HttpResponseMessage)>
				{
					(
						new MembershipContainer
						{
							Id = _membershipContainer.Id,
							Members = _membershipContainer.Members.Take(1).ToList()
						},
						new HttpResponseMessage
						{
							Content = new StringContent(""),
							Headers =
							{
								{
									"Link", "<https://example.com/some-roster?limit=1&page=2>; rel=\"next\""
								} // use a space in this URL, but not the others
							}
						}
					),
					(
						new MembershipContainer
						{
							Id = _membershipContainer.Id,
							Members = _membershipContainer.Members.Skip(1).Take(1).ToList()
						},
						new HttpResponseMessage
						{
							Content = new StringContent(""),
							Headers =
							{
								{ "Link", "<https://example.com/some-roster?limit=3&page=3>;rel=\"next\"" }
							}
						}
					),
					(
						new MembershipContainer
						{
							Id = _membershipContainer.Id,
							Members = _membershipContainer.Members.Skip(2).ToList()
						},
						new HttpResponseMessage
						{
							Content = new StringContent(""),
							Headers =
							{
								// return the same "Link" on the last page
								{ "Link", "<https://example.com/some-roster?limit=3&page=3>;rel=\"next\"" }
							}
						}
					)
				});
		_ltiApiService = mockLtiApiService.Object;

		var service = GetService();
		var rosterEntries = await service.GetRosterAsync(new LtiExternalProvider(), _externalGroup, CancellationToken.None);
		Assert.IsTrue(rosterEntries.SequenceEqual(_expectedResult, new ExternalRosterEntryEqualityComparer()));
	}

	#region Helpers

	private LtiRosterProviderService<TestUser, TestExternalGroup, TestExternalGroupUser> GetService()
	{
		return new LtiRosterProviderService<TestUser, TestExternalGroup, TestExternalGroupUser>(_ltiApiService, _userFactory,
			_errorHandler);
	}

	#endregion Helpers
}