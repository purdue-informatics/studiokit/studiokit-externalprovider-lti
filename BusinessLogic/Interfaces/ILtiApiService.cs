﻿using StudioKit.ExternalProvider.Lti.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;

public interface ILtiApiService
{
	Task<(T ResponseObject, HttpResponseMessage ResponseMessage)> GetResultAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

	Task<List<(T ResponseObject, HttpResponseMessage ResponseMessage)>> GetResultsAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

	Task<(T ResponseObject, HttpResponseMessage ResponseMessage)> PostAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

	Task<(T ResponseObject, HttpResponseMessage ResponseMessage)> PutAsync<T>(
		LtiExternalProvider ltiExternalProvider,
		string url,
		Func<Uri, HttpContent> getHttpContent,
		CancellationToken cancellationToken = default,
		MediaTypeWithQualityHeaderValue acceptHeaderValue = null);
}