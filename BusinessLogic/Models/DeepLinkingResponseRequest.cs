﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class DeepLinkingResponseRequest
{
	public int GradableId { get; set; }

	public string Url { get; set; }
}