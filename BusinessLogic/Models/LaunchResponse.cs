﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class LaunchResponse
{
	public string TargetLinkUri { get; set; }

	public int? LtiLaunchId { get; set; }

	public int? GroupId { get; set; }
}