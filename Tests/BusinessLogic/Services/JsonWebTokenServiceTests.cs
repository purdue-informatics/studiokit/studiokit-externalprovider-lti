﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.BusinessLogic.Services;
using StudioKit.Security.Common;
using StudioKit.Tests;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using BaseConfiguration = StudioKit.Scaffolding.Models.BaseConfiguration;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services;

[TestClass]
public class JsonWebTokenServiceTests : BaseTest
{
	private SigningCredentials _signingCredentials;
	private JsonWebKeySet _jsonWebKeySet;
	private ILtiKeySetProviderService _keySetProviderService;
	private ICertificateConfigurationProvider _certificateConfigurationProvider;

	private readonly LtiExternalProvider _ltiExternalProvider = TestUtils.GetLtiExternalProvider();

	[TestInitialize]
	public void Initialize()
	{
		var (signingCredentials, jsonWebKeySet) = TestUtils.GetCredentials();
		_signingCredentials = signingCredentials;
		_jsonWebKeySet = jsonWebKeySet;

		var mockKeySetProviderService = new Mock<ILtiKeySetProviderService>();
		mockKeySetProviderService.Setup(x => x.GetKeySetAsync(It.IsAny<LtiExternalProvider>(), It.IsAny<CancellationToken>()))
			.ReturnsAsync(_jsonWebKeySet);
		_keySetProviderService = mockKeySetProviderService.Object;

		var ltiConfiguration = new BaseConfiguration
		{
			RsaKeyPair = TestUtils.TestRsaKeyPair
		};

		var mockConfigurationProvider = new Mock<ICertificateConfigurationProvider>();
		mockConfigurationProvider.Setup(x => x.GetCertificateConfigurationAsync(It.IsAny<CancellationToken>()))
			.ReturnsAsync(ltiConfiguration);
		_certificateConfigurationProvider = mockConfigurationProvider.Object;
	}

	#region GetTokenProviderAsync

	[TestMethod]
	public void GetTokenProviderAsync_ShouldThrowIfTokenIsNull()
	{
		using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		var service = GetService(dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetTokenProviderAsync(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("token"));
	}

	[TestMethod]
	public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfNoProvidersInDatabase()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		var service = GetService(dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var externalProvider = await service.GetTokenProviderAsync(token);
		Assert.IsNull(externalProvider);
	}

	[TestMethod]
	public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfNoDeploymentIdClaim()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary.Remove(LtiClaim.DeploymentId);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
		var externalProvider = await service.GetTokenProviderAsync(token);
		Assert.IsNull(externalProvider);
	}

	[TestMethod]
	public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfDeploymentIdClaimDoesNotMatch()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.DeploymentId] = "2";
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
		var externalProvider = await service.GetTokenProviderAsync(token);
		Assert.IsNull(externalProvider);
	}

	[TestMethod]
	public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfAudienceClaimDoesNotMatch()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[JsonWebTokenClaim.Audience] = "6789";
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, "6789", TestUtils.GetClaims(claimsDictionary));
		var externalProvider = await service.GetTokenProviderAsync(token);
		Assert.IsNull(externalProvider);
	}

	#endregion GetTokenProviderAsync

	#region AssertIsTokenValidAsync

	[TestMethod]
	public void AssertIsTokenValidAsync_ShouldThrowIfNullTokenString()
	{
		using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		var service = GetService(dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.AssertIsTokenValidAsync(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.AssertIsTokenValidAsync(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));
	}

	[TestMethod]
	public async Task AssertIsTokenValidAsync_ShouldThrowForMissingClaims()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var token = TestUtils.GetToken(_signingCredentials, null);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Issuer));

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, null);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Audience));

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(JsonWebTokenClaim.Subject);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience,
					TestUtils.GetClaims(claimsDictionary));
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Subject));

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(JsonWebTokenClaim.IssuedAt);
				var token = new JwtSecurityToken(TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary), null,
					DateTime.UtcNow.AddHours(1), _signingCredentials);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.IssuedAt));

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(JsonWebTokenClaim.Expiration);
				var token = new JwtSecurityToken(TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary),
					DateTime.UtcNow, null, _signingCredentials);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, JsonWebTokenClaim.Expiration));

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(OidcTokenClaim.Nonce);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience,
					TestUtils.GetClaims(claimsDictionary));
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, OidcTokenClaim.Nonce));

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.DeploymentId);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience,
					TestUtils.GetClaims(claimsDictionary));
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.DeploymentId));
	}

	[TestMethod]
	public async Task AssertIsTokenValidAsync_ShouldThrowForMoreThanOneAudience()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary[JsonWebTokenClaim.Audience] = new[] { TestUtils.Audience, "5678" };
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience,
					TestUtils.GetClaims(claimsDictionary));
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), Strings.TokenUntrustedAudiences);
	}

	[TestMethod]
	public void AssertIsTokenValidAsync_ShouldThrowIfProviderNotRegistered()
	{
		using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		var service = GetService(dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var token = TestUtils.GetToken(_signingCredentials);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), Strings.AudienceDeploymentIdNotRegistered);
	}

	[TestMethod]
	public async Task AssertIsTokenValidAsync_ShouldThrowIfAuthorizedPartyClaimDoesNotMatchClientId()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary[OidcTokenClaim.AuthorizedParty] = "notTheRightValue";
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience,
					TestUtils.GetClaims(claimsDictionary));
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), Strings.AzpClaimDoesNotMatchClientId);
	}

	[TestMethod]
	public async Task AssertIsTokenValidAsync_ShouldThrowIfTokenIsExpired()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () =>
			{
				var issuedAt = DateTime.UtcNow.AddHours(-2);
				var expires = DateTime.UtcNow.AddHours(-1);
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary(issuedAt, expires);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience,
					TestUtils.GetClaims(claimsDictionary), issuedAt, expires);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}), Strings.ValidateTokenFailed);
	}

	[TestMethod]
	public async Task AssertIsTokenValidAsync_ShouldSucceedWithValidToken()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
	}

	#endregion AssertIsTokenValidAsync

	#region CreateJwtAsync

	[TestMethod]
	public void CreateJwtAsync_ShouldThrowIfProviderNull()
	{
		using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		var service = GetService(dbContext);
		var claimList = new List<Claim>
		{
			new("type1", "value1")
		};

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateJwtAsync(null, claimList);
			}), ExceptionUtils.ArgumentNullExceptionMessage("provider")
		);
	}

	[TestMethod]
	public async Task CreateJwtAsync_ShouldSucceedIfClaimsNull()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		var token = await service.CreateJwtAsync(_ltiExternalProvider);

		// Serializing should not throw an exception
		var jwtHandler = new JwtSecurityTokenHandler();
		jwtHandler.WriteToken(token);

		// Check the token contains the required Claims for LTI
		Assert.AreEqual(_ltiExternalProvider.OauthClientId, token.Issuer);
		Assert.AreEqual(1, token.Audiences.Count());
		Assert.AreEqual(_ltiExternalProvider.Issuer, token.Audiences.First());
		Assert.AreNotEqual(DateTime.MinValue, token.ValidTo); // 'exp' claim
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(JsonWebTokenClaim.IssuedAt)));
	}

	[TestMethod]
	public async Task CreateJwtAsync_ShouldSucceedAndIncludeLtiClaims()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);
		var claimList = new List<Claim>
		{
			new("type1", "value1"),
			new("type2", "value2")
		};
		var token = await service.CreateJwtAsync(_ltiExternalProvider, claimList);

		// Serializing should not throw an exception
		var jwtHandler = new JwtSecurityTokenHandler();
		jwtHandler.WriteToken(token);

		// Should contain the passed in claim
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals("type1") && c.Value.Equals("value1")));
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals("type2") && c.Value.Equals("value2")));
	}

	[TestMethod]
	public async Task CreateJwtAsync_ShouldThrowIfCustomIssuerInvalid()
	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		const string issuerValue = "invalidIssuer";
		var claimList = new List<Claim>
		{
			new(JsonWebTokenClaim.Issuer, issuerValue)
		};

		Assert.ThrowsAsync<LtiInvalidJwtException>(
			Task.Run(async () =>
			{
				await service.CreateJwtAsync(_ltiExternalProvider, claimList);
			}), $"Invalid claim {JsonWebTokenClaim.Issuer} with value {issuerValue}"
		);
	}

	[TestMethod]
	public async Task CreateJwtAsync_ShouldNotOverrideOrDuplicateRequiredClaims()

	{
		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient();
		await SeedExternalProviderAsync(dbContext);
		var service = GetService(dbContext);

		var issuerValue = _ltiExternalProvider.OauthClientId;
		const string audienceValue = "audience";
		const string nonceValue = "nonce-1234";
		var dateValue = DateTime.UtcNow.AddDays(-5);
		var issuedAtTimeSpan = dateValue.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
		var issuedAtValue = Convert.ToInt64(issuedAtTimeSpan.TotalSeconds).ToString();
		var expirationTimeSpan = dateValue.AddMinutes(3).Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
		var expirationValue = Convert.ToInt64(expirationTimeSpan.TotalSeconds).ToString();
		var claimList = new List<Claim>
		{
			new(JsonWebTokenClaim.Issuer, issuerValue),
			new(JsonWebTokenClaim.Audience, audienceValue),
			new(JsonWebTokenClaim.Expiration, expirationValue),
			new(JsonWebTokenClaim.IssuedAt, issuedAtValue),
			new(OidcTokenClaim.Nonce, nonceValue)
		};

		var token = await service.CreateJwtAsync(_ltiExternalProvider, claimList);

		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(JsonWebTokenClaim.Issuer)));
		Assert.AreEqual(issuerValue, token.Issuer);
		Assert.AreEqual(1, token.Audiences.Count());
		Assert.AreEqual(audienceValue, token.Audiences.Single());
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(JsonWebTokenClaim.Expiration)));
		Assert.AreEqual(expirationValue, token.Claims.Single(c => c.Type.Equals(JsonWebTokenClaim.Expiration)).Value);
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(JsonWebTokenClaim.IssuedAt)));
		Assert.AreEqual(issuedAtValue, token.Claims.Single(c => c.Type.Equals(JsonWebTokenClaim.IssuedAt)).Value);
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(OidcTokenClaim.Nonce)));
		Assert.AreEqual(nonceValue, token.Claims.Single(c => c.Type.Equals(OidcTokenClaim.Nonce)).Value);
	}

	#endregion CreateJwtAsync

	#region Helpers

	private JsonWebTokenService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser> GetService(
		TestLtiLaunchDbContext dbContext)
	{
		dbContext.ChangeTracker.Clear();
		return new JsonWebTokenService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser>(
			dbContext, _keySetProviderService, new CertificateService(_certificateConfigurationProvider));
	}

	private async Task SeedExternalProviderAsync(TestLtiLaunchDbContext dbContext, CancellationToken cancellationToken = default)
	{
		dbContext.LtiExternalProviders.Add(_ltiExternalProvider);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	#endregion Helpers
}