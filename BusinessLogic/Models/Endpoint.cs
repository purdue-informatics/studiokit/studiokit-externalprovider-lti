﻿using Newtonsoft.Json;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class Endpoint
{
	public string[] Scope { get; set; }

	[JsonProperty("lineitems")]
	public string LineItems { get; set; }

	[JsonProperty("lineitem")]
	public string LineItem { get; set; }
}