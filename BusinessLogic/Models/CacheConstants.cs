﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class CacheConstants
{
	public const string LtiStateCachePrefix = "lti:state:";
	public const string LtiNonceCachePrefix = "lti:nonce:";
	public const string ExistentialCacheValue = "exists";
}