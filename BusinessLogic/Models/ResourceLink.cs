﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class ResourceLink
{
	public string Id { get; set; }

	public string Title { get; set; }

	public string Description { get; set; }
}