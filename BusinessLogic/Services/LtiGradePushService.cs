﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services;

public class LtiGradePushService<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser, TExternalGradable, TGradable,
	TScore, TGradableJobLog> : ILtiGradePushService
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>
	where TGroupUserRole : class, IGroupUserRole
	where TExternalTerm : class, IExternalTerm
	where TExternalGroup : class, IExternalGroup
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>
	where TExternalGradable : class, IExternalGradable, new()
	where TGradable : class, IGradable
	where TScore : class, IScore
	where TGradableJobLog : class, IGradableJobLog, new()
{
	private readonly IGradePushDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser, TExternalGradable,
		TGradable,
		TScore, TGradableJobLog> _dbContext;

	private readonly ILtiApiService _ltiApiService;
	private readonly IErrorHandler _errorHandler;

	private readonly ILogger<LtiGradePushService<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser,
		TExternalGradable, TGradable, TScore, TGradableJobLog>> _logger;

	private readonly IDateTimeProvider _dateTimeProvider;

	public LtiGradePushService(
		IGradePushDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser, TExternalGradable, TGradable, TScore,
			TGradableJobLog> dbContext,
		ILtiApiService ltiApiService,
		IErrorHandler errorHandler,
		ILogger<LtiGradePushService<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser, TExternalGradable, TGradable,
			TScore, TGradableJobLog>> logger,
		IDateTimeProvider dateTimeProvider)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_ltiApiService = ltiApiService ?? throw new ArgumentNullException(nameof(ltiApiService));
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		_dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
	}

	public async Task PushGradesAsync(
		int gradableId,
		IPrincipal principal,
		bool shouldSave = true,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		if (!(principal is SystemPrincipal))
			throw new ForbiddenException(string.Format(Security.Properties.Strings.RequiresSystemPrincipal, nameof(PushGradesAsync)));

		// Load the Gradable
		// NOTE: casting is required so that IGradePushDbContext and ILtiLaunchDbContext
		// can share a method signature that uses the interface (type generics are really annoying)
		var gradable = (TGradable)await _dbContext.GetGradableAsync(gradableId, cancellationToken);

		// Require Points
		if (!gradable.Points.HasValue || gradable.Points.Value <= 0)
		{
			_logger.LogError(Strings.GradablePointsRequired);
			throw new Exception(Strings.GradablePointsRequired);
		}

		var externalGroupsQueryable = _dbContext.ExternalGroups
			.Where(eg =>
				eg.GroupId.Equals(gradable.GroupId) &&
				eg.GradesUrl != null &&
				eg.GradesUrl.Trim() != string.Empty &&
				eg.ExternalProvider.GradePushEnabled);

		var externalGroups = await externalGroupsQueryable
			.Include(eg => eg.ExternalProvider)
			.ToListAsync(cancellationToken);

		// Require that GradePush is enabled
		if (!externalGroups.Any())
		{
			_logger.LogError(Strings.GradePushExternalGroupsRequired);
			throw new Exception(Strings.GradePushExternalGroupsRequired);
		}

		var scores = await _dbContext.GetScoresAsync(gradableId, cancellationToken);

		// Require Scores
		if (scores == null || !scores.Any())
		{
			_logger.LogError(Strings.GradePushScoresRequired);
			throw new Exception(Strings.GradePushScoresRequired);
		}

		var externalGradables = await _dbContext.GetExternalGradablesAsync(gradableId, cancellationToken);

		// Find ExternalGroups that need an ExternalGradable
		var externalGroupsWithNoExternalGradable = externalGroups
			.Where(eg => !externalGradables.Any(ega => ega.ExternalGroupId.Equals(eg.Id))).ToList();

		var externalGradablesToRemove = new List<TExternalGradable>();
		var externalGradablesToAdd = new List<TExternalGradable>();
		var updatedScoreMaximumLineItemIds = new List<string>();

		// Load LineItems for existing ExternalGradables
		// 1. Check if they were deleted in the platform, so a new LineItem can be created
		// 2. If they exist, update if needed
		foreach (var externalGradable in externalGradables)
		{
			var externalGroup = externalGroups.Single(eg => eg.Id.Equals(externalGradable.ExternalGroupId));
			var ltiExternalProvider = externalGroup.ExternalProvider as LtiExternalProvider;
			var sharedMessage =
				$"{externalGradable.ExternalId} (ExternalGradable = {externalGradable.Id}, ExternalGroup = {externalGroup.Id})";

			// LineItem was never connected, so add the existing ExternalGradable to a list to be deleted, and flag the ExternalGroup as needing to create a new one
			// e.g. a deep link was made, but it was never launched
			if (string.IsNullOrWhiteSpace(externalGradable.ExternalId))
			{
				_logger.LogDebug($"LineItem Not Connected {sharedMessage}");
				externalGradablesToRemove.Add(externalGradable);
				externalGroupsWithNoExternalGradable.Add(externalGroup);
				continue;
			}

			_logger.LogDebug($"Loading LineItem {sharedMessage}");

			try
			{
				var (lineItem, _) = await _ltiApiService.GetResultAsync<LineItem>(
					ltiExternalProvider,
					externalGradable.ExternalId,
					cancellationToken,
					new MediaTypeWithQualityHeaderValue(LineItem.ContentType)
				);

				_logger.LogDebug($"Loaded LineItem {sharedMessage}");

				// Update LineItem if Points or Name has changed
				if (lineItem.ScoreMaximum != gradable.Points.Value || lineItem.Label != gradable.Name)
				{
					_logger.LogDebug(
						$"Updating LineItem (Label = {gradable.Name}, ScoreMaximum = {gradable.Points.Value}) {sharedMessage}");

					var lineItemUpdate = CreateLineItem(gradable, lineItem);
					await _ltiApiService.PutAsync<LineItem>(
						externalGroup.ExternalProvider as LtiExternalProvider,
						externalGradable.ExternalId,
						_ => new StringContent(JsonConvert.SerializeObject(lineItemUpdate), Encoding.UTF8, LineItem.ContentType),
						cancellationToken,
						new MediaTypeWithQualityHeaderValue(LineItem.ContentType));
					if (lineItem.ScoreMaximum != gradable.Points.Value)
					{
						updatedScoreMaximumLineItemIds.Add(externalGradable.ExternalId);
					}

					_logger.LogDebug($"Updated LineItem {sharedMessage}");
				}
			}
			catch (Exception e)
			{
				if (e.InnerException is HttpRequestException &&
					e.InnerException.Message.Contains(Strings.HttpRequestExceptionNotFound))
				{
					_logger.LogDebug($"LineItem Not Found {sharedMessage}");

					// LineItem was deleted, so add the existing ExternalGradable to a list to be deleted, and flag the ExternalGroup as needing to create a new one
					externalGradablesToRemove.Add(externalGradable);
					externalGroupsWithNoExternalGradable.Add(externalGroup);
				}
				else
				{
					_logger.LogError($"LineItem Failure {sharedMessage}");
					throw;
				}
			}
		}

		// Create new LineItems and ExternalGradables if needed
		foreach (var externalGroup in externalGroupsWithNoExternalGradable)
		{
			var sharedMessage = $"(ExternalGroup = {externalGroup.Id})";

			_logger.LogDebug($"Creating LineItem {sharedMessage}");

			// POST to externalGroup.GradesUrl
			var lineItemToCreate = CreateLineItem(gradable);
			var (lineItem, _) = await _ltiApiService.PostAsync<LineItem>(
				externalGroup.ExternalProvider as LtiExternalProvider,
				externalGroup.GradesUrl,
				_ => new StringContent(JsonConvert.SerializeObject(lineItemToCreate), Encoding.UTF8, LineItem.ContentType),
				cancellationToken,
				new MediaTypeWithQualityHeaderValue(LineItem.ContentType));

			// Create new ExternalGradable
			var externalGradable = new TExternalGradable
			{
				ExternalGroupId = externalGroup.Id,
				GradableId = gradableId,
				ExternalId = lineItem.Id
			};

			_logger.LogDebug($"Created LineItem {lineItem.Id} {sharedMessage}");

			// Add to list to be saved
			externalGradablesToAdd.Add(externalGradable);
			// Add to list for future processing
			externalGradables.Add(externalGradable);
		}

		if (externalGradablesToRemove.Any() || externalGradablesToAdd.Any())
		{
			if (externalGradablesToRemove.Any())
			{
				_logger.LogDebug($"Removing ExternalGradables ({externalGradablesToRemove.Count})");
				_dbContext.GetExternalGradables().RemoveRange(externalGradablesToRemove);
				externalGradables = externalGradables.Except(externalGradablesToRemove).ToList();
			}

			if (externalGradablesToAdd.Any())
			{
				_logger.LogDebug($"Adding ExternalGradables ({externalGradablesToAdd.Count})");
				_dbContext.GetExternalGradables().AddRange(externalGradablesToAdd);
			}

			await _dbContext.SaveChangesAsync(cancellationToken);
			_logger.LogDebug("DbContext Saved Changes");
		}

		// Load ExternalGroupUsers that have a Learner
		_logger.LogDebug("Loading ExternalGroupUsers");

		var externalGroupUsers = await externalGroupsQueryable
			.Join(_dbContext.ExternalGroupUsers, eg => eg.Id, egu => egu.ExternalGroupId, (eg, egu) => egu)
			.ToListAsync(cancellationToken);

		// filter by Learner roles after load in order to use string split for accurate matching
		var learnerRoles = new List<string> { LtiRole.MembershipLearner, LtiRole.Learner };
		var externalGroupUserLearners = externalGroupUsers
			.Where(egu => egu.Roles.Split(',').Any(r => learnerRoles.Contains(r)))
			.ToList();

		_logger.LogDebug($"Loaded ExternalGroupUsers (Total = {externalGroupUsers.Count}, Learners = {externalGroupUserLearners.Count})");

		// Send Scores for ExternalGradables
		foreach (var externalGradable in externalGradables)
		{
			var externalGroup = externalGroups.Single(eg => eg.Id.Equals(externalGradable.ExternalGroupId));
			var ltiExternalProvider = externalGroup.ExternalProvider as LtiExternalProvider;
			var didJustUpdateScoreMaximum = updatedScoreMaximumLineItemIds.Contains(externalGradable.ExternalId);

			var sharedMessage = $"(ExternalGradable = {externalGradable.Id}, ExternalGroup = {externalGroup.Id})";

			_logger.LogDebug($"Loading existing Results {sharedMessage}");

			// Load existing results from the platform
			var existingResultsResponses = await _ltiApiService.GetResultsAsync<List<Result>>(
				ltiExternalProvider,
				$"{externalGradable.ExternalId}/results",
				cancellationToken,
				new MediaTypeWithQualityHeaderValue(Result.ContentType)
			);
			var existingResults = existingResultsResponses
				.SelectMany(r => r.ResponseObject)
				.ToList();

			_logger.LogDebug($"Loaded existing Results ({existingResults.Count}) {sharedMessage}");

			// Filter to ExternalGroupUsers for the current ExternalGroup
			var externalGroupUsersForExternalGroup = externalGroupUserLearners
				.Where(egu => egu.ExternalGroupId.Equals(externalGroup.Id))
				.ToList();

			foreach (var externalGroupUser in externalGroupUsersForExternalGroup)
			{
				var score = scores.FirstOrDefault(s => s.UserId.Equals(externalGroupUser.UserId));

				sharedMessage =
					$"(ExternalGradable = {externalGradable.Id}, ExternalGroup = {externalGroup.Id}, ExternalUserId = {externalGroupUser.ExternalUserId}, UserId = {externalGroupUser.UserId})";

				// Skip if score has no points
				if (score?.TotalPoints == null)
				{
					_logger.LogDebug($"No score or points, skipping {sharedMessage}");
					continue;
				}

				// Find existing result if any
				var existingResult = existingResults.SingleOrDefault(r => r.UserId.Equals(externalGroupUser.ExternalUserId));

				// Skip if the existing result matches the score
				if (existingResult != null && existingResult.ResultScore.Equals(score.TotalPoints))
				{
					_logger.LogDebug($"Existing Result is already {score.TotalPoints} Points, skipping {sharedMessage}");
					continue;
				}

				// Grade Push: POST score to {externalGradable.ExternalId}/scores
				try
				{
					// When the LineItem's ScoreMaximum is updated, Brightspace treats this as an update to ALL scores
					// Any scores would therefore be rejected as being "older" by Brightspace
					// Override the timestamp to allow any score to be treated as new
					// If any further updates are made to the answer after, things should go back to normal
					var timestamp = score.DateLastUpdated;
					if (didJustUpdateScoreMaximum)
					{
						timestamp = _dateTimeProvider.UtcNow;
						_logger.LogDebug($"Override Score Timestamp ({score.DateLastUpdated:O} => {timestamp:O}) {sharedMessage}");
					}

					_logger.LogDebug(
						$"Pushing Score (Score = {score.TotalPoints}/{gradable.Points}, Timestamp = {timestamp:O}) {sharedMessage}");

					var scoreToCreate = new Score
					{
						UserId = externalGroupUser.ExternalUserId,
						ActivityProgress = LtiAgsScoreActivityProgress.Completed,
						GradingProgress = LtiAgsScoreGradingProgress.FullyGraded,
						ScoreGiven = score.TotalPoints.Value,
						ScoreMaximum = gradable.Points.Value,
						Timestamp = timestamp
					};
					await _ltiApiService.PostAsync<Result>(
						ltiExternalProvider,
						$"{externalGradable.ExternalId}/scores",
						_ => new StringContent(JsonConvert.SerializeObject(scoreToCreate), Encoding.UTF8,
							Score.ContentType),
						cancellationToken,
						new MediaTypeWithQualityHeaderValue(Result.ContentType));

					_logger.LogDebug($"Pushed Score {sharedMessage}");
				}
				catch (Exception e)
				{
					// Do not stop processing if a learner has a grade push conflict
					if (e.InnerException is HttpRequestException &&
						e.InnerException.Message.Contains(Strings.HttpRequestExceptionConflict))
					{
						// TODO - Grade Sync: possibly store a new IScore when the platform result's score is newer
						// Capture grade push conflict and continue
						var message = string.Format(Strings.GradePushConflictEncountered, externalGradable.Id, externalGroup.Id,
							externalGroupUser.ExternalUserId, externalGroupUser.UserId);
						_logger.LogWarning(message);
						_errorHandler.CaptureException(new LtiException(message, e));
					}
					// Do not stop processing if a learner is not found or enrolled by the platform
					else if (e.InnerException is HttpRequestException &&
							(e.InnerException.Message.Contains(Strings.HttpRequestExceptionNotFound) &&
							e.InnerException.Message.Contains(Strings.GradePushUserDoesNotExist) ||
							e.InnerException.Message.Contains(Strings.HttpRequestExceptionForbidden) &&
							e.InnerException.Message.Contains(Strings.GradePushUserNotEnrolled) ||
							e.InnerException.Message.Contains(Strings.HttpRequestExceptionUnprocessableEntity) &&
							e.InnerException.Message.Contains(Strings.GradePushScoreCannotBePublished)))
					{
						// Capture exception and continue
						var message = string.Format(Strings.GradePushExternalUserNotFound, externalGradable.Id,
							externalGroup.Id, externalGroupUser.ExternalUserId, externalGroupUser.UserId);
						_logger.LogWarning(message);
						_errorHandler.CaptureException(new LtiException(message, e));
					}
					else
					{
						// Throw any other exceptions, with additional details
						var message = string.Format(Strings.GradePushFailed, externalGradable.Id, externalGroup.Id,
							externalGroupUser.ExternalUserId, externalGroupUser.UserId);
						_logger.LogError(message);
						throw new LtiException(message, e);
					}
				}
			}
		}
	}

	private static LineItem CreateLineItem(TGradable gradable, LineItem existingLineItem = null)
	{
		return new LineItem
		{
			// ReSharper disable once PossibleInvalidOperationException - gradable has already been validated to have points
			ScoreMaximum = gradable.Points.Value,
			Label = gradable.Name,
			// These are not allowed to change, so preserve the existing values, if any
			// Set ResourceId on creation
			ResourceId = existingLineItem != null ? existingLineItem.ResourceId : gradable.Id.ToString(),
			ResourceLinkId = existingLineItem?.ResourceLinkId,
			Tag = existingLineItem?.Tag
		};
	}
}