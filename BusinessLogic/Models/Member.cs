﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class Member
{
	public string Status { get; set; }

	[JsonProperty("context_id")]
	public string ContextId { get; set; }

	[JsonProperty("context_label")]
	public string ContextLabel { get; set; }

	[JsonProperty("context_title")]
	public string ContextTitle { get; set; }

	public string Name { get; set; }

	public string Picture { get; set; }

	[JsonProperty("given_name")]
	public string GivenName { get; set; }

	[JsonProperty("family_name")]
	public string FamilyName { get; set; }

	[JsonProperty("middle_name")]
	public string MiddleName { get; set; }

	public string Email { get; set; }

	[JsonProperty("user_id")]
	public string UserId { get; set; }

	public List<string> Roles { get; set; }

	public bool IsActive => string.IsNullOrWhiteSpace(Status) || Status.Equals(LisStatus.Active);
}