﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using Newtonsoft.Json;
using StudioKit.Caching.Interfaces;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.EqualityComparers;
using StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Lti.Tests.TestData.Services;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.BusinessLogic.Services;
using StudioKit.Security.Common;
using StudioKit.Tests;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using BaseConfiguration = StudioKit.Scaffolding.Models.BaseConfiguration;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services;

[TestClass]
public class LtiServiceTests : BaseTest
{
	private SigningCredentials _signingCredentials;
	private JsonWebKeySet _jsonWebKeySet;
	private ILtiKeySetProviderService _keySetProviderService;
	private ICertificateConfigurationProvider _certificateConfigurationProvider;

	private static DbConnection _dbConnection;
	private TestLtiLaunchDbContext _dbContext;
	private IDbContextTransaction _transaction;
	private IAsyncCacheClient _cacheClient;
	private readonly Dictionary<string, object> _cache = new();

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		_dbConnection = DbConnectionFactory.CreateSqliteInMemoryConnection();
		await _dbConnection.OpenAsync();

		await using var dbContext = DbContextFactory<TestLtiLaunchDbContext>.CreateTransient(_dbConnection);
		await SeedAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEachTest()
	{
		var (signingCredentials, jsonWebKeySet) = TestUtils.GetCredentials();
		_signingCredentials = signingCredentials;
		_jsonWebKeySet = jsonWebKeySet;

		var mockKeySetProviderService = new Mock<ILtiKeySetProviderService>();
		mockKeySetProviderService.Setup(x => x.GetKeySetAsync(It.IsAny<LtiExternalProvider>(), It.IsAny<CancellationToken>()))
			.ReturnsAsync(_jsonWebKeySet);
		_keySetProviderService = mockKeySetProviderService.Object;

		var ltiConfiguration = new BaseConfiguration
		{
			RsaKeyPair = TestUtils.TestRsaKeyPair
		};

		var mockConfigurationProvider = new Mock<ICertificateConfigurationProvider>();
		mockConfigurationProvider.Setup(x => x.GetCertificateConfigurationAsync(It.IsAny<CancellationToken>()))
			.ReturnsAsync(ltiConfiguration);
		_certificateConfigurationProvider = mockConfigurationProvider.Object;

		var mockCacheClient = new Mock<IAsyncCacheClient>();
		mockCacheClient.Setup(c => c.PutAsync(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<TimeSpan>()))
			.Callback<string, object, TimeSpan>((k, v, _) => _cache[k] = v)
			.ReturnsAsync(true);
		mockCacheClient.Setup(c => c.GetAsync<string>(It.IsAny<string>()))
			.ReturnsAsync((string k) => _cache.ContainsKey(k) ? (string)_cache[k] : null);
		mockCacheClient.Setup(c => c.RemoveAsync(It.IsAny<string>()))
			.Callback<string>(k => _cache.Remove(k))
			.ReturnsAsync(true);
		_cacheClient = mockCacheClient.Object;

		(_dbContext, _transaction) = DbContextFactory<TestLtiLaunchDbContext>.CreateTransientAndTransaction(_dbConnection);
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		_dbConnection.Close();
		_dbConnection.Dispose();
	}

	#region PrepareOidcRequest

	[TestMethod]
	public void PrepareOidcRedirect_ShouldThrowWithoutIssuer()
	{
		Assert.ThrowsAsync<InvalidOperationException>(Task.Run(async () =>
			await GetLtiService(_dbContext)
				.PrepareOidcRedirectAsync(new List<KeyValuePair<string, string>>(), null)));
	}

	[TestMethod]
	public void PrepareOidcRedirect_ShouldThrowWithoutPersistedProvider()
	{
		const string issuer = "test_issuer";

		Assert.ThrowsAsync<InvalidOperationException>(Task.Run(async () =>
			await GetLtiService(_dbContext)
				.PrepareOidcRedirectAsync(new List<KeyValuePair<string, string>>
					{
						new(JsonWebTokenClaim.Issuer, issuer)
					},
					null)));
	}

	[TestMethod]
	public async Task PrepareOidcRedirect_ShouldCacheState()
	{
		const string externalProviderName = "TestLtiProvider";
		const string issuer = "test_issuer";
		const string deploymentId = "test_deployment_id";
		const string oauthClientId = "test_client_id";
		const string oauthTokenUrl = "http://tokenUrl/";
		const string openIdConnectAuthenticationEndpoint = "http://oidcConnect/";
		const string oauthAudience = "audience";
		const string keySetUrl = "http://keyset/";
		const string ltiMessageHint = "test_message_hint";
		const string loginHint = "test_login_hint";
		_dbContext.LtiExternalProviders.Add(new LtiExternalProvider
		{
			Name = externalProviderName,
			Issuer = issuer,
			DeploymentId = deploymentId,
			OauthClientId = oauthClientId,
			OauthTokenUrl = oauthTokenUrl,
			OpenIdConnectAuthenticationEndpoint = openIdConnectAuthenticationEndpoint,
			OauthAudience = oauthAudience,
			KeySetUrl = keySetUrl
		});
		await _dbContext.SaveChangesAsync();
		await GetLtiService(_dbContext).PrepareOidcRedirectAsync(
			new List<KeyValuePair<string, string>>
			{
				new(JsonWebTokenClaim.Issuer, issuer),
				new(OidcAuthorizationRequestKey.LtiDeploymentId, deploymentId),
				new(OidcAuthorizationRequestKey.LtiMessageHint, ltiMessageHint),
				new(OidcAuthorizationRequestKey.LoginHint, loginHint)
			},
			null);
		Assert.AreEqual(1, _cache.Keys.Count);
		var key = _cache.Keys.First();
		Assert.AreEqual(CacheConstants.LtiStateCachePrefix,
			key.Substring(0, key.LastIndexOf(':') + 1));
		Assert.AreEqual("exists", _cache.Values.First());
	}

	#endregion PrepareOidcRequest

	#region GetLaunchPresentationReturnUrl

	[TestMethod]
	public void GetLaunchPresentationReturnUrl_ShouldThrowIfNullOrEmptyTokenString()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() =>
			{
				ltiService.GetLaunchPresentationReturnUrl(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() =>
			{
				ltiService.GetLaunchPresentationReturnUrl("");
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));
	}

	[TestMethod]
	public void GetLaunchPresentationReturnUrl_ShouldReturnUrlIfPresent()
	{
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var launchReturnUrl = ltiService.GetLaunchPresentationReturnUrl(TestUtils.GetTokenString(token));
		Assert.AreEqual(TestUtils.LaunchPresentationReturnUrl, launchReturnUrl);
	}

	[TestMethod]
	public void GetLaunchPresentationReturnUrl_ShouldReturnNullIfNoPresentationClaim()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary.Remove(LtiClaim.LaunchPresentation);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
		var launchReturnUrl = ltiService.GetLaunchPresentationReturnUrl(TestUtils.GetTokenString(token));
		Assert.IsNull(launchReturnUrl);
	}

	[TestMethod]
	public void GetLaunchPresentationReturnUrl_ShouldReturnNullIfNoReturnUrlInPresentationClaim()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var launchPresentation = TestUtils.LaunchPresentation;
		launchPresentation.ReturnUrl = null;
		claimsDictionary[LtiClaim.LaunchPresentation] = launchPresentation;
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
		var launchReturnUrl = ltiService.GetLaunchPresentationReturnUrl(TestUtils.GetTokenString(token));
		Assert.IsNull(launchReturnUrl);
	}

	#endregion GetLaunchPresentationReturnUrl

	#region GetLoginInfoAsync

	[TestMethod]
	public void GetLoginInfoAsync_ShouldThrowIfNullOrEmptyTokenString()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLoginInfoAsync(null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLoginInfoAsync("");
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));
	}

	[TestMethod]
	public async Task GetLoginInfoAsync_ShouldSucceedWithValidToken()
	{
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		Assert.IsNotNull(externalLoginInfo);
		Assert.AreEqual(TestUtils.Email, externalLoginInfo.Principal.Claims.First(c =>
			c.Type == ClaimTypes.Name &&
			c.Issuer == ClaimsIdentity.DefaultIssuer).Value);
		Assert.AreEqual(TestUtils.Email, externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Email));
		Assert.AreEqual(string.Format(Strings.LoginProviderFormat, "1"), externalLoginInfo.LoginProvider);
		Assert.AreEqual(TestUtils.Sub, externalLoginInfo.ProviderKey);
		Assert.AreEqual(36, externalLoginInfo.Principal.Claims.Count());
	}

	#endregion GetLoginInfoAsync

	#region GetLaunchResponseAsync

	[TestMethod]
	public void GetLaunchResponseAsync_ShouldThrowIfNullOrEmptyTokenString()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(null, null, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync("", null, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));
	}

	[TestMethod]
	public void GetLaunchResponseAsync_ShouldThrowIfNullOrEmptyUserId()
	{
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var tokenString = TestUtils.GetTokenString(token);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(tokenString, null, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("userId"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(tokenString, "", null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("userId"));
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldCreateLtiLaunchForInstructor()
	{
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNotNull(launchResponse.LtiLaunchId);
		Assert.IsNull(launchResponse.GroupId);

		var ltiLaunch =
			await _dbContext.LtiLaunches.SingleOrDefaultAsync(d => d.Id.Equals(launchResponse.LtiLaunchId.Value));
		Assert.IsNotNull(ltiLaunch);
		var expected = GenerateLtiLaunch();
		Assert.IsTrue(new LtiLaunchEqualityComparer().Equals(expected, ltiLaunch));
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldUseCourseDatesWhenProvided()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var customClaims =
			new Dictionary<string, object>
			{
				{ "myCustomValue", 123 },
				{ LtiCustomClaim.CourseSectionStartDateCustomName, "2020-05-04T05:00:00Z" },
				{ LtiCustomClaim.CourseSectionEndDateCustomName, "2020-05-05T05:00:00Z" }
			};
		claimsDictionary[LtiClaim.Custom] = customClaims;
		var claims = TestUtils.GetClaims(claimsDictionary);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNotNull(launchResponse.LtiLaunchId);
		Assert.IsNull(launchResponse.GroupId);

		var ltiLaunch = await _dbContext.LtiLaunches.SingleOrDefaultAsync(d => d.Id.Equals(launchResponse.LtiLaunchId.Value));
		Assert.IsNotNull(ltiLaunch);
		var expected = GenerateLtiLaunch();
		expected.CourseSectionStartDate = new DateTime(2020, 5, 4, 5, 0, 0, DateTimeKind.Utc);
		// End date is moved one second earlier to end at 59 seconds in the minute
		expected.CourseSectionEndDate = new DateTime(2020, 5, 5, 4, 59, 59, DateTimeKind.Utc);
		Assert.IsTrue(new LtiLaunchEqualityComparer().Equals(expected, ltiLaunch));

		Debug.Assert(ltiLaunch != null, nameof(ltiLaunch) + " != null");
		Debug.Assert(ltiLaunch.CourseSectionStartDate != null, "ltiLaunch.CourseSectionStartDate != null");
		Assert.AreEqual(ltiLaunch.CourseSectionStartDate.Value.Kind, DateTimeKind.Utc);

		Debug.Assert(ltiLaunch.CourseSectionEndDate != null, "ltiLaunch.CourseSectionEndDate != null");
		Assert.AreEqual(ltiLaunch.CourseSectionEndDate.Value.Kind, DateTimeKind.Utc);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldReturnNullCourseDatesWhenEmptyOrUnsupported()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var customClaims =
			new Dictionary<string, object>
			{
				{ "myCustomValue", 123 },
				{ LtiCustomClaim.CourseSectionStartDateCustomName, "" },
				{ LtiCustomClaim.CourseSectionEndDateCustomName, LtiCustomClaim.CourseSectionEndDateLisVariable }
			};
		claimsDictionary[LtiClaim.Custom] = customClaims;
		var claims = TestUtils.GetClaims(claimsDictionary);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNotNull(launchResponse.LtiLaunchId);
		Assert.IsNull(launchResponse.GroupId);

		var ltiLaunch =
			await _dbContext.LtiLaunches.SingleOrDefaultAsync(d => d.Id.Equals(launchResponse.LtiLaunchId.Value));
		Assert.IsNotNull(ltiLaunch);
		var expected = GenerateLtiLaunch();
		Assert.IsTrue(new LtiLaunchEqualityComparer().Equals(expected, ltiLaunch));
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldThrowIfLearnerAndContextNotSetup()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.Roles] = new[] { LtiRole.MembershipLearner };
		var claims = TestUtils.GetClaims(claimsDictionary);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
			}), Strings.CourseNotAvailable);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldThrowIfLearnerAndGroupIsDeleted()
	{
		var group = await SeedGroupAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.Roles] = new[] { LtiRole.MembershipLearner };
		var claims = TestUtils.GetClaims(claimsDictionary);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
			}), Strings.CourseNotAvailable);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldRestoreGroupIfInstructorAndGroupIsDeleted()
	{
		var group = await SeedGroupAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var claims = TestUtils.GetClaims(claimsDictionary);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);

		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNull(launchResponse.LtiLaunchId);
		group = await _dbContext.Groups.SingleAsync(g => g.Id == group.Id);
		Assert.AreEqual(group.Id, launchResponse.GroupId);
		Assert.IsFalse(group.IsDeleted);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldReturnGroupIdIfContextIsSetup()
	{
		var group = await SeedGroupAsync(_dbContext);
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNull(launchResponse.LtiLaunchId);
		Assert.AreEqual(group.Id, launchResponse.GroupId);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldUpdateExternalGroupDetails()
	{
		var group = await SeedGroupAsync(_dbContext);
		var externalGroup = group.ExternalGroups.First();
		externalGroup.Name = "Old Name";
		externalGroup.Description = "Old Description";
		externalGroup.RosterUrl = null;
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNull(launchResponse.LtiLaunchId);
		group = await _dbContext.Groups.SingleAsync(g => g.Id == group.Id);
		externalGroup = await _dbContext.ExternalGroups.SingleAsync(g => g.Id == externalGroup.Id);
		Assert.AreEqual(group.Id, launchResponse.GroupId);
		Assert.AreEqual(TestUtils.ContextTitle, externalGroup.Name);
		Assert.AreEqual(TestUtils.ContextLabel, externalGroup.Description);
		Assert.AreEqual(TestUtils.NamesRoleServiceMembershipsUrl, externalGroup.RosterUrl);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldUpdateExternalGroupUserRoles()
	{
		var group = await SeedGroupAsync(_dbContext);
		var externalGroup = group.ExternalGroups.First();
		externalGroup.Name = "Old Name";
		externalGroup.Description = "Old Description";
		externalGroup.RosterUrl = null;
		await _dbContext.SaveChangesAsync();

		var groupOwnerRole = await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner));
		_dbContext.ExternalGroupUsers.Add(new TestExternalGroupUser
		{
			UserId = "1",
			ExternalGroupId = externalGroup.Id,
			ExternalUserId = TestUtils.Sub,
			Roles = JsonConvert.SerializeObject(new[] { LtiRole.MembershipLearner })
		});
		_dbContext.GroupUserRoles.Add(new TestGroupUserRole
		{
			GroupId = group.Id,
			UserId = "1",
			RoleId = groupOwnerRole.Id
		});
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());

		var externalGroupUser = await _dbContext.ExternalGroupUsers.SingleOrDefaultAsync(u =>
			u.UserId.Equals("1") &&
			u.ExternalUserId.Equals(TestUtils.Sub) &&
			u.ExternalGroupId == externalGroup.Id);
		Assert.IsNotNull(externalGroupUser);
		Assert.AreEqual(LtiRole.MembershipInstructor, externalGroupUser?.Roles);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldAddLearnerToGroupIfNew()
	{
		var group = await SeedGroupAsync(_dbContext);
		var externalGroup = group.ExternalGroups.First();

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var roles = new[] { LtiRole.MembershipLearner };
		claimsDictionary[LtiClaim.Roles] = roles;
		var claims = TestUtils.GetClaims(claimsDictionary);
		var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		await ltiService.GetLaunchResponseAsync(tokenString, "2", externalLoginInfo.Principal.Claims.ToList());

		var externalGroupUser = await _dbContext.ExternalGroupUsers.SingleOrDefaultAsync(u =>
			u.UserId.Equals("2") &&
			u.ExternalUserId.Equals(TestUtils.Sub) &&
			u.ExternalGroupId == externalGroup.Id);
		Assert.IsNotNull(externalGroupUser);
		Assert.AreEqual(LtiRole.MembershipLearner, externalGroupUser?.Roles);

		var groupUserRole = await _dbContext.GroupUserRoles.SingleOrDefaultAsync(r =>
			r.UserId.Equals("2") && r.GroupId.Equals(group.Id) && r.Role.Name.Equals(BaseRole.GroupLearner));
		Assert.IsNotNull(groupUserRole);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldThrowIfTargetLinkUriAndExternalGroupCoursesDoNotMatch_InstructorMessage()
	{
		var (oldGroup, oldGroupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		oldGroup.ExternalGroups.First().ExternalId = "other-context-id";
		await _dbContext.SaveChangesAsync();

		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		var externalGroupId = group.ExternalGroups.First().Id;

		var externalGradable =
			await _dbContext.GetExternalGradableOrDefaultAsync(
				groupAssignment.Id,
				externalGroupId,
				TestUtils.EndpointLineItem);
		Assert.IsNull(externalGradable);

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		// Deep Link Uri is still targeting the old course, e.g. the Brightspace course was copied and not updated
		claimsDictionary[LtiClaim.TargetLinkUri] = $"https://tool.org/courses/{oldGroup.Id}/groupAssignments/{oldGroupAssignment.Id}";
		var claims = TestUtils.GetClaims(claimsDictionary);

		var token = TestUtils.GetToken(_signingCredentials, claims: claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(tokenString, "1",
					externalLoginInfo.Principal.Claims.ToList());
			}), string.Format(Strings.LtiDeepLinkInvalidCourseInstructorError, TestUtils.ResourceLink.Title));
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldThrowIfTargetLinkUriAndExternalGroupCoursesDoNotMatch_LearnerMessage()
	{
		var (oldGroup, oldGroupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		oldGroup.ExternalGroups.First().ExternalId = "other-context-id";
		await _dbContext.SaveChangesAsync();

		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		var externalGroupId = group.ExternalGroups.First().Id;

		var externalGradable =
			await _dbContext.GetExternalGradableOrDefaultAsync(
				groupAssignment.Id,
				externalGroupId,
				TestUtils.EndpointLineItem);
		Assert.IsNull(externalGradable);

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.Roles] = new[] { LtiRole.MembershipLearner };
		// Deep Link Uri is still targeting the old course, e.g. the Brightspace course was copied and not updated
		claimsDictionary[LtiClaim.TargetLinkUri] = $"https://tool.org/courses/{oldGroup.Id}/groupAssignments/{oldGroupAssignment.Id}";
		var claims = TestUtils.GetClaims(claimsDictionary);

		var token = TestUtils.GetToken(_signingCredentials, claims: claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(tokenString, "1",
					externalLoginInfo.Principal.Claims.ToList());
			}), string.Format(Strings.LtiDeepLinkInvalidCourseError, TestUtils.ResourceLink.Title));
	}

	#region ExternalGradable

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldCreateExternalGradableIfNeededWhenLineItemProvided()
	{
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		var externalGroupId = group.ExternalGroups.First().Id;

		var externalGradable =
			await _dbContext.GetExternalGradableOrDefaultAsync(
				groupAssignment.Id,
				externalGroupId,
				TestUtils.EndpointLineItem);
		Assert.IsNull(externalGradable);

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.TargetLinkUri] = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}";
		var claims = TestUtils.GetClaims(claimsDictionary);

		var token = TestUtils.GetToken(_signingCredentials, claims: claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		await ltiService.GetLaunchResponseAsync(tokenString, "1",
			externalLoginInfo.Principal.Claims.ToList());

		externalGradable =
			await _dbContext.GetExternalGradableOrDefaultAsync(
				groupAssignment.Id,
				externalGroupId,
				TestUtils.EndpointLineItem);
		Assert.IsNotNull(externalGradable);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldNotCreateExternalGradableForLineItemIfExists()
	{
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		var externalGroupId = group.ExternalGroups.First().Id;

		_dbContext.AddExternalGradable(new TestExternalGroupAssignment
		{
			ExternalGroupId = externalGroupId,
			GroupAssignment = groupAssignment,
			ExternalId = TestUtils.EndpointLineItem
		});
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.TargetLinkUri] = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}";
		var claims = TestUtils.GetClaims(claimsDictionary);

		var token = TestUtils.GetToken(_signingCredentials, claims: claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		await ltiService.GetLaunchResponseAsync(tokenString, "1",
			externalLoginInfo.Principal.Claims.ToList());

		var externalGradableCount = await _dbContext.ExternalGroupAssignments.CountAsync(ega =>
			ega.GroupAssignmentId.Equals(groupAssignment.Id) &&
			ega.ExternalGroupId.Equals(externalGroupId) &&
			ega.ExternalId.Equals(TestUtils.EndpointLineItem));
		Assert.AreEqual(1, externalGradableCount);
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldUpdateExternalGradableIfNeededWhenLineItemProvided()
	{
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		var externalGroupId = group.ExternalGroups.First().Id;

		_dbContext.AddExternalGradable(new TestExternalGroupAssignment
		{
			ExternalGroupId = externalGroupId,
			GroupAssignment = groupAssignment
			// No ExternalId yet
		});
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		claimsDictionary[LtiClaim.TargetLinkUri] = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}";
		var claims = TestUtils.GetClaims(claimsDictionary);

		var token = TestUtils.GetToken(_signingCredentials, claims: claims);
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		await ltiService.GetLaunchResponseAsync(tokenString, "1",
			externalLoginInfo.Principal.Claims.ToList());

		// ExternalGradable is found w/ correct LineItem value
		var externalGradable =
			await _dbContext.GetExternalGradableOrDefaultAsync(
				groupAssignment.Id,
				externalGroupId,
				TestUtils.EndpointLineItem);
		Assert.IsNotNull(externalGradable);
	}

	#endregion ExternalGradable

	#region Deep Linking

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldUseDeepLinkingSettingsWhenProvided()
	{
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials, claims: TestUtils.GetClaims(TestUtils.GetDeepLinkingClaimsDictionary()));
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNotNull(launchResponse.LtiLaunchId);
		Assert.IsNull(launchResponse.GroupId);

		var ltiLaunch =
			await _dbContext.LtiLaunches.SingleOrDefaultAsync(d => d.Id.Equals(launchResponse.LtiLaunchId.Value));
		Assert.IsNotNull(ltiLaunch);
		var expected = GenerateLtiLaunch();
		expected.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		expected.DeepLinkingData = TestUtils.DeepLinkingData;
		Assert.IsTrue(new LtiLaunchEqualityComparer().Equals(expected, ltiLaunch));
	}

	[TestMethod]
	public async Task GetLaunchResponseAsync_ShouldUseGroupIdForDeepLinkingWhenExists()
	{
		var group = await SeedGroupAsync(_dbContext);
		var ltiService = GetLtiService(_dbContext);
		var token = TestUtils.GetToken(_signingCredentials, claims: TestUtils.GetClaims(TestUtils.GetDeepLinkingClaimsDictionary()));
		var tokenString = TestUtils.GetTokenString(token);
		var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
		var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.Principal.Claims.ToList());
		Assert.IsNotNull(launchResponse.LtiLaunchId);
		Assert.IsNull(launchResponse.GroupId);

		var ltiLaunch =
			await _dbContext.LtiLaunches.SingleOrDefaultAsync(d => d.Id.Equals(launchResponse.LtiLaunchId.Value));
		Assert.IsNotNull(ltiLaunch);
		var expected = GenerateLtiLaunch();
		expected.GroupId = group.Id;
		expected.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		expected.DeepLinkingData = TestUtils.DeepLinkingData;
		Assert.IsTrue(new LtiLaunchEqualityComparer().Equals(expected, ltiLaunch));
	}

	#endregion Deep Linking

	#endregion GetLaunchResponseAsync

	#region AssertLaunchClaimsAreValid

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldSucceedWithResourceLinkMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var claims = TestUtils.GetClaims(claimsDictionary).ToList();
		ltiService.AssertLaunchClaimsAreValid(claims);
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldSucceedWithDeepLinkingMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		var claimsDictionary = TestUtils.GetDeepLinkingClaimsDictionary();
		var claims = TestUtils.GetClaims(claimsDictionary).ToList();
		ltiService.AssertLaunchClaimsAreValid(claims);
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoMessageType()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.MessageType);
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}),
			string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.MessageType,
				$"{LtiMessageType.LtiResourceLinkRequest} or {LtiMessageType.LtiDeepLinkingRequest}"));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfInvalidMessageType()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary[LtiClaim.MessageType] = "invalid";
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}),
			string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.MessageType,
				$"{LtiMessageType.LtiResourceLinkRequest} or {LtiMessageType.LtiDeepLinkingRequest}"));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfInvalidVersion()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary[LtiClaim.Version] = "2.0.0";
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.Version, LtiVersion.Lti1P3));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoContext()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.Context);
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Context));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoContextId()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				var context = TestUtils.Context;
				context.Id = null;
				claimsDictionary[LtiClaim.Context] = context;
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.Context}.id"));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoEmail()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(OidcTokenClaim.Email);
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, OidcTokenClaim.Email));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoTargetLinkUri()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.TargetLinkUri);
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.TargetLinkUri));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoResourceLink_ResourceLinkMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.ResourceLink);
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.ResourceLink));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoResourceLinkId_ResourceLinkMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
				var resourceLink = TestUtils.ResourceLink;
				resourceLink.Id = null;
				claimsDictionary[LtiClaim.ResourceLink] = resourceLink;
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.ResourceLink}.id"));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoDeepLinkingSettings_DeepLinkingMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetDeepLinkingClaimsDictionary();
				claimsDictionary.Remove(LtiDeepLinkingClaim.DeepLinkingSettings);
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, LtiDeepLinkingClaim.DeepLinkingSettings));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoDeepLinkingSettingsDeepLinkReturnUrl_DeepLinkingMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetDeepLinkingClaimsDictionary();
				var deepLinkingSettings = TestUtils.DeepLinkingSettings;
				deepLinkingSettings.DeepLinkReturnUrl = null;
				claimsDictionary[LtiDeepLinkingClaim.DeepLinkingSettings] = deepLinkingSettings;
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, $"{LtiDeepLinkingClaim.DeepLinkingSettings}.deep_link_return_url"));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoDeepLinkingSettingsAcceptTypes_DeepLinkingMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetDeepLinkingClaimsDictionary();
				var deepLinkingSettings = TestUtils.DeepLinkingSettings;
				deepLinkingSettings.AcceptTypes = null;
				claimsDictionary[LtiDeepLinkingClaim.DeepLinkingSettings] = deepLinkingSettings;
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}), string.Format(Strings.TokenMissingRequiredClaim, $"{LtiDeepLinkingClaim.DeepLinkingSettings}.accept_types"));
	}

	[TestMethod]
	public void AssertLaunchClaimsAreValid_ShouldThrowIfNoDeepLinkingSettingsAcceptPresentationDocumentTargets_DeepLinkingMessage()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() =>
			{
				var claimsDictionary = TestUtils.GetDeepLinkingClaimsDictionary();
				var deepLinkingSettings = TestUtils.DeepLinkingSettings;
				deepLinkingSettings.AcceptPresentationDocumentTargets = null;
				claimsDictionary[LtiDeepLinkingClaim.DeepLinkingSettings] = deepLinkingSettings;
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}),
			string.Format(Strings.TokenMissingRequiredClaim,
				$"{LtiDeepLinkingClaim.DeepLinkingSettings}.accept_presentation_document_targets"));
	}

	#endregion AssertLaunchClaimsAreValid

	#region AssertLtiLaunchRequestValidAsync

	[TestMethod]
	public void AssertLtiLaunchRequestValid_ShouldThrowWithoutStateParameter()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await GetLtiService(_dbContext).AssertLtiLaunchRequestIsValidAsync(null, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("state"));
	}

	[TestMethod]
	public void AssertLtiLaunchRequestValid_ShouldThrowWithoutTokenStringParameter()
	{
		const string state = "test_state";
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await GetLtiService(_dbContext).AssertLtiLaunchRequestIsValidAsync(state, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));
	}

	[TestMethod]
	public void AssertLtiLaunchRequestIsValid_ShouldThrowWithoutNonceClaim()
	{
		const string state = "test_state";
		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await GetLtiService(_dbContext)
				.AssertLtiLaunchRequestIsValidAsync(state, TestUtils.GetTokenString(new JwtSecurityToken()));
		}), string.Format(Strings.TokenMissingRequiredClaim, OidcTokenClaim.Nonce));
	}

	[TestMethod]
	public void AssertLtiLaunchRequestIsValid_ShouldThrowWithCachedNonce()
	{
		const string state = "test_state";
		const string nonce = "test_nonce";
		const string nonceCachePrefix = CacheConstants.LtiNonceCachePrefix;
		const string existentialCacheValue = CacheConstants.ExistentialCacheValue;
		_cache[$"{nonceCachePrefix}{nonce}"] = existentialCacheValue;
		Assert.ThrowsAsync<UnauthorizedException>(Task.Run(async () =>
		{
			await GetLtiService(_dbContext)
				.AssertLtiLaunchRequestIsValidAsync(state,
					TestUtils.GetTokenString(TestUtils.GetToken(_signingCredentials,
						claims: new List<Claim> { new(OidcTokenClaim.Nonce, nonce) })));
		}), Strings.LtiLaunchNonceCached);
	}

	[TestMethod]
	public void IsLtiLaunchRequestValid_ShouldThrowWithoutCachedState()
	{
		const string state = "test_state";
		const string nonce = "test_nonce";
		Assert.ThrowsAsync<UnauthorizedException>(Task.Run(async () =>
		{
			await GetLtiService(_dbContext)
				.AssertLtiLaunchRequestIsValidAsync(state,
					TestUtils.GetTokenString(TestUtils.GetToken(_signingCredentials,
						claims: new List<Claim> { new(OidcTokenClaim.Nonce, nonce) })));
		}));
	}

	[TestMethod]
	public async Task IsLtiLaunchRequestValid_ShouldNotThrowAndShouldRemoveCachedStateAndCacheNonce()
	{
		const string state = "test_state";
		const string nonce = "test_nonce";
		const string stateCachePrefix = CacheConstants.LtiStateCachePrefix;
		const string nonceCachePrefix = CacheConstants.LtiNonceCachePrefix;
		const string existentialCacheValue = CacheConstants.ExistentialCacheValue;
		_cache[$"{stateCachePrefix}{state}"] = existentialCacheValue;
		await GetLtiService(_dbContext)
			.AssertLtiLaunchRequestIsValidAsync(state,
				TestUtils.GetTokenString(TestUtils.GetToken(_signingCredentials,
					claims: new List<Claim> { new(OidcTokenClaim.Nonce, nonce) })));
		Assert.AreEqual(1, _cache.Keys.Count);
		Assert.AreEqual(existentialCacheValue, _cache[$"{nonceCachePrefix}{nonce}"]);
	}

	#endregion AssertLtiLaunchRequestValidAsync

	#region GetDeepLinkingResponseAsync

	[TestMethod]
	public void GetDeepLinkingResponseAsync_ShouldThrowIfNullArguments()
	{
		var ltiService = GetLtiService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync(null, null, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await ltiService.GetLaunchResponseAsync("", null, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("tokenString"));
	}

	[TestMethod]
	public async Task GetDeepLinkingResponseAsync_ShouldThrowIfNoExternalGroupIsFound()
	{
		var ltiExternalProvider = await _dbContext.LtiExternalProviders.FirstAsync();
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);

		var ltiService = GetLtiService(_dbContext);

		var ltiLaunch = GenerateLtiLaunch();
		ltiLaunch.ExternalId = "dummy";
		ltiLaunch.ExternalProvider = ltiExternalProvider;
		ltiLaunch.GroupId = group.Id;
		ltiLaunch.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		ltiLaunch.DeepLinkingData = TestUtils.DeepLinkingData;

		var deepLinkingResponseRequest = new DeepLinkingResponseRequest
		{
			GradableId = groupAssignment.Id,
			Url = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}"
		};

		Assert.ThrowsAsync<InvalidOperationException>(
			Task.Run(async () =>
			{
				await ltiService.GetDeepLinkingResponseAsync(ltiLaunch, deepLinkingResponseRequest);
			}));
	}

	[TestMethod]
	public async Task GetDeepLinkingResponseAsync_ShouldThrowIfGradableAndExternalGroupCoursesDoNotMatch()
	{
		var ltiExternalProvider = await _dbContext.LtiExternalProviders.FirstAsync();
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);

		// seed a new second group and externalGroup
		var group2 = new TestGroup
		{
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow,
			ExternalGroups = new List<TestExternalGroup>
			{
				new TestExternalGroup
				{
					ExternalProviderId = 1,
					ExternalId = "other-context-id",
					UserId = "1",
					Name = TestUtils.ContextTitle,
					Description = TestUtils.ContextLabel,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				}
			}
		};
		_dbContext.Groups.Add(group2);
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);

		var ltiLaunch = GenerateLtiLaunch();
		ltiLaunch.ExternalId = "other-context-id";
		ltiLaunch.ExternalProvider = ltiExternalProvider;
		ltiLaunch.GroupId = group2.Id;
		ltiLaunch.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		ltiLaunch.DeepLinkingData = TestUtils.DeepLinkingData;

		var deepLinkingResponseRequest = new DeepLinkingResponseRequest
		{
			GradableId = groupAssignment.Id,
			Url = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}"
		};

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await ltiService.GetDeepLinkingResponseAsync(ltiLaunch, deepLinkingResponseRequest);
			}), string.Format(Strings.LtiDeepLinkInvalidCourseInstructorError, groupAssignment.Assignment.Name));
	}

	[TestMethod]
	public async Task GetDeepLinkingResponseAsync_ShouldReturnTokenWithContentItemAndLineItem()
	{
		var ltiExternalProvider = await _dbContext.LtiExternalProviders.FirstAsync();
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);

		var ltiService = GetLtiService(_dbContext);

		var ltiLaunch = GenerateLtiLaunch();
		ltiLaunch.ExternalProvider = ltiExternalProvider;
		ltiLaunch.GroupId = group.Id;
		ltiLaunch.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		ltiLaunch.DeepLinkingData = TestUtils.DeepLinkingData;

		var deepLinkingResponseRequest = new DeepLinkingResponseRequest
		{
			GradableId = groupAssignment.Id,
			Url = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}"
		};
		var token = await ltiService.GetDeepLinkingResponseAsync(ltiLaunch, deepLinkingResponseRequest);

		// Serializing should not throw an exception
		var jwtHandler = new JwtSecurityTokenHandler();
		jwtHandler.WriteToken(token);

		// Check the token contains the required Claims for LTI
		Assert.AreEqual(ltiExternalProvider.OauthClientId, token.Subject);
		Assert.AreEqual(1, token.Audiences.Count());
		Assert.AreEqual(ltiExternalProvider.Issuer, token.Audiences.First());
		Assert.AreNotEqual(DateTime.MinValue, token.ValidTo); // 'exp' claim
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(JsonWebTokenClaim.IssuedAt) && c.Value != null));
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(JsonWebTokenClaim.JwtId) && c.Value != null));
		Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(OidcTokenClaim.Nonce) && c.Value != null));
		Assert.AreEqual(LtiMessageType.LtiDeepLinkingResponse,
			token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.MessageType))?.Value);
		Assert.AreEqual(LtiVersion.Lti1P3, token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.Version))?.Value);
		Assert.AreEqual(ltiExternalProvider.DeploymentId, token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.DeploymentId))?.Value);
		Assert.AreEqual(ltiLaunch.DeepLinkingData, token.Claims.SingleOrDefault(c => c.Type.Equals(LtiDeepLinkingClaim.Data))?.Value);

		// Has one ContentItem
		var contentItemsClaimsCount = token.Claims.Count(c => c.Type.Equals(LtiDeepLinkingClaim.ContentItems));
		Assert.AreEqual(1, contentItemsClaimsCount);

		// for arrays of claims, each item is stored separately
		var contentItemsClaim = token.Claims.First(c => c.Type.Equals(LtiDeepLinkingClaim.ContentItems));
		var tokenContentItem = JsonConvert.DeserializeObject<LtiResourceLinkContentItem>(contentItemsClaim.Value);

		// ContentItem has all expected values
		Assert.AreEqual(deepLinkingResponseRequest.Url, tokenContentItem.Url);
		Assert.AreEqual(groupAssignment.Name, tokenContentItem.Title);
		Assert.IsNotNull(tokenContentItem.LineItem);
		Assert.AreEqual(groupAssignment.Points, tokenContentItem.LineItem.ScoreMaximum);
		Assert.AreEqual(groupAssignment.Name, tokenContentItem.LineItem.Label);
		Assert.AreEqual(groupAssignment.Id.ToString(), tokenContentItem.LineItem.ResourceId);

		// Inserted a new ExternalGradable
		var externalGradable =
			await _dbContext.GetExternalGradableOrDefaultAsync(
				groupAssignment.Id,
				group.ExternalGroups.First().Id);
		Assert.IsNotNull(externalGradable);
	}

	[TestMethod]
	public async Task GetDeepLinkingResponseAsync_ShouldReturnTokenWithContentItemButNotLineItemIfOneExists()
	{
		var ltiExternalProvider = await _dbContext.LtiExternalProviders.FirstAsync();
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);

		_dbContext.AddExternalGradable(new TestExternalGroupAssignment
		{
			ExternalGroupId = group.ExternalGroups.First().Id,
			GroupAssignment = groupAssignment
			// No ExternalId yet
		});
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);

		var ltiLaunch = GenerateLtiLaunch();
		ltiLaunch.ExternalProvider = ltiExternalProvider;
		ltiLaunch.GroupId = group.Id;
		ltiLaunch.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		ltiLaunch.DeepLinkingData = TestUtils.DeepLinkingData;

		var deepLinkingResponseRequest = new DeepLinkingResponseRequest
		{
			GradableId = groupAssignment.Id,
			Url = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}"
		};
		var token = await ltiService.GetDeepLinkingResponseAsync(ltiLaunch, deepLinkingResponseRequest);

		// Serializing should not throw an exception
		var jwtHandler = new JwtSecurityTokenHandler();
		jwtHandler.WriteToken(token);

		// Has one ContentItem
		var contentItemsClaimsCount = token.Claims.Count(c => c.Type.Equals(LtiDeepLinkingClaim.ContentItems));
		Assert.AreEqual(1, contentItemsClaimsCount);

		// for arrays of claims, each item is stored separately
		var contentItemsClaim = token.Claims.First(c => c.Type.Equals(LtiDeepLinkingClaim.ContentItems));
		var tokenContentItem = JsonConvert.DeserializeObject<LtiResourceLinkContentItem>(contentItemsClaim.Value);

		// ContentItem has all expected values
		Assert.AreEqual(deepLinkingResponseRequest.Url, tokenContentItem.Url);
		Assert.AreEqual(groupAssignment.Name, tokenContentItem.Title);
		Assert.IsNull(tokenContentItem.LineItem);
	}

	[TestMethod]
	public async Task GetDeepLinkingResponseAsync_ShouldReturnTokenWithContentItemButNotLineItemIfNoPoints()
	{
		var ltiExternalProvider = await _dbContext.LtiExternalProviders.FirstAsync();
		var (group, groupAssignment) = await SeedGroupAndAssignmentAsync(_dbContext);
		groupAssignment.Assignment.Points = null;
		await _dbContext.SaveChangesAsync();

		var ltiService = GetLtiService(_dbContext);

		var ltiLaunch = GenerateLtiLaunch();
		ltiLaunch.ExternalProvider = ltiExternalProvider;
		ltiLaunch.GroupId = group.Id;
		ltiLaunch.DeepLinkingReturnUrl = TestUtils.DeepLinkingReturnUrl;
		ltiLaunch.DeepLinkingData = TestUtils.DeepLinkingData;

		var deepLinkingResponseRequest = new DeepLinkingResponseRequest
		{
			GradableId = groupAssignment.Id,
			Url = $"https://tool.org/courses/{group.Id}/groupAssignments/{groupAssignment.Id}"
		};
		var token = await ltiService.GetDeepLinkingResponseAsync(ltiLaunch, deepLinkingResponseRequest);

		// Serializing should not throw an exception
		var jwtHandler = new JwtSecurityTokenHandler();
		jwtHandler.WriteToken(token);

		// Has one ContentItem
		var contentItemsClaimsCount = token.Claims.Count(c => c.Type.Equals(LtiDeepLinkingClaim.ContentItems));
		Assert.AreEqual(1, contentItemsClaimsCount);

		// for arrays of claims, each item is stored separately
		var contentItemsClaim = token.Claims.First(c => c.Type.Equals(LtiDeepLinkingClaim.ContentItems));
		var tokenContentItem = JsonConvert.DeserializeObject<LtiResourceLinkContentItem>(contentItemsClaim.Value);

		// ContentItem has all expected values
		Assert.AreEqual(deepLinkingResponseRequest.Url, tokenContentItem.Url);
		Assert.AreEqual(groupAssignment.Name, tokenContentItem.Title);
		Assert.IsNull(tokenContentItem.LineItem);
	}

	#endregion GetDeepLinkingResponseAsync

	#region Helpers

	private LtiService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser,
		TestExternalGroupAssignment> GetLtiService(TestLtiLaunchDbContext dbContext)
	{
		dbContext.ChangeTracker.Clear();

		return new LtiService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser,
			TestExternalGroupAssignment>(
			dbContext,
			new JsonWebTokenService<TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser>(
				dbContext,
				_keySetProviderService,
				new CertificateService(_certificateConfigurationProvider)),
			_cacheClient,
			new TestGroupUserRoleService(dbContext));
	}

	private static async Task SeedAsync(TestLtiLaunchDbContext dbContext, CancellationToken cancellationToken = default)
	{
		dbContext.Roles.AddRange(new List<Role>
		{
			new(BaseRole.SuperAdmin),
			new(BaseRole.Admin),
			new(BaseRole.Creator),
			new(BaseRole.GroupOwner),
			new(BaseRole.GroupLearner)
		});

		dbContext.LtiExternalProviders.Add(TestUtils.GetLtiExternalProvider());

		dbContext.Users.Add(new TestUser { Id = "1", UserName = "1", NormalizedUserName = "1" });
		dbContext.Users.Add(new TestUser { Id = "2", UserName = "2", NormalizedUserName = "2" });

		await dbContext.SaveChangesAsync(cancellationToken);
	}

	private static async Task<TestGroup> SeedGroupAsync(TestLtiLaunchDbContext dbContext, CancellationToken cancellationToken = default)
	{
		var group = new TestGroup
		{
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow,
			ExternalGroups = new List<TestExternalGroup>
			{
				new()
				{
					ExternalProviderId = 1,
					ExternalId = TestUtils.ContextId,
					UserId = "1",
					Name = TestUtils.ContextTitle,
					Description = TestUtils.ContextLabel,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				}
			}
		};
		dbContext.Groups.Add(group);
		await dbContext.SaveChangesAsync(cancellationToken);
		return group;
	}

	private static async Task<(TestGroup, TestGroupAssignment)> SeedGroupAndAssignmentAsync(TestLtiLaunchDbContext dbContext,
		CancellationToken cancellationToken = default)
	{
		var group = await SeedGroupAsync(dbContext, cancellationToken);

		var groupAssignment = new TestGroupAssignment
		{
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow,
			Assignment = new TestAssignment
			{
				Name = "Test Assignment",
				Points = 100m,
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow
			},
			Group = group
		};
		dbContext.GroupAssignments.Add(groupAssignment);

		await dbContext.SaveChangesAsync(cancellationToken);
		return (group, groupAssignment);
	}

	private static LtiLaunch GenerateLtiLaunch()
	{
		return new LtiLaunch
		{
			ExternalProviderId = 1,
			CreatedById = "1",
			ExternalId = TestUtils.ContextId,
			Name = TestUtils.ContextTitle,
			Description = TestUtils.ContextLabel,
			Roles = LtiRole.MembershipInstructor,
			LtiVersion = LtiVersion.Lti1P3,
			ReturnUrl = TestUtils.LaunchPresentationReturnUrl,
			RosterUrl = TestUtils.NamesRoleServiceMembershipsUrl,
			GradesUrl = TestUtils.EndpointLineItems
		};
	}

	#endregion Helpers
}