﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public static class LisStatus
{
	public static string Active = "Active";

	public static string Inactive = "Inactive";

	public static string Deleted = "Deleted";
}