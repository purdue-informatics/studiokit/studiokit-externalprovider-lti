﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.Security.Common;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Utils;

[TestClass]
public class ClaimUtilTests : BaseTest
{
	#region UpdateExternalClaimsIssuer

	[TestMethod]
	public void UpdateExternalClaimsIssuer_ShouldUpdateIssuers()
	{
		var resourceLink = TestUtils.ResourceLink;
		var resourceClaim = ClaimUtils.CreateJsonClaim(LtiClaim.ResourceLink, resourceLink);

		var context = TestUtils.Context;
		var contextClaim = ClaimUtils.CreateJsonClaim(LtiClaim.Context, context);

		// Defaults to the same value for all claims
		var issuer = resourceClaim.Issuer;

		var claims = new List<Claim>
		{
			new(LtiClaim.MessageType, LtiMessageType.LtiResourceLinkRequest),
			resourceClaim,
			contextClaim
		};

		var updatedIssuerClaims = ClaimUtils.UpdateExternalClaimsIssuer(claims);

		Assert.IsTrue(updatedIssuerClaims.All(c => c.Issuer.Equals($"{issuer}-{resourceLink.Id}-{context.Id}")));
	}

	[TestMethod]
	public void UpdateExternalClaimsIssuer_ShouldUpdateIssuersWithoutResourceLink()
	{
		var context = TestUtils.Context;
		var contextClaim = ClaimUtils.CreateJsonClaim(LtiClaim.Context, context);

		// Defaults to the same value for all claims
		var issuer = contextClaim.Issuer;

		var claims = new List<Claim>
		{
			new(LtiClaim.MessageType, LtiMessageType.LtiDeepLinkingRequest),
			contextClaim
		};

		var updatedIssuerClaims = ClaimUtils.UpdateExternalClaimsIssuer(claims);

		Assert.IsTrue(updatedIssuerClaims.All(c => c.Issuer.Equals($"{issuer}-{context.Id}")));
	}

	#endregion UpdateExternalClaimsIssuer

	#region GetExternalRoleNames

	[TestMethod]
	public void GetExternalRoleNames_ShouldReturnRoleValues()
	{
		var roleClaims = new List<Claim>
		{
			new(LtiClaim.Roles, LtiRole.MembershipInstructor),
			new(LtiClaim.Roles, LtiRole.MembershipAdministrator),
			new(LtiClaim.Roles, LtiRole.Learner),
			new(LtiClaim.Roles, LtiRole.SystemAdministrator),
			new(LtiClaim.Roles, LtiRole.InstitutionStaff)
		};

		var externalRoleNames = ClaimUtils.GetExternalRoleNames(roleClaims);

		var expectedRoleNames = new List<string>
		{
			LtiRole.MembershipInstructor,
			LtiRole.MembershipAdministrator,
			LtiRole.Learner,
			LtiRole.SystemAdministrator,
			LtiRole.InstitutionStaff
		};

		externalRoleNames.Sort();
		expectedRoleNames.Sort();

		Assert.IsTrue(externalRoleNames.SequenceEqual(expectedRoleNames));
	}

	#endregion GetExternalRoleNames

	#region GetGroupRoleNames

	[TestMethod]
	public void GetGroupRoleNames_ShouldReturnGroupOwner()
	{
		var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor);
		var groupRoleNames = ClaimUtils.GetGroupRoleNames(new List<Claim> { roleClaim });
		groupRoleNames.Sort();
		var expectedGroupRoleNames = new List<string>
		{
			BaseRole.GroupOwner
		};
		expectedGroupRoleNames.Sort();
		Assert.IsTrue(groupRoleNames.SequenceEqual(expectedGroupRoleNames));
	}

	[TestMethod]
	public void GetGroupRoleNames_ShouldReturnGroupLearner()
	{
		var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipLearner);
		var groupRoleNames = ClaimUtils.GetGroupRoleNames(new List<Claim> { roleClaim });
		groupRoleNames.Sort();
		var expectedGroupRoleNames = new List<string>
		{
			BaseRole.GroupLearner
		};
		expectedGroupRoleNames.Sort();
		Assert.IsTrue(groupRoleNames.SequenceEqual(expectedGroupRoleNames));
	}

	[TestMethod]
	public void GetGroupRoleNames_ShouldReturnGroupGraderAndNotGroupOwner()
	{
		var groupRoleNames = ClaimUtils.GetGroupRoleNames(new List<Claim>
		{
			new(LtiClaim.Roles, LtiRole.MembershipInstructor),
			new(LtiClaim.Roles, LtiRole.MembershipInstructorGrader)
		});
		groupRoleNames.Sort();
		var expectedGroupRoleNames = new List<string>
		{
			BaseRole.GroupGrader
		};
		expectedGroupRoleNames.Sort();
		Assert.IsTrue(groupRoleNames.SequenceEqual(expectedGroupRoleNames));
	}

	#endregion GetGroupRoleNames

	#region GetCommonClaims

	[TestMethod]
	public void GetCommonClaims_ShouldMapToCommonClaims()
	{
		var idClaim = new Claim(JsonWebTokenClaim.Subject, "id123");
		var givenNameClaim = new Claim(OidcTokenClaim.GivenName, "user");
		var familyNameClaim = new Claim(OidcTokenClaim.FamilyName, "McUser");
		var emailClaim = new Claim(OidcTokenClaim.Email, "user@example.com");
		var extraClaim = new Claim("NotCommonClaim", "some value");

		var claimList = new List<Claim>
		{
			idClaim,
			givenNameClaim,
			familyNameClaim,
			emailClaim,
			extraClaim
		};

		var commonClaims = ClaimUtils.GetCommonClaims(claimList);

		//Verify commonClaims does not contain the extra claim
		Assert.IsTrue(commonClaims.Count == LtiClaim.CommonClaimMap.Count);

		//Verify claim values mapped correctly
		Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
			c.Type.Equals(ClaimTypes.NameIdentifier) &&
			c.Value.Equals(idClaim.Value) &&
			c.ValueType.Equals(idClaim.ValueType) &&
			Equals(c.Subject, idClaim.Subject) &&
			c.Issuer.Equals(idClaim.Issuer) &&
			c.OriginalIssuer.Equals(idClaim.OriginalIssuer))
		);

		Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
			c.Type.Equals(ClaimTypes.GivenName) &&
			c.Value.Equals(givenNameClaim.Value) &&
			c.ValueType.Equals(givenNameClaim.ValueType) &&
			Equals(c.Subject, givenNameClaim.Subject) &&
			c.Issuer.Equals(givenNameClaim.Issuer) &&
			c.OriginalIssuer.Equals(givenNameClaim.OriginalIssuer))
		);

		Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
			c.Type.Equals(ClaimTypes.Surname) &&
			c.Value.Equals(familyNameClaim.Value) &&
			c.ValueType.Equals(familyNameClaim.ValueType) &&
			Equals(c.Subject, familyNameClaim.Subject) &&
			c.Issuer.Equals(familyNameClaim.Issuer) &&
			c.OriginalIssuer.Equals(familyNameClaim.OriginalIssuer))
		);

		Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
			c.Type.Equals(ClaimTypes.Email) &&
			c.Value.Equals(emailClaim.Value) &&
			c.ValueType.Equals(emailClaim.ValueType) &&
			Equals(c.Subject, emailClaim.Subject) &&
			c.Issuer.Equals(emailClaim.Issuer) &&
			c.OriginalIssuer.Equals(emailClaim.OriginalIssuer))
		);
	}

	#endregion GetCommonClaims

	#region GetBrightspaceClaims

	[TestMethod]
	public void GetBrightspaceClaims_ShouldReturnNoClaimsIfWithoutBrightspaceClaim()
	{
		var brightspaceClaims = ClaimUtils.GetBrightspaceClaims(new List<Claim>());
		Assert.AreEqual(0, brightspaceClaims.Count);
	}

	[TestMethod]
	public void GetBrightspaceClaims_ShouldReturnClaimsSuccessfully()
	{
		var claims = TestUtils.GetClaims(TestUtils.GetResourceLinkClaimsDictionary());
		var brightspaceClaims = ClaimUtils.GetBrightspaceClaims(claims);
		Assert.AreEqual(TestUtils.BrightspaceOrgDefinedId,
			brightspaceClaims.SingleOrDefault(c => c.Type.Equals(BrightspaceClaim.BrightspaceOrgDefinedId))?.Value);
		Assert.AreEqual(TestUtils.BrightspaceUsername,
			brightspaceClaims.SingleOrDefault(c => c.Type.Equals(BrightspaceClaim.BrightspaceUsername))?.Value);
	}

	[TestMethod]
	public void GetBrightspaceClaims_ShouldReturnImpersonatorClaimIfAny()
	{
		var claimsDictionary = TestUtils.GetResourceLinkClaimsDictionary();
		var brightspaceClaim = TestUtils.BrightspaceClaim;
		brightspaceClaim.ImpersonationOrgDefinedId = "222222222";
		claimsDictionary[BrightspaceClaim.BrightspaceClaimType] = brightspaceClaim;
		var claims = TestUtils.GetClaims(claimsDictionary);
		var brightspaceClaims = ClaimUtils.GetBrightspaceClaims(claims);

		Assert.AreEqual(brightspaceClaim.ImpersonationOrgDefinedId,
			brightspaceClaims.SingleOrDefault(c => c.Type.Equals(BrightspaceClaim.BrightspaceImpersonationOrgDefinedId))?.Value);
	}

	#endregion GetBrightspaceClaims

	#region GetApplicationRoleNames

	[TestMethod]
	public void GetApplicationRoleNames_ShouldReturnCreator()
	{
		var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor);
		var applicationRoleNames = ClaimUtils.GetApplicationRoleNames(new List<Claim> { roleClaim });
		applicationRoleNames.Sort();
		var expectedRoleNames = new List<string>
		{
			BaseRole.Creator
		};
		expectedRoleNames.Sort();
		Assert.IsTrue(applicationRoleNames.SequenceEqual(expectedRoleNames));
	}

	#endregion GetApplicationRoleNames

	#region GetApplicationRoleClaims

	[TestMethod]
	public void GetApplicationRoleClaims_ShouldReturnNewClaims()
	{
		var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor);
		var applicationRoleClaims = ClaimUtils.GetApplicationRoleClaims(new List<Claim> { roleClaim });
		Assert.AreEqual(1, applicationRoleClaims.Count);
		Assert.AreEqual(ClaimTypes.Role, applicationRoleClaims[0].Type);
		Assert.AreEqual(BaseRole.Creator, applicationRoleClaims[0].Value);
	}

	#endregion GetApplicationRoleClaims
}