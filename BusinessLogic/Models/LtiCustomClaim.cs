using Newtonsoft.Json;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public class LtiCustomClaim
{
	/// <summary>
	/// Expected name for the start date custom variable. We provide this name when registering with an LMS
	/// </summary>
	public const string CourseSectionStartDateCustomName = "lis_course_section_begin";

	/// <summary>
	/// The substitution custom property name for the course start date.
	/// </summary>
	public static string CourseSectionStartDateLisVariable = "$CourseSection.timeFrame.begin";

	/// <summary>
	/// Expected name for the end date custom variable. We provide this name when registering with an LMS
	/// </summary>
	public const string CourseSectionEndDateCustomName = "lis_course_section_end";

	/// <summary>
	/// The substitution variable name for the course end date.
	/// </summary>
	public static string CourseSectionEndDateLisVariable = "$CourseSection.timeFrame.end";

	/// <summary>
	/// Custom LIS variable for the course start date. If this is equal to <see cref="CourseSectionStartDateLisVariable"/>
	/// then the platform does not support substituting this value.
	/// </summary>
	[JsonProperty(CourseSectionStartDateCustomName)]
	public string CourseSectionStartDate { get; set; }

	/// <summary>
	/// Custom LIS variable for the course end date. If this is equal to <see cref="CourseSectionEndDateLisVariable"/>
	/// then the platform does not support substituting this value.
	/// </summary>
	[JsonProperty(CourseSectionEndDateCustomName)]
	public string CourseSectionEndDate { get; set; }
}