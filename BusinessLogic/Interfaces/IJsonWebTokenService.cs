﻿using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;

public interface IJsonWebTokenService
{
	/// <summary>
	/// Find the <see cref="LtiExternalProvider"/> that corresponds to the given <paramref name="token"/> using OAuth Client Id and Deployment Id.
	/// </summary>
	/// <param name="token">A json web token</param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <returns></returns>
	Task<LtiExternalProvider> GetTokenProviderAsync(JwtSecurityToken token, CancellationToken cancellationToken = default);

	/// <summary>
	/// Checks the payload of a platform originating JWT for required claims and verifies the signature
	/// </summary>
	/// <param name="tokenString">The JWT as a string</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <exception cref="UnauthorizedException"></exception>
	Task AssertIsTokenValidAsync(string tokenString, CancellationToken cancellationToken = default);

	/// <summary>
	/// Creates a JWT intended to be sent to the given <paramref name="externalProvider"/>, including the given <paramref name="claims"/>.
	/// Signs the JWT using <see cref="ICertificateService"/>.
	/// </summary>
	/// <param name="externalProvider"></param>
	/// <param name="claims"></param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	Task<JwtSecurityToken> CreateJwtAsync(LtiExternalProvider externalProvider, List<Claim> claims,
		CancellationToken cancellationToken = default);
}