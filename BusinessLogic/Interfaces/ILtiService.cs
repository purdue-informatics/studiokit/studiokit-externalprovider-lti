﻿using Microsoft.AspNetCore.Identity;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Models;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;

public interface ILtiService
{
	Task<string> PrepareOidcRedirectAsync(
		IEnumerable<KeyValuePair<string, string>> requestData,
		string baseUri,
		CancellationToken cancellationToken = default);

	string GetLaunchPresentationReturnUrl(string token);

	Task<ExternalLoginInfo> GetLoginInfoAsync(string token, CancellationToken cancellationToken = default);

	Task<LaunchResponse> GetLaunchResponseAsync(
		string token,
		string userId,
		List<Claim> claims,
		CancellationToken cancellationToken = default);

	Task AssertLtiLaunchRequestIsValidAsync(string state, string tokenString);

	Task<JwtSecurityToken> GetDeepLinkingResponseAsync(
		LtiLaunch ltiLaunch,
		DeepLinkingResponseRequest deepLinkingResponseRequest,
		CancellationToken cancellationToken = default);
}