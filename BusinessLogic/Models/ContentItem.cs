﻿using System;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models;

public abstract class ContentItem
{
	public string Type { get; protected set; }
}

public class LinkContentItem : ContentItem
{
	public LinkContentItem()
	{
		Type = LtiDeepLinkingContentType.Link;
	}

	public string Url { get; set; }

	public string Title { get; set; }

	public string Text { get; set; }

	public ContentItemImage Icon { get; set; }

	public ContentItemImage Thumbnail { get; set; }

	public ContentItemEmbed Embed { get; set; }

	public ContentItemWindow Window { get; set; }

	public ContentItemIframe Iframe { get; set; }
}

public class LtiResourceLinkContentItem : ContentItem
{
	public LtiResourceLinkContentItem()
	{
		Type = LtiDeepLinkingContentType.LtiResourceLink;
	}

	public string Url { get; set; }

	public string Title { get; set; }

	public string Text { get; set; }

	public ContentItemImage Icon { get; set; }

	public ContentItemImage Thumbnail { get; set; }

	public ContentItemIframe Iframe { get; set; }

	public Dictionary<string, string> Custom { get; set; }

	public LineItem LineItem { get; set; }

	public ContentItemDates Available { get; set; }

	public ContentItemDates Submission { get; set; }
}

public class FileContentItem : ContentItem
{
	public FileContentItem()
	{
		Type = LtiDeepLinkingContentType.File;
	}

	public string Url { get; set; }

	public string Title { get; set; }

	public string Text { get; set; }

	public ContentItemImage Icon { get; set; }

	public ContentItemImage Thumbnail { get; set; }

	public DateTime? ExpiresAt { get; set; }
}

public class HtmlContentItem : ContentItem
{
	public HtmlContentItem()
	{
		Type = LtiDeepLinkingContentType.Html;
	}

	public string Html { get; set; }

	public string Title { get; set; }

	public string Text { get; set; }
}

public class ImageContentItem : ContentItem
{
	public ImageContentItem()
	{
		Type = LtiDeepLinkingContentType.Image;
	}

	public string Url { get; set; }

	public string Title { get; set; }

	public string Text { get; set; }

	public ContentItemImage Icon { get; set; }

	public ContentItemImage Thumbnail { get; set; }

	public int? Width { get; set; }

	public int? Height { get; set; }
}

#region Property Types

public class ContentItemImage
{
	public string Url { get; set; }

	public int Width { get; set; }

	public int Height { get; set; }
}

public class ContentItemEmbed
{
	public string Html { get; set; }
}

public class ContentItemWindow
{
	public string TargetName { get; set; }

	public int Width { get; set; }

	public int Height { get; set; }

	public string WindowFeatures { get; set; }
}

public class ContentItemIframe
{
	public string Src { get; set; }

	public int Width { get; set; }

	public int Height { get; set; }
}

public class ContentItemDates
{
	public DateTime? StartDateTime { get; set; }

	public DateTime? EndDateTime { get; set; }
}

#endregion Property Types