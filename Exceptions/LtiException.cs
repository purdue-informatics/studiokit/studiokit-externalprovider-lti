﻿using System;

namespace StudioKit.ExternalProvider.Lti.Exceptions;

public class LtiException : Exception
{
	public LtiException()
	{
	}

	public LtiException(string message) : base(message)
	{
	}

	public LtiException(string message, Exception inner) : base(message, inner)
	{
	}
}